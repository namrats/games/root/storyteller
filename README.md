# Storyteller

A tool for notating games of [Root][root] and playing them back.

[![(c) William RM Bartlett, Creative Commons Attribution-ShareAlike 4.0 International License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)

## License

See [LICENSE.md](LICENSE.md)

Authors:
- William Bartlett

## Contributing

For more information, join the Woodland Warriors Discord and get in touch with `punkstarman`.

## Acknowledgements

This is largely inspired by [Rootlog][rootlog] and [RootVis][rootvis].
There are some adjustments to the Rootlog notation.
There are plans to add a layer on top in order to notate higher level actions (March, Organize, Sway...).

This is loosely inspired by similar tools for games like chess, backgammon and go.

[root]: https://ledergames.com/products/root-a-game-of-woodland-might-and-right
[rootlog]: https://github.com/Vagabottos/Rootlog/blob/master/Rootlog_V2.md
[rootvis]: https://rootvis.seiyria.com/
