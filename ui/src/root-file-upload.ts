import {LitElement, html, css} from 'lit';
import {customElement, property} from 'lit/decorators.js';
import {styles} from './styles';
import './root-icon';

@customElement('root-file-upload')
export class RootFileUpload extends LitElement {
    @property()
    content?: string;

    static styles = [
        styles,
        css`
            :host { display: block; }
            .container { display: flex; flex-direction: row; gap: 8px; }
        `,
    ];

    override render() {
        return html`
            <div class="container">
                <input type="file" id="dropZone" @change="${this._onChange}"/>
                <button class="wide" @click="${this._onClick}">Upload</button>
            </div>
            `;
    }

    private async _onChange(event: Event) {
        const files = (event.target as HTMLInputElement).files;
        if (files && files[0]) {
            this.content = await files[0].text();
        }
    }

    private _onClick() {
        this.dispatchEvent(new Event("upload"));
    }
}
