import {LitElement, html, css} from 'lit';
import {customElement, property} from 'lit/decorators.js';
import {when} from 'lit/directives/when.js';
import {styles} from './styles';
import './root-icon';

@customElement('root-warriors')
export class RootWarriors extends LitElement {
    @property()
    faction: string = "";

    @property({type: Number})
    count?: number;

    @property({type: Boolean})
    warlord: boolean = false;

    static styles = [
        styles,
        css`
            :host { display: block; aspect-ratio: 1 / 1; }
            .container { position: relative; width: 100%; height: 100%; }
            .warrior { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
            .warrior::part(icon) { stroke: #000; stroke-width: 4px; }
            .count { position: absolute; top: 55%; left: 50%; width: 47%; height: 47%; translate: -50% -50%; }
            .banner { position: absolute; top: -25%; left: 20%; width: 80%; height: 80%; }
            .banner::part(icon) { stroke: #000; stroke-width: 0.5px; }
            text {
                font-size: 7pt;
                font-weight: bold;
                fill: white;
                text-anchor: middle;
            }
        `,
    ];

    override render() {
        return html`
            <style>
                .warrior, .banner { color: var(--${this.faction}-color); }
            </style>
            <div class="container">
                ${when(this.warlord, () => html`
                    <root-icon class="banner" icon="hundreds-banner"></root-icon>
                `)}
                <root-icon class="warrior" icon="${this.faction}"></root-icon>
                ${this.renderCount()}
            </div>
            `;
    }

    renderCount() {
        if (!this.faction.startsWith("vagabond") && this.count !== undefined) {
            return html`
            <svg class="count" viewBox="0 0 10 10">
                <text x="5" y="10">${this.count}</text>
            </svg>
            `;
        } else {
            return null;
        }
    }
}
