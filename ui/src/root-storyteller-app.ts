import {LitElement, html, css} from 'lit';
import {customElement, property, query} from 'lit/decorators.js';
import {styles} from './styles';
import {RootStoryteller} from './root-storyteller';
import './root-storyteller';
import './root-file-download';
import {RootFileUpload} from './root-file-upload';
import './root-file-upload';
import init, {parseAction, states, turns_text, dump_game, load_game} from 'storyteller-domain';
import {GameRecord, Turn, TurnText} from './types';
import {produce} from 'immer';

@customElement('root-storyteller-app')
export class RootStorytellerApp extends LitElement {
    @property()
    gameRecord: GameRecord = {
        mapName: "autumn",
        deckName: "enp",
        landmarks: [],
        turns: [],
    }

    @property()
    turns: TurnText[] = [];

    @property()
    states: Turn[] = [];

    @query("#downloadLink")
    downloadLink!: HTMLAnchorElement;

    override async connectedCallback() {
        super.connectedCallback();
        await init();
        window.addEventListener("hashchange", this._onHashChange);
        window.addEventListener("keydown", this._onKey);
    }

    override disconnectedCallback() {
        window.removeEventListener("keydown", this._onKey);
        window.removeEventListener("hashchange", this._onHashChange);
        super.disconnectedCallback();
    }

    static styles = [
        styles,
        css`
            :host { display: block; }
            root-storyteller { width: 100%; height: 100%; }
        `,
    ];

    override render() {
        const segments = window.location.hash.replace("#", "").split("/");
        const mode = segments[0] || "edit";

        const actionRefs = this.states.flatMap((turn, i) => turn.states.map((_, j) => [i, j]));

        let selectedTurn: number|undefined, selectedAction: number|undefined;
        if (mode == "view") {
            [selectedTurn, selectedAction] = segments.slice(1).map(Number);
            if (selectedTurn === undefined) {
                [selectedTurn, selectedAction] = actionRefs[0] || [undefined, undefined];
            } else if (selectedAction === undefined) {
                selectedAction = 0;
            } else if (selectedTurn >= this.states.length) {
                if (actionRefs.slice(-1)[0]) {
                    [selectedTurn, selectedAction] = actionRefs.slice(-1)[0];
                    window.location.hash = `#view/${selectedTurn}/${selectedAction}`;
                } else {
                    [selectedTurn, selectedAction] = [undefined, undefined];
                    window.location.hash = "#view";
                }
            }
        } else {
            [selectedTurn, selectedAction] = actionRefs.slice(-1)[0] || [undefined, undefined];
        }

        const state = (selectedTurn !== undefined && selectedAction !== undefined)
            ? this.states[selectedTurn].states[selectedAction]
            : {piecesByLocation: [], scores: {}};

        return html`
        <root-storyteller ?edit="${mode == "edit"}" selectedTurn="${selectedTurn}" selectedAction="${selectedAction}" .gameRecord="${this.gameRecord}" .turns="${this.turns}" .state="${state}" @change="${this._onChange}">
            <div slot="tools"><a class="button wide" href="#edit">Edit</a></div>
            <div slot="tools"><a class="button wide" href="#view">View</a></div>
            <root-file-download slot="tools" @download="${this._onDownload}"></root-file-download>
            <root-file-upload slot="tools" @upload="${this._onUpload}"></root-file-upload>
        </root-storyteller>
        <a id="downloadLink" download="root-game.txt" style="display: none"></a>
        `;
    }

    private _onChange(event: Event) {
        const storyteller = event.target as RootStoryteller;

        this.turns = storyteller.turns;
        this.gameRecord = produce(storyteller.gameRecord, (draft) => {
            draft.turns = this.turns.map((turn: any) => {
                return {
                    faction: turn.faction,
                    actions: turn.actions.flatMap((action: any) => {
                        try {
                            return [parseAction(turn.faction, action)];
                        } catch (e) {
                            console.log(e);
                            return [];
                        }
                    })
                };
            })
        });
        this.states = states(this.gameRecord);
    }

    private _onDownload() {
        const content = dump_game(this.gameRecord);
        const blob = new Blob([content], {type: "text/plain"});
        const url = URL.createObjectURL(blob)
        this.downloadLink.href = url;
        this.downloadLink.click();
        URL.revokeObjectURL(url);
    }

    private _onUpload(event: Event) {
        const content = (event.target as RootFileUpload).content;
        if (content) {
            this.gameRecord = load_game(content);
            this.turns = turns_text(this.gameRecord);
            this.states = states(this.gameRecord);
        }
    }

    private _onHashChange = () => {
        this.requestUpdate();
    }

    private _onKey = (event: KeyboardEvent) => {
        const actionRefs = this.states.flatMap((turn, i) => turn.states.map((_, j) => `#view/${i}/${j}`));

        if (window.location.hash.replace("#", "").split("/")[0] == "view" && actionRefs.length && !event.isComposing && !event.ctrlKey && !event.shiftKey && !event.metaKey) {
            const i = Math.max(0, actionRefs.indexOf(window.location.hash));
            if (event.key == "ArrowUp" && i > 0) {
                window.location.hash = actionRefs[i-1];
            } else if (event.key == "ArrowDown" && i < actionRefs.length - 1) {
                window.location.hash = actionRefs[i+1];
            }
        }
    }
}
