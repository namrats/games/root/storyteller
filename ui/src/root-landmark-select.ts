import {LitElement, html, css} from 'lit';
import {customElement, property, queryAll} from 'lit/decorators.js';
import {map} from 'lit/directives/map.js';
import {styles} from './styles';
import {Landmark} from './types';
import './root-icon';

@customElement('root-landmark-select')
export class RootLandmarkSelect extends LitElement {
    static readonly landmarks = ["ferry", "tower", "lostCity"];

    @property()
    value: Landmark[] = [];

    @queryAll("input")
    _checkboxes!: NodeListOf<HTMLInputElement>

    static styles = [
        styles,
        css`
            :host { display: inline-block; }
            .container { display: flex; flex-direction: row; }
            root-icon {
                height: 2em;
                color: lightgray;
            }
            input { display: none; }
            label { display: block; }
            input:checked + label > root-icon[icon="ferry"] {
                color: var(--ferry-color);
            }
            input:checked + label > root-icon[icon="tower"] {
                color: var(--tower-color);
            }
            input:checked + label > root-icon[icon="lostCity"] {
                color: var(--lostCity-color);
            }
        `,
    ];

    override render() {
        return html`
            <div class="container">
            ${map(RootLandmarkSelect.landmarks, (landmark) => html`
                <input type="checkbox" name="landmark" id="${landmark}" value="${landmark}" ?checked="${this.value !== undefined && this.value.includes(landmark)}" @change="${this._onChange}" />
                <label for="${landmark}">
                    <root-icon icon="${landmark}"></root-icon>
                </label>
            `)}
            <div>
        `;
    }

    private _onChange() {
        this.value = Array.from(this._checkboxes).filter(input => input.checked).map(input => input.value);
        this.dispatchEvent(new Event("change"));
    }
}
