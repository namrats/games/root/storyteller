import { StoryObj, Meta } from '@storybook/web-components';
import { html } from 'lit';
import '../root-draft-select';
import '../root.css';

export default {
  title: 'Root/Molecules/DraftSelect',
} as Meta;

export const DraftSelect: StoryObj = {
    render: (args) => html`
    <style>
    </style>
    <root-draft-select></root-draft-select>
    `,
};
