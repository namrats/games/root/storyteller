import { StoryObj, Meta } from '@storybook/web-components';
import { html } from 'lit';
import { repeat } from 'lit/directives/repeat.js';
import '../root-icon';
import '../root.css';

export default {
  title: 'Root/Atoms/Icon',
} as Meta;

const iconsByCategory = [
    {title: "Suits", icons: ["fox", "mouse", "rabbit", "bird"]},
    {title: "Factions", icons: ["marquise", "eyrie", "alliance", "vagabond1", "vagabond2", "riverfolk", "cult", "duchy", "corvid", "hundreds", "keepers"]},
    {title: "Buildings", icons: ["ruins", "recruiter", "sawmill", "workshop", "roost", "base", "garden", "citadel", "market", "stronghold", "waystation"]},
    {title: "Tokens", icons: ["keep", "wood", "sympathy", "tradePost", "tunnel", "plot", "bomb", "extortion", "raid", "snare", "mob", "figure", "jewelry", "tablet"]},
    {title: "Items", icons: ["torch", "bag", "boot", "crossbow", "hammer", "sword", "tea", "coins"]},
    {title: "Landmarks", icons: ["ferry", "tower", "lostCity"]},
    {title: "Actions", icons: ["upload", "download", "view"]},
];

export const All: StoryObj = {
    render: () => {
        return html`
        <style>
        root-icon {
            width: 32px;
            height: 32px;
            color: white;
            background-color: gray;
        }
        div.category {
            display: flex; flex-flow: row wrap;
        }
        </style>
        <div>
        ${repeat(iconsByCategory, (category) => html`
            <h2>${category.title}</h2>
            <div class="category">
                ${repeat(category.icons, (icon) => html`<root-icon icon="${icon}"></root-icon>`)}
            </div>
            `)}
        </div>
    `},
}

export const Custom: StoryObj = {
    render: (args) => html`
    <style>
        :root {
            background-color: var(--${args.backgroundColor}-color);
        }
        root-icon {
            width: ${args.size}px; height: ${args.size}px; color: var(--${args.color}-color);
        }

        root-icon::part(icon) {
            stroke: black; stroke-width: ${args.strokeWidth}px;
        }
    </style>
    <root-icon icon="${args.icon}"></root-icon>
    `,
    args: {
        size: 64,
        strokeWidth: 2,
        icon: "marquise",
        color: "marquise",
        backgroundColor: "none",
    },
    argTypes: {
        size: {
            type: 'number',
            control: {
                type: 'range',
                min: 8,
                max: 128,
                step: 8,
            },
        },
        strokeWidth: {
            type: 'number',
            control: {
                type: 'range',
                min: 0,
                max: 10,
            },
        },
        icon: {
            options: [
                'marquise',
                'eyrie',
                'alliance',
                'vagabond',
                'riverfolk',
                'cult',
                'duchy',
                'corvid',
                'hundreds',
                'keepers',
                'bird',
                'fox-wborder',
                'mouse-wborder',
                'rabbit-wborder',
                'recruiter',
                'sawmill',
                'workshop',
                'roost',
                'base',
                'keep',
                'wood',
                'sympathy',
                'torch',
                'bag',
                'boot',
                'crossbow',
                'hammer',
                'sword',
                'tea',
                'coins',
                'ferry',
                'tower',
                'lostCity',
                'ruins',
            ],
            control: "select",
        },
        color: {
            type: 'string',
            options: [
                'white',
                'marquise',
                'eyrie',
                'alliance',
                'vagabond',
                'cult',
                'riverfolk',
                'duchy',
                'duchy-alt',
                'corvid',
                'corvid-alt',
                'hundreds',
                'keepers',
                'fox',
                'mouse',
                'rabbit',
                'bird',
                'ferry',
                'tower',
                'lostCity',
            ],
            control: "select",
        },
        backgroundColor: {
            type: 'string',
            options: [
                'none',
                'marquise',
                'eyrie',
                'alliance',
                'vagabond',
                'cult',
                'riverfolk',
                'duchy',
                'corvid',
                'hundreds',
                'keepers',
                'fox',
                'mouse',
                'rabbit',
                'bird',
                'ferry',
                'tower',
                'lostCity',
            ],
            control: "select",
        },
    },
};
