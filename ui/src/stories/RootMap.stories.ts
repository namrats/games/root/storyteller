import { StoryObj, Meta } from '@storybook/web-components';
import { html } from 'lit';
import '../root-map';
import maps from '../root-maps';
import '../root.css';

export default {
  title: 'Root/Organisms/Map',
} as Meta;

const pieces = [{"location":{"path":[5,9]},"pieces":[{"piece":{"marker":"closedPath"},"count":1}]},{"location":{"clearing":6},"pieces":[{"piece":{"building":{"type":"recruiter"}},"count":1},{"piece":{"warrior":"marquise"},"count":1},{"piece":{"building":{"type":"workshop"}},"count":1}]},{"location":{"clearing":4},"pieces":[{"piece":{"building":{"type":"sawmill"}},"count":1},{"piece":{"token":{"type":"keep"}},"count":1},{"piece":{"warrior":"marquise"},"count":1}]},{"location":{"clearing":12},"pieces":[{"piece":{"warrior":"marquise"},"count":1}]},{"location":{"clearing":8},"pieces":[{"piece":{"warrior":"marquise"},"count":1}]},{"location":{"hand":"eyrie"},"pieces":[{"piece":{"card":{"deckCard":[null,null]}},"count":3}]},{"location":{"clearing":2},"pieces":[{"piece":{"warrior":"marquise"},"count":1}]},{"location":{"supply":null},"pieces":[{"piece":{"item":["coins","refreshed"]},"count":2},{"piece":{"item":["bag","refreshed"]},"count":2},{"piece":{"item":["boot","refreshed"]},"count":2},{"piece":{"item":["sword","refreshed"]},"count":2},{"piece":{"item":["tea","refreshed"]},"count":2},{"piece":{"item":["hammer","refreshed"]},"count":1},{"piece":{"item":["crossbow","refreshed"]},"count":1}]},{"location":"eyrieLeader","pieces":[{"piece":{"card":{"eyrieLeader":"despot"}},"count":1}]},{"location":{"clearing":11},"pieces":[{"piece":{"marker":"ruins"},"count":1},{"piece":{"warrior":"marquise"},"count":1}]},{"location":"drawPile","pieces":[{"piece":{"card":{"deckCard":[null,null]}},"count":48}]},{"location":{"clearing":5},"pieces":[{"piece":{"warrior":"marquise"},"count":1}]},{"location":{"hand":"marquise"},"pieces":[{"piece":{"card":{"deckCard":[null,null]}},"count":3}]},{"location":{"supply":"marquise"},"pieces":[{"piece":{"token":{"type":"keep"}},"count":0},{"piece":{"building":{"type":"workshop"}},"count":5},{"piece":{"building":{"type":"recruiter"}},"count":5},{"piece":{"warrior":"marquise"},"count":14},{"piece":{"building":{"type":"sawmill"}},"count":5},{"piece":{"token":{"type":"wood"}},"count":8}]},{"location":{"clearing":1},"pieces":[{"piece":{"warrior":"marquise"},"count":1}]},{"location":{"clearing":3},"pieces":[{"piece":{"building":{"type":"roost"}},"count":1},{"piece":{"warrior":"eyrie"},"count":6}]},{"location":{"supply":"vagabond"},"pieces":[{"piece":{"pawn":"vagabond"},"count":1}]},{"location":{"clearing":10},"pieces":[{"piece":{"warrior":"marquise"},"count":1}]},{"location":{"supply":"alliance"},"pieces":[{"piece":{"token":{"type":"sympathy"}},"count":10},{"piece":{"building":{"type":"base","suit":"fox"}},"count":1},{"piece":{"building":{"type":"base","suit":"mouse"}},"count":1},{"piece":{"building":{"type":"base","suit":"rabbit"}},"count":1},{"piece":{"warrior":"alliance"},"count":10}]},{"location":{"clearing":9},"pieces":[{"piece":{"warrior":"marquise"},"count":1}]},{"location":{"supply":"eyrie"},"pieces":[{"piece":{"warrior":"eyrie"},"count":14},{"piece":{"card":{"eyrieLeader":"charismatic"}},"count":1},{"piece":{"card":{"eyrieLeader":"commander"}},"count":1},{"piece":{"building":{"type":"roost"}},"count":6},{"piece":{"card":{"eyrieLeader":"builder"}},"count":1},{"piece":{"card":{"eyrieLeader":"despot"}},"count":0}]},{"location":{"clearing":7},"pieces":[{"piece":{"warrior":"marquise"},"count":1}]},{"location":{"forest":[3,7,11,12]},"pieces":[{"piece":{"pawn":"vagabond1"},"count":1}]}]

export const Map: StoryObj = {
    render: (args) => html`
        <style>
            root-map {
                width: 800px;
                height: 600px;
            }
        </style>
        <root-map .map=${maps[args.map]} .pieces=${pieces}></root-map>
        `,
    args: {
        map: "autumn",
    },
    argTypes: {
        map: {
            options: Object.keys(maps),
            control: "select",
        }
    }
};
