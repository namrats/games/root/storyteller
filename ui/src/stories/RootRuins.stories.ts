import { StoryObj, Meta } from '@storybook/web-components';
import { html } from 'lit';
import '../root-ruins';
import '../root.css';

export default {
  title: 'Root/Molecules/Ruins',
} as Meta;

export const Ruins: StoryObj = {
    render: (args) => html`
        <style>
        root-ruins {
            width: ${args.size}px;
            height: ${args.size}px;
        }
        </style>
        <root-ruins></root-ruins>
    `,
    args: {
        size: 64,
    },
    argTypes: {
        size: {
            type: 'number',
            control: {
                type: 'range',
                min: 8,
                max: 128,
                step: 8,
            },
        },
    }
};
