import { StoryObj, Meta } from '@storybook/web-components';
import { html } from 'lit';
import '../root-forest';
import '../root.css';

export default {
  title: 'Root/Organisms/Forest',
} as Meta;

const pieces = [
    {piece: {pawn: "vagabond1"}, count: 1},
    {piece: {token: {type: "relic", relic: "jewelry"}}, count: 1},
]

export const Forest: StoryObj = {
    render: (args) => html`
        <style>
            root-forest {
                width: ${args.size}px; height: ${args.size}px;
            }
        </style>
        <root-forest .pieces="${pieces}"></root-forest>
        `,
    args: {
        size: 128,
    },
    argTypes: {
        size: {
            type: 'number',
            defaultValue: 128,
            control: {
                type: 'range',
                min: 8,
                max: 128,
                step: 8,
            },
        },
    },
};
