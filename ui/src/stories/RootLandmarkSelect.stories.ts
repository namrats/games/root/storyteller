import { StoryObj, Meta } from '@storybook/web-components';
import { action } from '@storybook/addon-actions';
import { html } from 'lit';
import '../root-landmark-select';
import '../root.css';

export default {
  title: 'Root/Molecules/LandmarkSelect',
} as Meta;

export const LandmarkSelect: StoryObj = {
    render: (args) => html`
        <style>
        </style>
        <root-landmark-select @change="${action("change")}"></root-landmark-select>
        `,
};
