import { StoryObj, Meta } from '@storybook/web-components';
import { html } from 'lit';
import '../root-building';
import '../root.css';

export default {
  title: 'Root/Molecules/Building',
} as Meta;

export const Building: StoryObj = {
    render: (args) => {
        const building = {type: args.building};
        if (building.type === "base") {
            building.suit = args.baseSuit;
        } else if (building.type === "garden") {
            building.suit = args.gardenSuit;
        } else if (building.type === "waystation") {
            building.obverse = args.obverse;
            building.reverse = args.reverse;
        }
        return html`
            <style>
            root-building {
                width: ${args.size}px;
                height: ${args.size}px;
            }
            </style>
            <root-building .building="${building}"></root-building>
            `;
    },
    args: {
        size: 128,
        building: "recruiter",
    },
    argTypes: {
        size: {
            type: 'number',
            control: {
                type: 'range',
                min: 8,
                max: 128,
                step: 8,
            },
        },
        building: {
            options: ["recruiter", "sawmill", "workshop", "roost", "base", "garden", "citadel", "market", "stronghold", "waystation"],
            control: "select",
        },
        baseSuit: {
            options: ["fox", "mouse", "rabbit"],
            control: "select",
            if: {arg: "building", eq: "base"},
        },
        gardenSuit: {
            options: ["fox", "mouse", "rabbit"],
            control: "select",
            if: {arg: "building", eq: "garden"},
        },
        obverse: {
            options: ["figure", "jewelry", "tablet"],
            control: "select",
            if: {arg: "building", eq: "waystation"}
        },
        reverse: {
            options: ["figure", "jewelry", "tablet"],
            control: "select",
            if: {arg: "building", eq: "waystation"}
        },
    },
};
