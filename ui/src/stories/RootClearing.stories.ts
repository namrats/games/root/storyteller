import { StoryObj, Meta } from '@storybook/web-components';
import { html } from 'lit';
import '../root-clearing';
import '../root.css';

export default {
  title: 'Root/Organisms/Clearing',
} as Meta;

const pieces = [
    {piece: {building: {type: "base", suit: "fox"}}, count: 1},
    {piece: {warrior: "marquise"}, count: 6},
    {piece: {warrior: "alliance"}, count: 2},
    {piece: {warrior: "eyrie"}, count: 2},
    {piece: {pawn: "vagabond1"}, count: 1},
    {piece: {token: {type: "keep"}}, count: 1},
    {piece: {token: {type: "wood"}}, count: 3},
    {piece: {token: {type: "sympathy"}}, count: 1},
]

export const Clearing: StoryObj = {
    render: (args) => {
        const landmarks = args.landmarks.map(landmark => ({piece: {landmark}, count: 1}));
        const ruins = args.ruins ? [{piece: {marker: "ruins"}}] : [];
        return html`
        <style>
            root-clearing {
                width: ${args.size}px; height: ${args.size}px;
            }
        </style>
        <root-clearing number="${args.number}" suit="${args.suit}" building-slot-count="${args.buildingSlotCount}" .pieces="${[...pieces, ...landmarks, ...ruins]}"></root-clearing>
        `;
    },
    args: {
        size: 128,
        number: 1,
        buildingSlotCount: 2,
        suit: "fox",
        ruins: false,
        landmarks: [],
    },
    argTypes: {
        size: {
            type: 'number',
            control: {
                type: 'range',
                min: 8,
                max: 128,
                step: 8,
            },
        },
        number: {
            type: "number",
            control: {
                type: "range",
                min: 1,
                max: 12,
            }
        },
        buildingSlotCount: {
            type: 'number',
            control: {
                type: 'range',
                min: 1,
                max: 3,
            },
        },
        suit: {
            options: [
                'fox',
                'mouse',
                'rabbit',
            ],
            control: {
                type: 'radio',
            },
        },
        ruins: {
            control: "boolean",
        },
        landmarks: {
            options: ["ferry", "tower", "lostCity"],
            control: "check",
        }
    },
};
