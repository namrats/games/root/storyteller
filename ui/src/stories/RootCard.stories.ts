import { StoryObj, Meta } from '@storybook/web-components';
import { html } from 'lit';
import '../root-card';
import '../root.css';

export default {
  title: 'Root/Molecules/Card',
} as Meta;

export const Card: StoryObj = {
    render: (args) => {
        const card = {deckCard: [args.suit, args.name]};
        return html`
            <style>
            root-card {
                width: ${args.size}px;
                height: ${args.size}px;
            }
            </style>
            <root-card .card="${card}"></root-card>
            `;
    },
    args: {
        size: 64,
        suit: null,
        name: null,
    },
    argTypes: {
        size: {
            type: 'number',
            defaultValue: 64,
            control: {
                type: 'range',
                min: 8,
                max: 128,
                step: 8,
            },
        },
        suit: {
            options: [null, "bird", "fox", "mouse", "rabbit"],
            defaultValue: null,
            control: {
                type: 'select',
            },
        },
        name: {
            options: [null, "Ambush", "Dominance"],
            defaultValue: null,
            control: {
                type: 'select',
            },
        }
    },
};
