import { StoryObj, Meta } from '@storybook/web-components';
import { action } from '@storybook/addon-actions';
import { html } from 'lit';
import '../root-file-download';
import '../root.css';

export default {
  title: 'Root/Molecules/Download',
} as Meta;

export const Download: StoryObj = {
    render: (args) => html`
        <style>
        </style>
        <root-file-download @download="${action("download")}"></root-file-download>
        `,
};
