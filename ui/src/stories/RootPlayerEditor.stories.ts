import { StoryObj, Meta } from '@storybook/web-components';
import { action } from '@storybook/addon-actions';
import { html } from 'lit';
import '../root-player-editor';
import '../root.css';

export default {
  title: 'Root/Molecules/PlayerEditor',
} as Meta;

const factions = {
    "none": {label: "", value: undefined},
    "marquise": {label: "Marquise", value: "marquise"},
    "eyrie": {label: "Eyrie", value: "eyrie"},
    "alliance": {label: "Alliance", value: "alliance"},
    "vagabond1": {label: "Vagabond 1", value: "vagabond1"},
    "vagabond2": {label: "Vagabond 2", value: "vagabond2"},
    "cult": {label: "Cult", value: "cult"},
    "riverfolk": {label: "Riverfolk", value: "riverfolk"},
    "duchy": {label: "Duchy", value: "duchy"},
    "corvid": {label: "Corvid", value: "corvid"},
    "hundreds": {label: "Hundreds", value: "hundreds"},
};

export const PlayerEditor: StoryObj = {
    render: (args) => {
        let score = {};
        switch (args.score) {
            case "points":
                score = {points: args.points};
                break;
            case "dominance":
                score = {dominance: args.dominance};
                break;
            case "coalition":
                score = {coalition: args.coalition};
                break;
        }
        return html`
        <style>
        </style>
        <root-player-editor faction="${args.faction}" name="${args.name}" .score="${score}" @change="${action("change")}"></root-player-editor>
        `;
    },
    args: {
        faction: "marquise",
        name: "",
        score: "points",
        points: 1,
    },
    argTypes: {
        faction: {
            options: Object.keys(factions),
            mapping: Object.fromEntries(Object.entries(factions).map(([key, value]) => [key, value.value])),
            control: {
                type: 'select',
                labels: Object.fromEntries(Object.entries(factions).map(([key, value]) => [key, value.label])),
            },
        },
        name: {
            control: 'text',
        },
        score: {
            options: [
                'points',
                'dominance',
                'coalition',
            ],
            control: "select",
        },
        points: {
            type: 'number',
            defaultValue: 1,
            control: {
                type: 'range',
                min: 0,
                max: 30,
            },
            if: { arg: 'score', eq: 'points' },
        },
        dominance: {
            options: [
                undefined,
                'fox',
                'mouse',
                'rabbit',
                'bird',
            ],
            defaultValue: 'fox',
            control: {
                type: 'radio',
                labels: {
                    undefined: 'none',
                }
            },
            if: { arg: 'score', eq: 'dominance' },
        },
        coalition: {
            options: [
                undefined,
                'marquise',
                'eyrie',
                'alliance',
                'vagabond',
                'riverfolk',
                'lizard',
                'duchy',
                'corvid',
                'hundreds',
                'keepers',
            ],
            defaultValue: 'marquise',
            control: {
                type: 'select',
                labels: {
                    undefined: 'none',
                    'marquise': 'Marquise',
                    'eyrie': 'Eyrie',
                    'alliance': 'Alliance',
                    'vagabond': 'Vagabond',
                    'riverfolk': 'Riverfolk',
                    'lizard': 'Lizard',
                    'duchy': 'Duchy',
                    'corvid': 'Corvid',
                    'hundreds': 'Hundreds',
                    'keepers': 'Keepers',
                },
            },
            if: { arg: 'score', eq: 'coalition' },
        }
    }
};
