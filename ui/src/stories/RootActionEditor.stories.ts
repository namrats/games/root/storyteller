import { StoryObj, Meta } from '@storybook/web-components';
import { action } from '@storybook/addon-actions';
import { html } from 'lit';
import '../root-action-editor';
import '../root.css';

export default {
  title: 'Root/Organisms/ActionEditor',
} as Meta;

let turns = [
    {
        faction: "marquise",
        actions: ["tk->4", "(br,bw)->6", "bs->4", "w->(1,2,4,5,6,7,8,9,10,11,12)", "3#->M"],
    },
    {
        faction: "eyrie",
        actions: ["(b,6w)->3","#despot->$","3#->E"],
    }
];

export const ActionEditor: StoryObj = {
    render: (args) => html`
        <style>
        root-action-editor {
            width: 250px;
            height: 100%;
            max-height: 100vh;
            overflow-y: scroll;
        }
        </style>
        <root-action-editor .turns="${turns}" @change=${action('change')}></root-action-editor>
        `,
};
