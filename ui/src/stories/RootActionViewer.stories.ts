import { StoryObj, Meta } from '@storybook/web-components';
import { action } from '@storybook/addon-actions';
import { html } from 'lit';
import '../root.css';
import '../root-action-viewer';

export default {
  title: 'Root/Organisms/ActionViewer',
} as Meta;

export const ActionViewer: StoryObj = {
    render: (args) => html`
        <style>
        root-action-viewer {
            width: 250px;
            height: 100%;
            max-height: 100vh;
            overflow-y: scroll;
        }
        </style>
        <root-action-viewer .turns="${turns}" playerCount="4"></root-action-viewer>
    `,
};

const turns = [{"faction":"marquise","actions":[{"type":"place","pieces":[[{"token":{"type":"keep"}},1]],"at":[{"clearing":4}],"commentary":null},{"type":"place","pieces":[[{"building":{"type":"recruiter"}},1],[{"building":{"type":"workshop"}},1]],"at":[{"clearing":6}],"commentary":null},{"type":"place","pieces":[[{"building":{"type":"sawmill"}},1]],"at":[{"clearing":4}],"commentary":null},{"type":"place","pieces":[[{"warrior":"marquise"},1]],"at":[{"clearing":1},{"clearing":2},{"clearing":4},{"clearing":5},{"clearing":6},{"clearing":7},{"clearing":8},{"clearing":9},{"clearing":10},{"clearing":11},{"clearing":12}],"commentary":null},{"type":"place","pieces":[[{"card":{"deckCard":[null,null]}},3]],"at":[{"hand":"marquise"}],"commentary":null}]},{"faction":"eyrie","actions":[{"type":"place","pieces":[[{"building":{"type":"roost"}},1],[{"warrior":"eyrie"},6]],"at":[{"clearing":3}],"commentary":null},{"type":"place","pieces":[[{"card":{"eyrieLeader":"Despot"}},1]],"at":["eyrieLeader"],"commentary":null},{"type":"place","pieces":[[{"card":{"deckCard":[null,null]}},3]],"at":[{"hand":"eyrie"}],"commentary":null}]}];
