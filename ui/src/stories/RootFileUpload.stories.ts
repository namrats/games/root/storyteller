import { StoryObj, Meta } from '@storybook/web-components';
import { action } from '@storybook/addon-actions';
import { html } from 'lit';
import '../root-file-upload';
import '../root.css';

export default {
  title: 'Root/Molecules/Upload',
} as Meta;

export const Upload: StoryObj = {
    render: (args) => html`
        <style>
        </style>
        <root-file-upload @upload="${action("upload")}"></root-file-upload>
        `,
};
