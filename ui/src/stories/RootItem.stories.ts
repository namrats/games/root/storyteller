import { StoryObj, Meta } from '@storybook/web-components';
import { html } from 'lit';
import '../root-item';
import '../root.css';

export default {
  title: 'Root/Molecules/Item',
} as Meta;

export const Item: StoryObj = {
    render: (args) => {
        const item = [args.item, args.state];
        return html`
            <style>
            root-item {
                width: ${args.size}px;
                height: ${args.size}px;
            }
            </style>
            <root-item .item="${item}" ?damaged="${args.damaged}"></root-item>
            `;
    },
    args: {
        size: 64,
        item: "bag",
        state: "refreshed",
        damaged: false,
    },
    argTypes: {
        size: {
            type: 'number',
            control: {
                type: 'range',
                min: 8,
                max: 128,
                step: 8,
            },
        },
        item: {
            options: ["bag", "boot", "coins", "crossbow", "hammer", "sword", "tea", "torch"],
            control: "select",
        },
        state: {
            options: ["refreshed", "exhausted"],
            control: "select",
        },
        damaged: {
            control: "boolean",
        }
    },
};
