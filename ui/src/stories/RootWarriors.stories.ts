import { StoryObj, Meta } from '@storybook/web-components';
import { html } from 'lit';
import '../root-warriors';
import '../root.css';

export default {
  title: 'Root/Molecules/Warriors',
} as Meta;

export const Warriors: StoryObj = {
    render: (args) => html`
        <style>
        root-warriors {
            width: ${args.size}px;
            height: ${args.size}px;
        }
        </style>
        <root-warriors faction="${args.faction}" count="${args.number}" ?warlord="${args.warlord}"></root-warriors>
    `,
    args: {
        size: 64,
        number: 1,
        faction: "marquise",
        warlord: false,
    },
    argTypes: {
        size: {
            type: 'number',
            control: {
                type: 'range',
                min: 8,
                max: 128,
                step: 8,
            },
        },
        number: {
            type: 'number',
            control: {
                type: 'range',
                min: 1,
                max: 20,
            },
        },
        faction: {
            options: [
                'marquise',
                'eyrie',
                'alliance',
                'vagabond1',
                'vagabond2',
                'riverfolk',
                'cult',
                'duchy',
                'corvid',
                'hundreds',
                'keepers',
            ],
            control: {
                type: 'select',
            },
        },
        warlord: {
            control: "boolean",
            if: { arg: "faction", eq: "hundreds" },
        },
    },
};
