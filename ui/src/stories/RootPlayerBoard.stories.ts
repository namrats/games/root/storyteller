import { StoryObj, Meta } from '@storybook/web-components';
import { html } from 'lit';
import '../root-player-board';
import '../root.css';

export default {
  title: 'Root/Organisms/Player Board',
} as Meta;

const factions = {
    "marquise": {label: "Marquise", value: "marquise"},
    "eyrie": {label: "Eyrie", value: "eyrie"},
    "alliance": {label: "Alliance", value: "alliance"},
    "vagabond1": {label: "Vagabond 1", value: "vagabond1"},
    "vagabond2": {label: "Vagabond 2", value: "vagabond2"},
    "cult": {label: "Cult", value: "cult"},
    "riverfolk": {label: "Riverfolk", value: "riverfolk"},
    "duchy": {label: "Duchy", value: "duchy"},
    "corvid": {label: "Corvid", value: "corvid"},
    "hundreds": {label: "Hundreds", value: "hundreds"},
    "keepers": {label: "Keepers", value: "keepers"},
};

export const PlayerBoard: StoryObj = {
    render: (args) => {
        let piecesByLocation = [];
        if (args.faction) {
            piecesByLocation = [
                {location: {supply: args.faction}, pieces: [
                    {piece: {warrior: args.faction}, count: args.warriorCount},
                    {piece: {token: {type: "wood"}}, count: 3},
                    {piece: {token: {type: "sympathy"}}, count: 3},
                    {piece: {building: {type: "recruiter"}}, count: 3},
                    {piece: {building: {type: "sawmill"}}, count: 3},
                    {piece: {building: {type: "workshop"}}, count: 3},
                    {piece: {building: {type: "roost"}}, count: 3},
                    {piece: {building: {type: "base", suit: "fox"}}, count: 1},
                    {piece: {building: {type: "base", suit: "mouse"}}, count: 0},
                    {piece: {building: {type: "base", suit: "rabbit"}}, count: 1},
                    {piece: {building: {type: "garden", suit: "fox"}}, count: 3},
                    {piece: {building: {type: "garden", suit: "mouse"}}, count: 5},
                    {piece: {building: {type: "garden", suit: "rabbit"}}, count: 4},
                    {piece: {token: {type: "tradePost", suit: "fox"}}, count: 2},
                    {piece: {token: {type: "tradePost", suit: "mouse"}}, count: 3},
                    {piece: {token: {type: "tradePost", suit: "rabbit"}}, count: 3},
                    {piece: {token: {type: "tunnel"}}, count: 2},
                    {piece: {token: {type: "plot"}}, count: 7},
                    {piece: {token: {type: "relic", relic: "figure", value: 2}}, count: 1},
                    {piece: {token: {type: "relic", relic: "figure", value: 3}}, count: 1},
                    {piece: {token: {type: "relic", relic: "tablet", value: 3}}, count: 2},
                ]},
                {location: {crafted: args.faction}, pieces: [
                    {piece: {item: ["sword", "refreshed"]}, count: 1},
                    {piece: {card: {deckCard: ["rabbit", "coffinMakers"]}}, count: 1},
                ]},
                {location: {eyrieDecree: "recruit"}, pieces: [{piece: {card: {deckCard: ["mouse", "bag"]}}, count: 1}]},
                {location: {eyrieDecree: "move"}, pieces: [{piece: {card: {deckCard: ["rabbit", "bag"]}}, count: 1}]},
                {location: {eyrieDecree: "battle"}, pieces: [{piece: {card: {deckCard: ["fox", "bag"]}}, count: 1}]},
                {location: {eyrieDecree: "build"}, pieces: [{piece: {card: {deckCard: ["bird", "bag"]}}, count: 1}]},
                {location: "allianceSupporters", pieces: [{piece: {card: {deckCard: [null, null]}}, count: 5}]},
                {location: "allianceOfficers", pieces: [{piece: {warrior: "alliance"}, count: 2}]},
                {location: {vagabondTrack: 1}, pieces: [{piece: {item: ["tea", "refreshed"]}, count: 1}]},
                {location: {vagabondSatchel: 1}, pieces: [{piece: {item: ["torch", "refreshed"]}, count: 1}, {piece: {item: ["sword", "exhausted"]}, count: 1}]},
                {location: {vagabondDamaged: 1}, pieces: [{piece: {item: ["sword", "refreshed"]}, count: 1}, {piece: {item: ["hammer", "exhausted"]}, count: 1}]},
                {location: {vagabondCompletedQuests: 1}, pieces: [{piece: {card: {quest: ["rabbit", "expelBandits"]}}, count: 1}]},
                {location: "cultAcolytes", pieces: [{piece: {warrior: "cult"}, count: 3}]},
                {location: "cultLostSouls", pieces: [{piece: {card: {deckCard: ["mouse", "bag"]}}, count: 1}]},
                {location: "riverfolkPayments", pieces: [{piece: {warrior: "marquise"}, count: 3}]},
                {location: "riverfolkFunds", pieces: [{piece: {warrior: "riverfolk"}, count: 3}, {piece: {warrior: "corvid"}, count: 1},]},
                {location: {riverfolkCommitments: null}, pieces: [{piece: {warrior: "riverfolk"}, count: 1}, {piece: {warrior: "eyrie"}, count: 1},]},
                {location: {riverfolkCommitments: "fox"}, pieces: [{piece: {warrior: "riverfolk"}, count: 1},]},
                {location: "burrow", pieces: [{piece: {warrior: "duchy"}, count: 5}]},
                {location: "hundredsMood", pieces: [{piece: {card: {hundredsMood: args.hundredsMood}}, count: 1}]},
                {location: "hundredsHoard", pieces: [{piece: {item: ["bag", "refreshed"]}, count: 2}, {piece: {item: ["hammer", "refreshed"]}, count: 1},]},
            ];
        }
        if (args.faction == "eyrie" && args.eyrieLeader) {
            piecesByLocation.push({location: "eyrieLeader", pieces: [{piece: {card: {eyrieLeader: args.eyrieLeader}}, count: 1}]})
        }
        if (args.faction == "vagabond1" && args.vagabond1Character) {
            piecesByLocation.push({location: {vagabondCharacter: 1}, pieces: [{piece: {card: {vagabondCharacter: args.vagabond1Character}}, count: 1}]})
        }
        if (args.faction == "vagabond2" && args.vagabond2Character) {
            piecesByLocation.push({location: {vagabondCharacter: 2}, pieces: [{piece: {card: {vagabondCharacter: args.vagabond2Character}}, count: 1}]})
        }
        if (args.faction == "riverfolk") {
            piecesByLocation.push({location: {hand: args.faction}, pieces: [
                {piece: {card: {deckCard: ["bird", "ambush"]}}, count: 1},
                {piece: {card: {deckCard: ["fox", "boot"]}}, count: 1},
                {piece: {card: {deckCard: ["rabbit", "tunnels"]}}, count: 2},
            ]});
        } else {
            piecesByLocation.push({location: {hand: args.faction}, pieces: [{piece: {card: {deckCard: [null, null]}}, count: args.handSize}]});
        }
        return html`
            <style>
            root-player-board {
                width: 300px;
                height: 70px;
            }
            </style>
            <root-player-board faction="${args.faction}" .piecesByLocation="${piecesByLocation}"></root-player-board>
        `;
    },
    args: {
        faction: "marquise",
        handSize: 3,
        warriorCount: 15,
        eyrieLeader: null,
        vagabond1Character: null,
        vagabond2Character: null,
        hundredsMood: null,
    },
    argTypes: {
        faction: {
            options: Object.keys(factions),
            mapping: Object.fromEntries(Object.entries(factions).map(([key, value]) => [key, value.value])),
            control: {
                type: 'select',
                labels: Object.fromEntries(Object.entries(factions).map(([key, value]) => [key, value.label])),
            },
        },
        handSize: {
            type: "number",
            control: {
                type: "range",
                min: 0,
                max: 10,
            }
        },
        warriorCount: {
            type: "number",
            control: {
                type: "range",
                min: 0,
                max: 25,
            }
        },
        eyrieLeader: {
            options: [null, "builder", "charismatic", "commander", "despot"],
            control: "select",
            if: { arg: "faction", eq: "eyrie" },
        },
        vagabond1Character: {
            options: [null, "adventurer", "arbiter", "harrier", "ranger", "ronin", "scoundrel", "thief", "tinker", "vagrant"],
            control: "select",
            if: { arg: "faction", eq: "vagabond1" },
        },
        vagabond2Character: {
            options: [null, "adventurer", "arbiter", "harrier", "ranger", "ronin", "scoundrel", "thief", "tinker", "vagrant"],
            control: "select",
            if: { arg: "faction", eq: "vagabond2" },
        },
        hundredsMood: {
            options: [null, "bitter", "grandiose", "jubilant", "lavish", "relentless", "rowdy", "stubborn", "wrathful"],
            control: "select",
            if: { arg: "faction", eq: "hundreds" },
        },
    }
};
