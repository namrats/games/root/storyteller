import {LitElement, html, css} from 'lit';
import {customElement, property} from 'lit/decorators.js';
import {until} from 'lit/directives/until.js';
import {unsafeHTML} from 'lit/directives/unsafe-html.js';
import {styles} from './styles';

const iconModules = import.meta.glob(
    './assets/*.svg',
    { eager: false, as: 'raw' }
);

@customElement('root-icon')
export class RootIcon extends LitElement {
    @property()
    icon: string = "";

    static styles = [
        styles,
        css`
            :host { display: inline-block; aspect-ratio: 1 / 1; }
            .container { width: 100%; height: 100%; }
            svg { width: 100%; height: 100%; }
        `,
    ];

    override render() {
        return html`
            <div class="container" part="icon">
                ${until(this._svg().then(unsafeHTML), html`...`)}
            </div>
            `;
    }

    private async _svg() {
        return await iconModules[`./assets/${this.icon}.svg`]();
    }
}
