import {LitElement, html, css} from 'lit';
import {customElement} from 'lit/decorators.js';
import {styles} from './styles';

@customElement('root-building-slot')
export class RootBuildingSlot extends LitElement {
    static styles = [
        styles,
        css`
            :host { display: block; aspect-ratio: 1 / 1; }
            rect { width: 100%; height: 100%; fill: none; stroke: black; stroke-width: 2px; }
        `,
    ]

    override render() {
        return html`
            <svg style="width: 100%; height: 100%">
                <defs>
                    <clipPath id="c">
                        <rect x="0%" y="0%" width="100%" height="100%" rx="15%" fill="#000"/>
                    </clipPath>
                </defs>
                <rect clip-path="url(#c)" x="0%" y="0%" rx="15%"/>
            </svg>
            `;
    }
}
