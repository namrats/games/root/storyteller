import {LitElement, html, css} from 'lit';
import {customElement} from 'lit/decorators.js';
import {styles} from './styles';
import './root-icon';

@customElement('root-ruins')
export class RootRuins extends LitElement {
    static styles = [
        styles,
        css`
            :host { display: block; aspect-ratio: 1 / 1; }
            .icon {
                height: 100%;
                clip-path: inset(0% round 15%);
            }
        `,
    ];

    override render() {
        return html`<root-icon class="icon" icon="ruins"></root-icon>`;
    }
}
