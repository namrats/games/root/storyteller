import {LitElement, html, css} from 'lit';
import {customElement, property} from 'lit/decorators.js';
import {map} from 'lit/directives/map.js';
import {join} from 'lit/directives/join.js';
import {when} from 'lit/directives/when.js';
import {styles} from './styles';
import {TurnRecord, Faction, Piece} from './types';
import './root-icon';
import './root-card';
import './root-item';
import './root-ruins';

@customElement('root-action-viewer')
export class RootActionViewer extends LitElement {
    @property()
    turns: TurnRecord[] = [];

    @property({type: Number})
    playerCount: number = 0;

    @property({type: Number})
    selectedTurn?: number;

    @property({type: Number})
    selectedAction?: number;

    static styles = [
        styles,
        css`
            :host { display: block; background-color: var(--brown); }
            .viewer {
                width: 100%; height: 100%;
                display: flex; flex-direction: column; gap: 8px; padding: 8px;
            }
            .round-header {
                border-bottom: 1px solid var(--beige);
                font-size: 12pt;
                color: var(--beige);
                text-transform: uppercase;
            }
            .turn {
                width: 100%;
                background-color: var(--beige);
                display: flex;
                flex-direction: column;
            }
            .turn-header {
                padding: 8px;
                color: var(--beige);
            }
            .turn-body {
                width: 100%;
                display: flex; flex-direction: column; padding: 8px; gap: 8px;
            }
            .action {
                width: 100%;
                padding: 8px;
                margin: 0;
                border: 1px solid var(--brown);
                text-decoration: inherit;
                color: inherit;
            }
            .action.selected {
                background-color: var(--brown);
                color: var(--beige);
            }
            root-icon, root-building, root-token, root-card, root-item, root-warriors, root-ruins {
                height: 1.2em;
                display: inline-block;
                vertical-align: bottom;
            }
            .action > p {
                margin: 0;
            }
            .commentary {
                font-style: italic;
            }
        `,
    ];

    override render() {
        return html`
            <div class="viewer">
                ${map(this.turns, (turn, i) => html`
                    ${when(i % this.playerCount == 0, () => this.roundHeader(Math.floor(i/this.playerCount)))}
                    <div class="turn">
                        <div class="turn-header" style="background-color: var(--${turn.faction}-color)"><root-icon icon="${turn.faction}"></root-icon></div>
                        <div class="turn-body">
                            ${map(turn.actions, (action, j) =>
                                when(
                                    this.selectedTurn == i && this.selectedAction == j,
                                    () => html`<p class="action selected">${this.explainActionWithCommentary(action)}</p>`,
                                    () => html`<a class="action" href="#view/${i}/${j}"><p>${this.explainActionWithCommentary(action)}</p></a>`))}
                        </div>
                    </div>
                    `)}
            </div>
        `;
    }

    override updated() {
        (this.renderRoot as ShadowRoot).querySelector(".selected")?.scrollIntoView({block: "center"});
    }

    private roundHeader(i: number) {
        return html`<h2 class="round-header">${when(i == 0, () => "Setup Round", () => `Round ${i}`)}</h2>`;
    }

    private explainActionWithCommentary(action: any) {
        return html`
                ${this.explainAction(action)}
                ${when(action.commentary, () => html`<span class="commentary">(${action.commentary})</span>`)}
        `;
    }

    private explainAction(action: any) {
        if (action.type == "place") {
            if (action.at[0] == "eyrieLeader") {
                return html`Choose ${this.explainQuantities(action.pieces)} leader`;
            } else if (action.at[0].vagabondCharacter) {
                return html`Choose ${this.explainQuantities(action.pieces)} character`;
            } else if (action.at[0] == "duchySwayedMinisters") {
                return html`Sway ${this.explainQuantities(action.pieces)}`;
            } else if (action.at[0] == "hundredsMood") {
                return html`Choose ${this.explainQuantities(action.pieces)} mood`;
            } else if (action.at[0].hand) {
                return html`
                    ${this.explainFaction(action.at[0].hand)}
                    draws ${this.explainQuantities(action.pieces)}
                `;
            } else if (action.at[0].crafted) {
                return html`
                    Craft ${this.explainQuantities(action.pieces)}
                `;
            } else {
                return html`
                    Place ${this.explainQuantities(action.pieces)}
                    in ${join(map(action.at, (location) => this.explainLocation(location as Location)), () => html`, `)}
                `;
            }
        } else if (action.type == "move") {
            if (action.to.crafted) {
                return html`Craft ${this.explainPieces(action.pieces)}`;
            } else if (action.to.vagabondCompletedQuests) {
                return html`Complete ${this.explainPieces(action.pieces)} quest`;
            } else if (action.to.riverfolkCommitted !== undefined) {
                return html`
                    Commit funds ${this.explainPieces(action.pieces)}
                    ${when(action.to.riverfolkCommitted, () => html`to ${this.explainSuit(action.to.riverfolkCommitted)} crafting`)}
                `;
            } else {
                return html`
                    Move ${this.explainPieces(action.pieces)}
                    from ${this.explainLocation(action.from)}
                    to ${this.explainLocation(action.to)}
                `;
            }
        } else if (action.type == "remove") {
            if (action.from[0].hand) {
                return html`
                    ${this.explainFaction(action.from[0].hand)}
                    discards ${this.explainPieces(action.pieces)}
                `;
            } else if (action.from[0].crafted) {
                    return html`
                        ${this.explainFaction(action.from[0].crafted)}
                        discards ${this.explainPieces(action.pieces)}
                        from crafted
                    `;
            } else {
                return html`
                    Remove ${this.explainPieces(action.pieces)}
                    from ${join(map(action.from, (location) => this.explainLocation(location as Location)), () => html`, `)}
                `;
            }
        } else if (action.type == "removeFromGame") {
            if (action.from?.hand) {
                return html`
                    ${this.explainFaction(action.from.hand)}
                    discards ${this.explainPieces(action.pieces)}
                    from the game
                `;
            } else if (action.from?.crafted) {
                    return html`
                        ${this.explainFaction(action.from.crafted)}
                        discards ${this.explainPieces(action.pieces)}
                        in crafted from the game
                    `;
            } else {
                return html`
                    Remove ${this.explainQuantities(action.pieces)}
                    ${when(action.from, () => html`in ${this.explainLocation(action.from)}`)}
                    from the game
                `;
            }
        } else if (action.type == "reveal") {
            return html`
                Reveal
                ${when(action.pieces, () => this.explainQuantities(action.pieces), () => "hand")}
                ${when(action.to, () => html`to ${this.explainFaction(action.to)}`)}
            `;
        } else if (action.type == "expose") {
            return html`In clearing ${action.at}, is it a ${this.explainPiece(action.piece)}?`;
        } else if (action.type == "flip") {
            return html`
                Flip ${this.explainPiece(action.obverse)}
                to ${this.explainPiece(action.reverse)}
                in ${this.explainLocation(action.at)}
            `;
        } else if (action.type == "battle") {
            return html`
                ${this.explainFaction(action.attacker)}
                ${when(action.loot, () => "loots", () => "battles")}
                ${this.explainFaction(action.defender)}
                in clearing ${action.in_}
            `;
        } else if (action.type == "rollBattleDice") {
            return html`
                Roll ${action.attacker}, ${action.defender}
            `;
        } else if (action.type == "rollMobDie") {
            return html`
                Roll ${this.explainSuit(action.suit)}
            `;
        } else if (action.type == "exhaust") {
            return html`
                Exhaust ${this.explainPieces(action.pieces)}
                ${when(action.in_, () => html`in ${this.explainLocation(action.in_)}`)}
            `;
        } else if (action.type == "moveExhaust") {
            return html`
                Exhaust ${this.explainPieces(action.pieces)}
                moving them from ${this.explainLocation(action.from)}
                to ${this.explainLocation(action.to)}
            `;
        } else if (action.type == "refresh") {
            return html`
                Refresh ${this.explainPieces(action.pieces)}
                ${when(action.in_, () => html`in ${this.explainLocation(action.in_)}`)}
            `;
        } else if (action.type == "moveRefresh") {
            return html`
                Refresh ${this.explainPieces(action.pieces)}
                moving them from ${this.explainLocation(action.from)}
                to ${this.explainLocation(action.to)}
            `;
        } else if (action.type == "scorePoints") {
            return html`
                ${this.explainFaction(action.faction)}
                scores ${action.amount} point${when(action.amount > 1, () => "s")}
            `;
        } else if (action.type == "losePoints") {
            return html`
                ${this.explainFaction(action.faction)}
                loses ${action.amount} point${when(action.amount > 1, () => "s")}
            `;
        } else if (action.type == "setRelationship") {
            return html`
                ${this.explainFaction(action.faction)}
                becomes ${action.to}
                with ${this.explainFaction(action.with)}
            `;
        } else if (action.type == "setOutcast") {
            return html`
                Set outcast to ${when(action.hated, () => "hated")}
                ${this.explainSuit(action.suit)}
            `;
        } else if (action.type == "setServicePrices") {
            return html`
                Set service prices to
                ${action.hand_card} for cards,
                ${action.riverboats} for riverboats
                and ${action.mercenaries} for mercenaries
            `;
        } else if (action.type == "activateDominance") {
            return html`
                Activate ${this.explainPiece({card: {deckCard: [action.suit, "Dominance"]}})}
                ${when(action.coalition, () => html`to form coalition with ${this.explainFaction(action.coalition)}`)}
            `;
        } else if (action.type == "refillDrawPile") {
            return html`Refill draw pile`;
        } else {
            return html`${JSON.stringify(action)}`;
        }
    }

    private explainFaction(faction: Faction) {
        return html`<root-icon icon="${faction}" style="color: var(--${faction}-color)"></root-icon>`;
    }

    private explainSuit(suit: string) {
        return html`<root-icon icon="${suit}-wborder" style="color: var(--${suit}-color)"></root-icon>`;
    }

    private explainPieces(pieces: any) {
        if (pieces == "all") {
            return "all";
        } else if (pieces.specific) {
            return this.explainQuantities(pieces.specific);
        } else {
            return null;
        }
    }

    private explainQuantities(quantities: any[]) {
        return join(map(quantities, (q) => this.explainQuantity(q)), () => html`, `)
    }

    private explainQuantity([piece, count]: [Piece, number]) {
        return html`${when(count > 1, () => html`${count}×`)}${this.explainPiece(piece)}`;
    }

    private explainPiece(piece: Piece) {
        if (piece.warrior) {
            return this.explainFaction(piece.warrior);
        } else if (piece.pawn) {
            return this.explainFaction(piece.pawn);
        } else if (piece.hundredsPiece == "warlord") {
            return html`<root-warriors faction="hundreds" warlord></root-warriors>`;
        } else if (piece.building) {
            return html`<root-building .building="${piece.building}"></root-building>`;
        } else if (piece.token) {
            return html`<root-token .token="${piece.token}"></root-token>`;
        } else if (piece.item) {
            return html`<root-item .item="${piece.item}"></root-item>`;
        } else if (piece.card?.deckCard) {
            return html`<root-card .card="${piece.card}"></root-card> ${when(piece.card?.deckCard?.[1], () => piece.card?.deckCard?.[1])}`;
        } else if (piece.card?.eyrieLeader) {
            return html`<root-card .card="${piece.card}"></root-card> ${piece.card.eyrieLeader}`;
        } else if (piece.card?.eyrieCard) {
            return html`<root-card .card="${piece.card}"></root-card> ${piece.card.eyrieCard}`;
        } else if (piece.card?.vagabondCharacter) {
            return html`<root-card .card="${piece.card}"></root-card> ${piece.card.vagabondCharacter}`;
        } else if (piece.card?.quest) {
            return html`<root-card .card="${piece.card}"></root-card> ${piece.card.quest[1]}`;
        } else if (piece.card?.duchyMinister) {
            return html`<root-card .card="${piece.card}"></root-card> ${piece.card.duchyMinister}`;
        } else if (piece.card?.hundredsMood) {
            return html`<root-card .card="${piece.card}"></root-card> ${piece.card.hundredsMood}`;
        } else if (piece.card?.keepersCard) {
            return html`<root-card .card="${piece.card}"></root-card> ${piece.card.keepersCard}`;
        } else if (piece.marker == "ruins") {
            return html`<root-ruins></root-ruins>`;
        } else if (piece.marker == "closedPath") {
            return "closed path";
        } else if (piece.landmark) {
            return html`<root-icon .icon="${piece.landmark}" style="color: var(--${piece.landmark}-color)"></root-icon>`
        } else {
            return JSON.stringify(piece);
        }
    }

    private explainLocation(location: any) {
        if (!location) {
            return null;
        } else if (location == "availableDominanceCards") {
            return "available dominance cards";
        } else if (location == "eyrieLeader") {
            return html`${this.explainFaction("eyrie")} leader`;
        } else if (location == "allianceSupporters") {
            return html`${this.explainFaction("alliance")} supporters`;
        } else if (location == "allianceOfficers") {
            return html`${this.explainFaction("alliance")} officers`;
        } else if (location == "availableQuests") {
            return "available quests";
        } else if (location == "cultLostSouls") {
            return html`${this.explainFaction("cult")} lost souls`;
        } else if (location == "cultAcolytes") {
            return html`${this.explainFaction("cult")} acolytes`;
        } else if (location == "riverfolkPayments") {
            return html`${this.explainFaction("riverfolk")} payments`;
        } else if (location == "riverfolkFunds") {
            return html`${this.explainFaction("riverfolk")} funds`;
        } else if (location == "burrow") {
            return "the burrow";
        } else if (location == "hundredsMood") {
            return html`${this.explainFaction("hundreds")} mood`;
        } else if (location == "hundredsHoard") {
            return html`${this.explainFaction("hundreds")} hoard`;
        } else if (location.clearing) {
            return html`clearing ${location.clearing}`;
        } else if (location.path) {
            return html`path from ${location.path[0]} to ${location.path[1]}`;
        } else if (location.forest) {
            return html`forest ${join(map(location.forest, (c) => c), () => "-")}`;
        } else if (location.hand) {
            return this.explainFaction(location.hand);
        } else if (location.crafted) {
            return html`${this.explainFaction(location.crafted)} crafted`;
        } else if (location.eyrieDecree) {
            return html`${this.explainFaction("eyrie")} ${location.eyrieDecree} column`;
        } else if (location.vagabondCompletedQuests) {
            return html`${this.explainFaction(`vagabond${location.vagabondCompletedQuests}`)} completed quests`;
        } else if (location.vagabondTrack) {
            return html`${this.explainFaction(`vagabond${location.vagabondTrack}`)} track`;
        } else if (location.vagabondSatchel) {
            return html`${this.explainFaction(`vagabond${location.vagabondSatchel}`)} satchel`;
        } else if (location.vagabondDamaged) {
            return html`${this.explainFaction(`vagabond${location.vagabondDamaged}`)} damaged`;
        } else if (location.riverfolkCommitted !== undefined) {
            return html`${this.explainFaction("riverfolk")} committed`;
        } else if (location.keepersRetinue) {
            return html`${location.keepersRetinue} column`;
        } else {
            return JSON.stringify(location);
        }
    }
}
