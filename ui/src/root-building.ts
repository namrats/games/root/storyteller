import {LitElement, html, css} from 'lit';
import {customElement, property} from 'lit/decorators.js';
import {styles} from './styles';
import {Building} from './types';
import './root-icon';

@customElement('root-building')
export class RootBuilding extends LitElement {
    @property()
    building!: Building;

    static styles = [
        styles,
        css`
            :host { display: inline-block; aspect-ratio: 1 / 1; }
            .container { position: relative; width: 100%; height: 100%; }
            .cardboard { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
            .icon {
                position: absolute; top: 5%; left: 5%; width: 90%; height: 90%;
                z-index: 1;
                color: white;
            }
            .base-decoration {
                position: absolute; top: 25%; left: 50%; width: 60%; height: 60%; translate: -50% 0;
                z-index: 2;
            }
            .garden-decoration {
                position: absolute; top: 50%; left: 50%; width: 40%; height: 40%; translate: -50% -50%;
                z-index: 2;
            }
            .waystation-obverse-decoration {
                position: absolute; top: 38%; left: 43%; width: 40%; height: 40%; translate: -50% -50%;
                z-index: 2;
            }
            .waystation-reverse-decoration {
                position: absolute; top: 70%; left: 69.6%; width: 20%; height: 20%; translate: -50% -50%;
                z-index: 2;
            }
        `,
    ];

    override render() {
        return html`
            <style>
                .cardboard rect { fill: var(--${this._faction()}-color); }
                .base-decoration, .garden-decoration { color: var(--${this.building.suit}-color); }
            </style>
            <div class="container">
                <svg class="cardboard">
                    <rect x="0" y="0" width="100%" height="100%" rx="15%"/>
                </svg>
                <root-icon class="icon" icon="${this.building.type}"></root-icon>
                ${this.renderDecorations()}
            </div>
            `;
    }

    private _faction() {
        switch (this.building.type) {
            case "recruiter": case "sawmill": case "workshop":
                return "marquise";
            case "roost":
                return "eyrie";
            case "base":
                return "alliance";
            case "garden":
                return "cult";
            case "citadel": case "market":
                return "duchy";
            case "stronghold":
                return "hundreds";
            case "waystation":
                return "keepers";
        }
        return null;
    }

    private renderDecorations() {
        if (this.building.type === "base" || this.building.type === "garden") {
            return html`
            <root-icon class="${this.building.type}-decoration" icon="${this.building.suit}-wborder"></root-icon>
            `;
        } else if (this.building.type === "waystation") {
            return html`
            <root-icon class="${this.building.type}-obverse-decoration" icon="${this.building.obverse}"></root-icon>
            <root-icon class="${this.building.type}-reverse-decoration" icon="${this.building.reverse}"></root-icon>
            `;
        } else {
            return null;
        }
    }

}
