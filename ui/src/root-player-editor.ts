import {LitElement, html, css} from 'lit';
import {customElement, property} from 'lit/decorators.js';
import {when} from 'lit/directives/when.js';
import {styles} from './styles';
import {Faction} from './types';
import './root-icon';
import {RootFactionSelect} from './root-faction-select';
import './root-faction-select';

@customElement('root-player-editor')
export class RootPlayerEditor extends LitElement {
    @property({type: Boolean})
    edit: boolean = false;

    @property()
    faction?: Faction;

    @property({type: String})
    name?: string;

    @property({type: Object})
    score?: any;

    static styles = [
        styles,
        css`
            :host { display: block; }
            .container {
                width: 100%;
                height: 100%;
                display: flex;
                flex-flow: row nowrap;
                align-items: baseline;
                gap: 8px;
                padding: 8px;
            }
            root-icon { height: 1.2em; vertical-align: bottom; }
            input { width: 100px; }
        `,
    ];

    override render() {
        return html`
            <style>
                .container {
                    background-color: ${this.backgroundColor()};
                    color: ${this.color()};
                }
            </style>
            <div class="container">
                ${when(
                    this.edit,
                    () => html`
                        <root-faction-select faction="${this.faction}" @change="${this._onFactionChange}"></root-faction-select>
                        <input value="${this.name}" @input="${this._onNameChange}" />
                        `,
                    () => html`
                        <root-icon icon="${this.faction}"></root-icon>
                        <span>${this.name}</span>
                        `,
                )}
                <span>${this.renderPlayerScore()}</span>
                <slot></slot>
            </div>
            `;
    }

    private backgroundColor() {
        if (this.faction) {
            return `var(--${this.faction}-color)`;
        } else {
            return "var(--beige)";
        }
    }

    private color() {
        if (!this.faction || ["vagabond2", "cult", "duchy"].includes(this.faction)) {
            return "var(--brown)";
        } else {
            return "var(--beige)";
        }
    }

    renderPlayerScore() {
        if (this.score !== undefined) {
            if (this.score.points !== undefined) {
                return html`${this.score.points}pts`;
            } else if (this.score.dominance !== undefined) {
                return html`<root-icon icon="${this.score.dominance}" style="color: ${this.color()}"></root-icon>`;
            } else if (this.score.coalition !== undefined) {
                return html`<root-icon icon="${this.score.coalition}" style="color: var(--${this.score.coalition}-color)"></root-icon>`;
            } else {
                return html``;
            }
        } else {
            return html`0pts`;
        }
    }

    private _onFactionChange(event: Event) {
        this.faction = (event.target as RootFactionSelect).faction;
        this.dispatchChange();
    }

    private _onNameChange(event: Event) {
        this.name = (event.target as HTMLInputElement).value;
        this.dispatchChange();
    }

    private dispatchChange() {
        this.dispatchEvent(new Event('change'));
    }
}
