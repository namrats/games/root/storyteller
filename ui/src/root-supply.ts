import {LitElement, html, css} from 'lit';
import {customElement, property} from 'lit/decorators.js';
import {map} from 'lit/directives/map.js';
import {when} from 'lit/directives/when.js';
import {styles} from './styles';
import {LocatedPieces} from './types';
import './root-item';
import './root-card';

@customElement('root-supply')
export class RootSupply extends LitElement {
    @property()
    piecesByLocation: LocatedPieces[] = [];

    static styles = [
        styles,
        css`
            :host { display: block; }
            .container {
                width: 100%;
                display: flex; flex-direction: column; gap: 8px; padding: 8px;
                background-color: var(--beige);
            }
            .component { margin: 0; }
            root-card, root-item {
                height: 1.5em;
                display: inline-block;
                vertical-align: bottom;
            }
        `,
    ];

    override render() {
        const items = this.piecesByLocation
            .find(({location}) => typeof location != "string" && location.supply === null)
            ?.pieces
            ?.flatMap(({piece, count}) => {
                if (piece.item) {
                    return new Array(count).fill(piece.item);
                } else {
                    return [];
                }
            })
            ?.sort((i1, i2) => i1[0].localeCompare(i2[0]))
            || [];
        const drawPileCount = this.piecesByLocation.find(({location}) => location == "drawPile")?.pieces?.[0]?.count || 0;
        const discard = this.piecesByLocation
            .find(({location}) => location == "discard")
            ?.pieces
            ?.flatMap(({piece, count}) => {
                if (piece.card) {
                    return new Array(count).fill(piece.card);
                } else {
                    return [];
                }
            })
            || [];
        const availableDominanceCards = this.piecesByLocation
            .find(({location}) => location == "availableDominanceCards")
            ?.pieces
            ?.flatMap(({piece, count}) => {
                if (piece.card) {
                    return new Array(count).fill(piece.card);
                } else {
                    return [];
                }
            })
            || [];
        const availableQuests = this.piecesByLocation
            .find(({location}) => location == "availableQuests")
            ?.pieces
            ?.flatMap(({piece, count}) => {
                if (piece.card) {
                    return new Array(count).fill(piece.card);
                } else {
                    return [];
                }
            })
            || [];

        return html`
            <div class="container">
                <p class="component">${drawPileCount}×<root-card></root-card></p>
                <p class="component">D:&nbsp;${map(discard, (card) => html`<root-card .card="${card}"></root-card>`)}</p>
                <p class="component">Dom:&nbsp;${map(availableDominanceCards, (card) => html`<root-card .card="${card}"></root-card>`)}</p>
                <p class="component">
                    ${map(items, (item) => html`<root-item .item="${item}"></root-item>`)}
                </p>

                ${when(availableQuests.length, () => html`
                <p class="component">
                    Q:&nbsp;${map(availableQuests, (quest) => html`<root-card .card="${quest}"></root-card>`)}
                </p>
                `)}
            </div>
            `;
    }
}
