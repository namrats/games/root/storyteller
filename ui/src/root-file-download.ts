import {LitElement, html, css} from 'lit';
import {customElement} from 'lit/decorators.js';
import {styles} from './styles'
import './root-icon';

@customElement('root-file-download')
export class RootFileDownload extends LitElement {
    static styles = [
        styles,
        css`:host { display: block; }`,
    ];

    override render() {
        return html`<button class="wide" @click="${this._onClick}">Download</button>`;
    }

    private _onClick() {
        this.dispatchEvent(new Event("download"));
    }
}
