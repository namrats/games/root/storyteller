import {LitElement, html, css} from 'lit';
import {customElement, property} from 'lit/decorators.js';
import {map} from 'lit/directives/map.js';
import {when} from 'lit/directives/when.js';
import {styles} from './styles';
import {Quantity} from './types';
import './root-icon';
import './root-building';
import './root-building-slot';
import './root-ruins';
import './root-warriors';
import './root-token';

@customElement('root-clearing')
export class RootClearing extends LitElement {
    @property()
    number?: number;

    @property()
    suit: string = "";

    @property({type: Number, attribute: "building-slot-count"})
    buildingSlotCount: number = 1;

    @property()
    pieces: Quantity[] = [];

    warriorPositions = [
        {x: 20, y: 20},
        {x: 80, y: 20},
        {x: 20, y: 80},
        {x: 80, y: 80},
        {x: 50, y: 20},
    ];

    tokenPositions = [
        {x: 10, y: 50},
        {x: 30, y: 70},
        {x: 90, y: 50},
        {x: 70, y: 70},
    ];

    static styles = [
        styles,
        css`
            :host { display: block; }
            .container { position: relative; height: 100%; width: 100%; }
            .border { aspect-ratio: 1 / 1 }
            .border circle { fill: var(--beige); stroke: var(--dark-brown); stroke-width: 2px; }
            .clearing-number { position: absolute; top: 0; left: 0; height: 20%; translate: 0 -50%; }
            .clearing-number polygon { fill: black; }
            .suit {
                position: absolute; top: 0; left: 50%; width: 32px; height: 32px; translate: -50% -50%;
                z-index: 4;
            }
            .building-container {
                position: absolute; top: 20%; left: 20%; width: 60%; height: 60%;
                aspect-ratio: 1 / 1;
                display: flex; flex-wrap: wrap; gap: 5%;
                align-content: center; justify-content: center;
                z-index: 2;
            }
            .ferry {
                position: absolute; top: 100%; left: 50%; width: 30%; height: 30%; translate: -50% -50%;
                color: var(--ferry-color);
                z-index: 4;
            }
            .tower {
                position: absolute; top: 80%; left: 50%; width: 30%; height: 30%; translate: -50% -50%;
                color: var(--tower-color);
                z-index: 4;
            }
            root-building-slot, root-building, root-ruins { height: 35%; }
            root-warriors { height: 25%; translate: -50% -50%; z-index: 3; }
            root-token { height: 20%; translate: -50% -50%; }
            text {
                font-size: 5pt;
                fill: white;
                text-anchor: middle;
            }
        `,
    ];

    override render() {
        return html`
            <div class="container">
                <svg class="border" viewBox="0 0 100 100">
                    <circle cx="50" cy="50" r="45"/>
                </svg>
                ${when(
                    this.number !== undefined,
                    () => html`
                        <svg class="clearing-number" viewBox="0 0 15 10">
                            <polygon points="0,5 2.5,0 8.5,0 11,5 8.5,10 2.5,10"/>
                            <text x="5.5" y="7.2">${this.number}</text>
                        </svg>
                    `
                )}
                ${when(
                    this.pieces.find(({piece, count}) => piece.landmark === "lostCity" && count > 0),
                    () => html`<root-icon class="suit" icon="lostCity" style="color: var(--lostCity-color);"></root-icon>`,
                    () => html`<root-icon class="suit" icon="${this.suit}-wborder" style="color: var(--${this.suit}-color);"></root-icon>`
                )}
                <div class="building-container">
                    ${this.renderRuinsBuildings()}
                </div>
                ${this.renderWarriors()}
                ${this.renderTokens()}
                ${when(this.pieces.find(({piece, count}) => piece.landmark === "ferry" && count > 0), () => html`<root-icon class="ferry" icon="ferry"></root-icon>`)}
                ${when(this.pieces.find(({piece, count}) => piece.landmark === "tower" && count > 0), () => html`<root-icon class="tower" icon="tower" style="color: var(--tower-color);"></root-icon>`)}
            </div>
            `;
    }

    private renderRuinsBuildings() {
        const pieces =
            this.pieces
            .filter(({piece}) => piece.building !== undefined || piece.marker === "ruins")
            .flatMap(({piece, count}) => Array(count).fill(piece.building || piece.marker));

        return map(
            Array(this.buildingSlotCount).keys(),
            i => {
                if (i < pieces.length) {
                    const b = pieces[i];
                    if (b === "ruins") {
                        return html`<root-ruins></root-ruins>`;
                    } else {
                        return html`<root-building .building="${b}"></root-building>`;
                    }
                } else {
                    return html`<root-building-slot></root-building-slot>`;
                }
            }
        );
    }

    private renderWarriors() {
        const warriors =
            new Map(this.pieces
                .filter(({piece, count}) => (piece.warrior !== undefined || piece.pawn !== undefined) && count > 0)
                .map(({piece, count}) => ([piece.warrior || piece.pawn, count])));
        const warlord =
            Boolean(this.pieces.find(({piece, count}) => piece.hundredsPiece == "warlord" && count > 0));
        if (warlord) {
            warriors.set("hundreds", (warriors.get("hundreds") || 0) + 1);
        }
        return map(
            warriors, ([faction, count], i) => {
                let p = this.warriorPositions[i];
                return html`
                <root-warriors faction="${faction}" count="${count}" ?warlord="${faction == "hundreds" && warlord}" style="position: absolute; top: ${p.y}%; left: ${p.x}%;"></root-warriors>
                `;
            }
        )
    }

    private renderTokens() {
        const tokens =
            this.pieces
            .filter(({piece, count}) => piece.token !== undefined && count > 0)
            .map(({piece, count}) => ({token: piece.token, count}))
        return map(
            tokens, ({token, count}, i) => {
                let p = this.tokenPositions[i];
                return html`
                <root-token .token="${token}" count="${count}" style="position: absolute; top: ${p.y}%; left: ${p.x}%;" />
                `;
            }
        )
    }
}
