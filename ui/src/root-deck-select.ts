import {LitElement, html, css} from 'lit';
import {customElement, property} from 'lit/decorators.js';
import {map} from 'lit/directives/map.js';
import {styles} from './styles';

@customElement('root-deck-select')
export class RootDeckSelect extends LitElement {
    deckOptions = { base: "Base", enp: "E&P" }

    @property()
    value: string = "Base";

    static styles = [
        styles,
        css`:host { display: inline-block; }`,
    ];

    override render() {
        return html`
            <select name="deckName">
                ${map(Object.entries(this.deckOptions), ([value, label]) => html`
                    <option value=${value} ?selected="${value == this.value}">${label}</option>
                    `)}
            </select>
        `;
    }
}
