import {LitElement, html, css} from 'lit';
import {customElement, property} from 'lit/decorators.js';
import {map} from 'lit/directives/map.js';
import {produce} from 'immer';
import {styles} from './styles';
import {TurnText} from './types';
import {RootFactionSelect} from './root-faction-select';
import './root-faction-select';

@customElement('root-action-editor')
export class RootActionEditor extends LitElement {
    @property()
    turnOrder?: string[];

    @property()
    turns: TurnText[] = [];

    static styles = [
        styles,
        css`
            :host { display: block; background-color: var(--brown); }
            .container {
                height: 100%;
                display: flex; flex-direction: column; gap: 8px; padding: 8px;
            }
            .turn { background-color: var(--beige); display: flex; flex-direction: column; }
            .turn-header { padding: 8px; }
            .turn-body { display: flex; flex-direction: column; gap: 8px; padding: 8px; }
            .action { display: flex; flex-direction: row; }
            .spacer { width: 1px; height: 48px; }
            input { width: 180px; }
        `,
    ];

    override render() {
        return html`
            <div class="container">
                ${map(this.turns, (turn, i) => this.renderTurn(turn, i))}
                <button @click="${() => this.addTurn()}">+turn</button>
                <div class="spacer">&nbsp;</div>
            </div>
            `;
    }

    renderTurn(turn: TurnText, i: number) {
        return html`
            <div class="turn">
                <div class="turn-header" style="background-color: var(--${turn.faction}-color);">
                    <root-faction-select faction="${turn.faction}" @change="${(e: Event) => this._onFactionChange(e, i)}"></root-faction-select>
                </div>
                <div class="turn-body">
                    ${map(turn.actions, (action, j) => this.renderAction(action, i, j))}
                    <button @click="${() => this.addAction(i)}">+action</button>
                </div>
            </div>
        `;
    }

    renderAction(action: string, i: number, j: number) {
        return html`
            <div class="action">
                <input type="text" .value="${action}" @input=${(e: Event) => this._onInputChange(e, i, j)} />
                <button @click="${() => this.removeAction(i, j)}">x</button>
            </div>
        `;
    }

    private addTurn() {
        this.turns = produce(this.turns, (draft) => {
            let faction;
            if (this.turnOrder !== undefined) {
                if (draft.length) {
                    const lastFaction = draft.slice(-1)[0].faction;
                    if (lastFaction) {
                        const i = this.turnOrder.indexOf(lastFaction);
                        faction = this.turnOrder[(i+1)%this.turnOrder.length];
                    } else {
                        faction = this.turnOrder[0];
                    }
                } else {
                    faction = this.turnOrder[0];
                }
            }
            draft.push({faction, actions: []});
        });
        this.dispatchEvent(new Event("change"));
    }

    private _onInputChange(event: Event, i: number, j: number) {
        this.turns = produce(this.turns, (draft) => {
            draft[i].actions[j] = (event.target as HTMLInputElement).value;
        });
        this.dispatchEvent(new Event("change"));
    }

    private _onFactionChange(event: Event, i: number) {
        this.turns = produce(this.turns, (draft) => {
            draft[i].faction = (event.target as RootFactionSelect).faction;
        });
        this.dispatchEvent(new Event("change"));
    }

    private addAction(i: number) {
        this.turns = produce(this.turns, (draft) => {
            draft[i].actions.push("");
        });
        this.dispatchEvent(new Event("change"));
    }

    private removeAction(i: number, j: number) {
        this.turns = produce(this.turns, (draft) => {
            draft[i].actions.splice(j, 1);
        });
        this.dispatchEvent(new Event("change"));
    }
}
