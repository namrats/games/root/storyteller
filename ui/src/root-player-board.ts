import {LitElement, html, css} from 'lit';
import {customElement, property} from 'lit/decorators.js';
import {map} from 'lit/directives/map.js';
import {when} from 'lit/directives/when.js';
import {styles} from './styles';
import {Faction, Token, Building, LocatedPieces} from './types';
import './root-icon';
import './root-item';
import './root-card';
import './root-warriors';

@customElement('root-player-board')
export class RootPlayerBoard extends LitElement {
    @property()
    faction?: Faction;

    @property()
    piecesByLocation: LocatedPieces[] = [];

    static styles = [
        styles,
        css`
            :host { display: block; }
            .container {
                width: 100%;
                display: flex; flex-flow: row wrap; gap: 8px; padding: 8px;
                align-items: baseline;
                color: var(--brown);
                background-color: var(--beige);
            }
            root-card, root-token, root-building, root-item, root-warriors, root-icon {
                height: 1.5em;
                display: inline-block;
                vertical-align: bottom;
            }
        `,
    ];

    override render() {
        if (this.faction) {
            return html`
                <style>
                </style>
                <div class="container">
                    ${this._renderBoard()}
                </div>
            `;
        } else {
            return null;
        }
    }

    private _renderBoard() {
        switch (this.faction) {
            case "marquise": return this._renderMarquiseBoard();
            case "eyrie": return this._renderEyrieBoard();
            case "alliance": return this._renderAllianceBoard();
            case "vagabond1": return this._renderVagabondBoard(1);
            case "vagabond2": return this._renderVagabondBoard(2);
            case "cult": return this._renderCultBoard();
            case "riverfolk": return this._renderRiverfolkBoard();
            case "duchy": return this._renderDuchyBoard();
            case "corvid": return this._renderCorvidBoard();
            case "hundreds": return this._renderHundredsBoard();
            case "keepers": return this._renderKeepersBoard();
            default: return null;
        }
    }

    private _renderMarquiseBoard() {
        return html`
        ${this._renderSecretHand()}
        ${this._renderWarriorCount()}
        ${this._renderTokenCount({type: "wood"})}
        ${this._renderBuildingCount({type: "sawmill"})}
        ${this._renderBuildingCount({type: "workshop"})}
        ${this._renderBuildingCount({type: "recruiter"})}
        ${this._renderCrafted()}
        `;
    }

    private _renderEyrieBoard() {
        return html`
        ${this._renderEyrieLeader()}
        ${this._renderSecretHand()}
        ${this._renderWarriorCount()}
        ${this._renderBuildingCount({type: "roost"})}
        ${this._renderCrafted()}
        ${this._renderEyrieDecree()}
        `;
    }

    private _renderAllianceBoard() {
        return html`
        ${this._renderSecretHand()}
        ${this._renderWarriorCount()}
        ${this._renderTokenCount({type: "sympathy"})}
        ${this._renderBuildingCount({type: "base", suit: "fox"})}
        ${this._renderBuildingCount({type: "base", suit: "mouse"})}
        ${this._renderBuildingCount({type: "base", suit: "rabbit"})}
        ${this._renderCrafted()}
        ${this._renderSupportersCount()}
        ${this._renderOfficersCount()}
        `;
    }

    private _renderVagabondBoard(i: number) {
        return html`
        ${this._renderVagabondCharacter(i)}
        ${this._renderSecretHand()}
        ${this._renderVagabondTrack(i)}
        ${this._renderVagabondSatchel(i)}
        ${this._renderCrafted()}
        ${this._renderVagabondCompletedQuests(i)}
        `;
    }

    private _renderCultBoard() {
        return html`
        ${this._renderSecretHand()}
        ${this._renderWarriorCount()}
        ${this._renderBuildingCount({type: "garden", suit: "fox"})}
        ${this._renderBuildingCount({type: "garden", suit: "mouse"})}
        ${this._renderBuildingCount({type: "garden", suit: "rabbit"})}
        ${this._renderCrafted()}
        ${this._renderCultAcolyteCount()}
        ${this._renderCultLostSouls()}
        `;
    }

    private _renderRiverfolkBoard() {
        return html`
        ${this._renderPublicHand()}
        ${this._renderWarriorCount()}
        ${this._renderTokenCount({type: "tradePost", suit: "fox"})}
        ${this._renderTokenCount({type: "tradePost", suit: "mouse"})}
        ${this._renderTokenCount({type: "tradePost", suit: "rabbit"})}
        ${this._renderCrafted()}
        ${this._renderRiverfolkEconomy()}
        `;
    }

    private _renderDuchyBoard() {
        return html`
        ${this._renderSecretHand()}
        ${this._renderWarriorCount()}
        ${this._renderBuildingCount({type: "citadel"})}
        ${this._renderBuildingCount({type: "market"})}
        ${this._renderTokenCount({type: "tunnel"})}
        ${this._renderCrafted()}
        ${this._renderBurrow()}
        ${this._renderDuchySwayedMinisters()}
        `;
    }

    private _renderCorvidBoard() {
        return html`
        ${this._renderSecretHand()}
        ${this._renderWarriorCount()}
        ${this._renderTokenCount({type: "plot", plot: null})}
        ${this._renderCrafted()}
        `;
    }

    private _renderHundredsBoard() {
        return html`
        ${this._renderHundredsMood()}
        ${this._renderSecretHand()}
        ${this._renderWarriorCount()}
        ${this._renderBuildingCount({type: "stronghold"})}
        ${this._renderTokenCount({type: "mob"})}
        ${this._renderCrafted()}
        ${this._renderHundredsHoard()}
        `;
    }

    private _renderKeepersBoard() {
        return html`
        ${this._renderSecretHand()}
        ${this._renderWarriorCount()}
        ${this._renderCrafted()}
        ${this._renderKeepersRecoveredRelics()}
        ${this._renderKeepersRetinue()}
        `;
    }

    private _renderSecretHand() {
        const handSize = this.piecesByLocation
            .find(({location}) => typeof location !== "string" && location.hand == this.faction)
            ?.pieces[0]
            ?.count
            || 0;
        return html`
        <div>
            ${handSize}×<root-card .card="${{deckCard: [null, null]}}"></root-card>
        </div>
        `;
    }

    private _renderPublicHand() {
        const cards = this.piecesByLocation
            .find(({location}) => typeof location !== "string" && location.hand == this.faction)
            ?.pieces
            ?.flatMap(({piece, count}) => {
                if (piece.card) {
                    return new Array(count).fill(piece.card);
                } else {
                    return [];
                }
            })
            || [];
        return html`
        <div>
            ${map(cards, (card) => html`<root-card .card="${card}"></root-card>`)}
        </div>
        `;
    }

    private _renderCrafted() {
        const crafted = this.piecesByLocation
            .find(({location}) => typeof location !== "string" && location.crafted == this.faction)
            ?.pieces;
        const craftedItems = crafted
            ?.flatMap(({piece, count}) => {
                if (piece.item) {
                    return new Array(count).fill(piece.item);
                } else {
                    return [];
                }
            })
            || [];
        const craftedImprovements = crafted
            ?.flatMap(({piece, count}) => {
                if (piece.card) {
                    return new Array(count).fill(piece.card);
                } else {
                    return []
                }
            })
            || [];

        return html`
        <div>
            ${map(craftedItems, (item) => html`<root-item .item="${item}"></root-item>`)}
        </div>
        <div>
            ${map(craftedImprovements, (card) => html`<root-card .card="${card}"></root-card>`)}
        </div>
        `;
    }

    private _renderWarriorCount() {
        const warriorCount = this.piecesByLocation
            .find(({location}) => typeof location !== "string" && location.supply == this.faction)
            ?.pieces
            ?.find(({piece}) => piece.warrior == this.faction)
            ?.count
            || 0;
        return html`
        <div>
            ${warriorCount}×<root-icon icon="${this.faction}" style="color: var(--${this.faction}-color)"></root-icon>
        </div>
        `;
    }

    private _renderTokenCount(token: Token) {
        const tokenCount = this.piecesByLocation
            .find(({location}) => typeof location !== "string" && location.supply == this.faction)
            ?.pieces
            ?.find(({piece}) => piece.token !== undefined && JSON.stringify(piece.token) == JSON.stringify(token))
            ?.count
            || 0;
        return html`
        <div>
            ${tokenCount}×<root-token .token="${token}"></root-token>
        </div>
        `;
    }

    private _renderBuildingCount(building: Building) {
        const buildingCount = this.piecesByLocation
            .find(({location}) => typeof location !== "string" && location.supply == this.faction)
            ?.pieces
            ?.find(({piece}) => piece.building !== undefined && piece.building.type == building.type && (building.suit === undefined || piece.building.suit == building.suit))
            ?.count
            || 0;
        return html`
        <div>
            ${buildingCount}×<root-building .building="${building}"></root-building>
        </div>
        `;
    }

    private _renderEyrieLeader() {
        const leader = this.piecesByLocation
            .find(({location}) => location == "eyrieLeader")
            ?.pieces[0]?.piece?.card?.eyrieLeader;
        return html`<div>${leader}</div>`;
    }

    private _renderEyrieDecree() {
        const decree = ["recruit", "move", "battle", "build"]
            .map((column) => this.piecesByLocation.find(({location}) => typeof location !== "string" && location.eyrieDecree == column)?.pieces?.flatMap(({piece, count}) => {
                if (piece.card) {
                    return new Array(count).fill(piece.card);
                } else {
                    return [];
                }
            }) || []);
        return html`
            <div>
                D: ${map(decree, (column, i) => html`
                    ${when(i > 0, () => html`|`)}
                    ${map(column, (card) => html`<root-card .card="${card}"></root-card>`)}
                `)}
            </div>
        `;
    }

    private _renderSupportersCount() {
        const supportersCount = this.piecesByLocation
            .find(({location}) => location == "allianceSupporters")
            ?.pieces[0]
            ?.count || 0;
        return html`
            <div>
                ${supportersCount}×S
            </div>
            `;
    }

    private _renderOfficersCount() {
        const officersCount = this.piecesByLocation
            .find(({location}) => location == "allianceOfficers")
            ?.pieces[0]
            ?.count || 0;
        return html`
            <div>
                ${officersCount}×O
            </div>
            `;
    }

    private _renderVagabondCharacter(i: number) {
        const character = this.piecesByLocation
            .find(({location}) => typeof location !== "string" && location.vagabondCharacter == i)
            ?.pieces[0]?.piece?.card?.vagabondCharacter;
        return html`<div>${character}</div>`;
    }

    private _renderVagabondTrack(i: number) {
        const pieces = this.piecesByLocation
            .find(({location}) => typeof location !== "string" && location.vagabondTrack == i)
            ?.pieces
            ?.flatMap(({piece, count}) => new Array(count).fill(piece))
            || [];

        return html`
            <div>
                T:
                ${map(pieces, (piece) => {
                    return html`<root-item .item="${piece.item}"></root-item>`;
                })}
            </div>
        `;
    }

    private _renderVagabondSatchel(i: number) {
        const undamagedPieces = this.piecesByLocation
            .find(({location}) => typeof location !== "string" && location.vagabondSatchel == i)
            ?.pieces
            ?.flatMap(({piece, count}) => new Array(count).fill(piece))
            || [];
        const damagedPieces = this.piecesByLocation
            .find(({location}) => typeof location !== "string" && location.vagabondDamaged == i)
            ?.pieces
            ?.flatMap(({piece, count}) => new Array(count).fill(piece))
            || [];

        return html`
            <div>
                S:
                ${map(undamagedPieces, (piece) => {
                    return html`<root-item .item="${piece.item}"></root-item>`;
                })}
                ${map(damagedPieces, (piece) => {
                    return html`<root-item .item="${piece.item}" damaged></root-item>`;
                })}
            </div>
        `;
    }

    private _renderVagabondCompletedQuests(i: number) {
        const completedQuests = this.piecesByLocation
            .find(({location}) => typeof location !== "string" && location.vagabondCompletedQuests == i)
            ?.pieces
            ?.flatMap(({piece, count}) => {
                if (piece.card) {
                    return new Array(count).fill(piece.card);
                } else {
                    return [];
                }
            })
            ?.sort((c1, c2) => c1?.quest?.[0]?.localeCompare(c2?.quest?.[0]) || 0)
            || [];

        return html`
            <div>
                Q: ${map(completedQuests, (card) => html`<root-card .card="${card}"></root-card>`)}
            </div>
        `;
    }

    private _renderCultAcolyteCount() {
        const acolyteCount = this.piecesByLocation
            .find(({location}) => location == "cultAcolytes")
            ?.pieces[0]
            ?.count
            || 0;

        return html`
            <div>${acolyteCount}×A</div>
        `;
    }

    private _renderCultLostSouls() {
        const lostSouls = this.piecesByLocation
            .find(({location}) => location == "cultLostSouls")
            ?.pieces
            ?.flatMap(({piece, count}) => {
                if (piece.card) {
                    return new Array(count).fill(piece.card);
                } else {
                    return [];
                }
            })
            ?.sort((c1, c2) => {
                if (c1.deckCard && c1.deckCard[0] && c2.deckCard && c2.deckCard[0]) {
                    return c1.deckCard[0].localeCompare(c2.deckCard[0]);
                } else {
                    return 0;
                }
            })
            || [];

        return html`
            <div>
                LS: ${map(lostSouls, (card) => html`<root-card .card="${card}"></root-card>`)}
            </div>
        `;
    }

    private _renderRiverfolkEconomy() {
        const payments = this.piecesByLocation
            .find(({location}) => location == "riverfolkPayments")
            ?.pieces
            ?.filter(({count}) => count != 0)
            || [];
        const funds = this.piecesByLocation
            .find(({location}) => location == "riverfolkFunds")
            ?.pieces
            ?.filter(({count}) => count != 0)
            || [];
        const commitments = this.piecesByLocation
            .filter(({location}) => typeof location !== "string" && location.riverfolkCommitted !== undefined)
            .flatMap(({pieces}) => pieces)
            .filter(({count}) => count != 0);

        return html`
            <div>
                ${map(payments, ({piece, count}) => html`${count}×<root-icon icon="${piece.warrior}" style="color: var(--${piece.warrior}-color)"></root-icon>`)}
                | ${map(funds, ({piece, count}) => html`${count}×<root-icon icon="${piece.warrior}" style="color: var(--${piece.warrior}-color)"></root-icon>`)}
                | ${map(commitments, ({piece, count}) => html`${count}×<root-icon icon="${piece.warrior}" style="color: var(--${piece.warrior}-color)"></root-icon>`)}
            </div>
        `;
    }

    private _renderBurrow() {
        const count = this.piecesByLocation
            .find(({location}) => location == "burrow")
            ?.pieces[0]
            ?.count
            || 0;

        return html`
            <div><root-warriors faction="duchy" count="${count}"></root-warriors></div>
        `;
    }

    private _renderDuchySwayedMinisters() {
        const swayedMinisters = this.piecesByLocation
            .find(({location}) => location == "duchySwayedMinisters")
            ?.pieces
            ?.flatMap(({piece, count}) => {
                if (count != 0 && piece.card) {
                    return [piece.card]
                } else {
                    return []
                }
            })
            || [];

        return html`
            <div>
                ${map(swayedMinisters, (minister) => html`<root-card .card="${minister}"></root-card>`)}
            </div>
        `;
    }

    private _renderHundredsMood() {
        const mood = this.piecesByLocation
            .find(({location}) => location == "hundredsMood")
            ?.pieces[0]?.piece?.card?.hundredsMood;

        return html`<div>${mood}</div>`;
    }

    private _renderHundredsHoard() {
        const hoard = this.piecesByLocation.find(({location}) => location == "hundredsHoard")?.pieces || [];
        const commandItems = hoard.flatMap(({piece, count}) => {
            if (piece.item && ["bag", "boot", "coins"].includes(piece.item[0])) {
                return new Array(count).fill(piece.item);
            } else {
                return [];
            }
        });
        const prowessItems = hoard.flatMap(({piece, count}) => {
            if (piece.item && ["crossbow", "hammer", "sword", "tea"].includes(piece.item[0])) {
                return new Array(count).fill(piece.item);
            } else {
                return [];
            }
        });

        return html`
            <div>
                ${map(commandItems, (item) => html`<root-item .item="${item}"></root-item>`)}
                | ${map(prowessItems, (item) => html`<root-item .item="${item}"></root-item>`)}
            </div>
        `;
    }

    private _renderKeepersRecoveredRelics() {
        const relics = this.piecesByLocation
            .find(({location}) => typeof location !== "string" && location.supply == this.faction)
            ?.pieces
            ?.flatMap(({piece, count}) => {
                if (piece.token && piece.token.type == "relic") {
                    return new Array(count).fill(piece.token);
                } else {
                    return [];
                }
            })
            ?.sort((r1: {type: string, relic: string}, r2: {type: string, relic: string}) => r1.relic.localeCompare(r2.relic))
            || [];
        return html`
        <div>
            ${map(relics, (relic) => html`<root-token .token="${relic}"></root-token>`)}
        </div>
        `;
    }

    private _renderKeepersRetinue() {
        const retinue = ["move", "battleDelve", "moveRecover"]
            .map((column) => this.piecesByLocation.find(({location}) => typeof location !== "string" && location.keepersRetinue == column)?.pieces?.flatMap(({piece, count}) => {
                if (piece.card) {
                    return new Array(count).fill(piece.card);
                } else {
                    return [];
                }
            }) || []);
        return html`
            <div>
                R: ${map(retinue, (column, i) => html`
                    ${when(i > 0, () => html`|`)}
                    ${map(column, (card) => html`<root-card .card="${card}"></root-card>`)}
                `)}
            </div>
        `;
    }
}
