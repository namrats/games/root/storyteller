import {LitElement, html, css} from 'lit';
import {customElement, property} from 'lit/decorators.js';
import {map} from 'lit/directives/map.js';
import {styles} from './styles';
import maps from './root-maps';

@customElement('root-map-select')
export class RootMapSelect extends LitElement {
    @property()
    value: string = "autumn";

    static styles = [
        styles,
        css`:host { display: inline-block; }`,
    ];

    override render() {
        return html`
            <select name="mapName" @change="${this._onChange}">
                ${map(Object.keys(maps), (mapName) => html`
                    <option value="${mapName}" ?selected="${mapName == this.value}">${mapName}</option>
                    `)}
            </select>
        `;
    }

    private _onChange(event: Event) {
        this.value = (event.target as HTMLSelectElement).value;
        this.dispatchEvent(new Event("change"));
    }
}
