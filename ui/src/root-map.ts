import {LitElement, html, css} from 'lit';
import {customElement, property} from 'lit/decorators.js';
import {map} from 'lit/directives/map.js';
import {when} from 'lit/directives/when.js';
import {styles} from './styles';
import {Map, LocatedPieces} from './types';
import './root-clearing';
import './root-forest';

@customElement('root-map')
export class RootMap extends LitElement {
    @property()
    map: Map = {
        clearings: [],
        forests: [],
        paths: [],
        rivers: [],
        lakes: [],
    };

    @property()
    pieces: LocatedPieces[] = [];

    @property()
    clearingSuits?: string[];

    static styles = [
        styles,
        css`
            :host { display: block; }
            .container {
                position: relative; width: 100%; height: 100%;
                padding: 8px;
                background-color: var(--beige);
            }
            .path, .river, .lake {
                position: absolute; top: 0; left: 0; width: 100%; height: 100%;
                z-index: 1;
            }
            .path line { stroke-width: 5px; stroke: var(--dark-brown); }
            .path.closed line { stroke-dasharray: 5,10; }
            .river line { stroke-width: 5px; stroke: var(--blue); }
            .lake polygon { fill: var(--blue); }
            root-clearing { width: 10%; translate: -50% -50%; z-index: 3; }
            root-forest { width: 10%; translate: -50% -50%; z-index: 1; }
        `,
    ];

    override render() {
        const clearings =
            this.map.clearings.map((clearing, i) => {
                const pieces = this.pieces.find(({location}) => typeof location !== "string" && location.clearing !== undefined && location.clearing - 1 == i)?.pieces || [];
                const result = {...clearing, pieces};
                if (this.clearingSuits) {
                    result.suit = this.clearingSuits[i];
                }
                return result;
            });
        const forests = this.map.forests.map((forest) => {
            const pieces = this.pieces.find(({location}) => typeof location !== "string" && location.forest !== undefined && JSON.stringify(location.forest) == JSON.stringify(forest.forest))?.pieces || [];
            return {...forest, pieces};
        });
        const lakes = this.map.lakes.map(lake => lake.map(c => this.map.clearings[c-1]));
        return html`
            <div class="container">
                ${map(clearings, (c, i) => html`
                    <root-clearing number="${i+1}" suit="${c.suit}" building-slot-count="${c.buildingSlotCount}" .pieces="${c.pieces}" style="position: absolute; top: ${c.y}%; left: ${c.x}%;"></root-clearing>
                `)}
                ${map(forests, (f) => html`
                    <root-forest .pieces="${f.pieces}" style="position: absolute; top: ${f.y}%; left: ${f.x}%;"></root-forest>
                `)}
                ${map(this.map.rivers, p => {
                    let from = this.map.clearings[p[0]-1];
                    let to = this.map.clearings[p[1]-1];
                    return html`
                    <svg class="river">
                        <line x1="${from.x}%" y1="${from.y}%" x2="${to.x}%" y2="${to.y}%" />
                    </svg>
                    `
                })}
                ${map(lakes, lake => html`
                    <svg class="lake" viewBox="0 0 100 100" preserveAspectRatio="none">
                        <polygon points="${lake.map(p => `${p.x},${p.y}`).join(" ")}"/>
                    </svg>
                `)}
                ${map(this.map.paths, p => {
                    let from = this.map.clearings[p[0]-1];
                    let to = this.map.clearings[p[1]-1];
                    const closed = this.pieces.find(({location}) => typeof location !== "string" && JSON.stringify(location.path) == JSON.stringify(p))?.pieces?.some(({piece, count}) => piece.marker === "closedPath" && count > 0) || false;
                    return html`
                    <svg class="path ${when(closed, () => "closed")}">
                        <line x1="${from.x}%" y1="${from.y}%" x2="${to.x}%" y2="${to.y}%" />
                    </svg>
                    `
                })}
            <div>
            `;
    }
}
