import {LitElement, html, css} from 'lit';
import {customElement, property} from 'lit/decorators.js';
import {map} from 'lit/directives/map.js';
import {when} from 'lit/directives/when.js';
import {produce} from 'immer';
import {styles} from './styles';
import {GameRecord, TurnText, State} from './types';
import './root-icon';
import {RootActionEditor} from './root-action-editor';
import './root-action-editor';
import './root-action-viewer';
import {RootPlayerEditor} from './root-player-editor';
import './root-player-editor';
import './root-player-board';
import {RootMapSelect} from './root-map-select';
import './root-map-select';
import {RootDeckSelect} from './root-deck-select';
import './root-deck-select';
import {RootLandmarkSelect} from './root-landmark-select';
import './root-landmark-select';
import './root-supply';
import './root-map';
import maps from './root-maps';

@customElement('root-storyteller')
export class RootStoryteller extends LitElement {
    @property()
    gameRecord: GameRecord = {
        mapName: "autumn",
        deckName: "enp",
        landmarks: [],
        turns: [],
    };

    @property()
    turns: TurnText[] = [];

    @property()
    state: State = {piecesByLocation: [], scores: {}};

    @property({type: Boolean})
    edit: boolean = false;

    @property({type: Number})
    selectedTurn?: number;

    @property({type: Number})
    selectedAction?: number;

    static styles = [
        styles,
        css`
            :host { display: block; }
            .screen {
                display: grid; width: 100%; height: 100%;
                grid: "menu menu menu" 70px "map status action" auto / auto 310px 250px;
            }

            root-map {
                grid-area: map;
                width: 100%; height: 100%;
            }

            .menu {
                grid-area: menu;
                height: 70px;
                padding: 0 16px 0 0;
                background: linear-gradient(90deg, rgba(0, 0, 0, 0.5) 11.31%, rgba(0, 0, 0, 0) 11.66%), linear-gradient(180deg, rgba(0, 0, 0, 0) 92.02%, rgba(150, 114, 114, 0.5) 100%), linear-gradient(270deg, rgba(0, 0, 0, 0.1) 0%, rgba(0, 0, 0, 0) 12.73%), linear-gradient(0deg, rgba(0, 0, 0, 0.1) 0.78%, rgba(0, 0, 0, 0) 50.39%), radial-gradient(302.71% 302.71% at 50% 50%, #F8E8C1 0%, #4B3D2E 100%);
            }
            .menu > div {
                display: flex; flex-direction: row; gap: 48px;
                align-items: baseline;
            }
            .menu h1 {
                width: 230px;
                text-align: center;
                color: var(--beige);
            }
            root-landmark-select { vertical-align: bottom; }

            .status { grid-area: status; }
            .status > div {
                height: 100%;
                display: flex; flex-direction: column; gap: 8px; padding: 8px;
                justify-content: flex-start; align-content: flex-start;
                background-color: var(--brown);
            }
            .player { display: flex; flex-direction: column; gap: 0; }
            .action { grid-area: action; }
            root-supply, root-player-board { width: 100%; }

            root-action-editor, root-action-viewer {
                width: 100%; height: 100%;
                max-height: calc(100vh - 70px);
                overflow-y: scroll;
                overflow-x: clip;
            }
        `,
    ];

    override render() {
        return html`
            <div class="screen">
                <div class="menu">
                    <div>
                        <h1>Storyteller</h1>
                        <p>Map <root-map-select value="${this.gameRecord.mapName}" @change="${this._onMapChange}"></root-map-select></p>
                        <p>Deck <root-deck-select value="${this.gameRecord.deckName}" @change="${this._onDeckChange}"></root-deck-select></p>
                        <p>Landmarks <root-landmark-select .value="${this.gameRecord.landmarks}" @change="${this._onLandmarksChange}"></root-landmark-select></p>
                        <slot name="tools"></slot>
                    </div>
                </div>
                <root-map .map="${maps[this.gameRecord.mapName]}" .clearingSuits="${this.gameRecord.clearingSuits}" .pieces="${this.state.piecesByLocation}"></root-map>
                <div class="status">
                    <div>
                        <root-supply .piecesByLocation="${this.state.piecesByLocation}"></root-supply>
                        ${map(this.gameRecord.players, (player, i) => html`
                            <div class="player">
                                <root-player-editor ?edit="${this.edit}" faction="${player.faction}" name="${player.name}" .score="${this.playerScore(player.faction)}" @change="${(e: Event) => this._onPlayerChange(e, i)}">
                                    ${when(this.edit, () => html`<button @click="${() => this.removePlayer(i)}">x</button>`)}
                                </root-player-editor>
                                <root-player-board faction="${player.faction}" .piecesByLocation="${this.state.piecesByLocation}"></root-player-board>
                            </div>
                            `)}
                        ${when(this.edit, () => html`<button @click="${this.addPlayer}">+player</button>`)}
                    </div>
                </div>
                ${when(this.edit,
                    () => html`<root-action-editor class="action" .turnOrder="${this.gameRecord.players?.map(player => player.faction)}" .turns="${this.turns}" @change="${this._onActionChange}"></root-action-editor>`,
                    () => html`<root-action-viewer class="action" .playerCount="${this.gameRecord.players?.length}" .turns="${this.gameRecord.turns}" selectedTurn="${this.selectedTurn}" selectedAction="${this.selectedAction}"></root-action-viewer>`
                )}
            </div>
        `;
    }

    playerScore(faction?: string): number|undefined {
        if (faction !== undefined) {
            return this.state.scores[faction];
        } else {
            return undefined;
        }
    }

    private addPlayer() {
        this.gameRecord = produce(this.gameRecord, (draft) => {
            if (!draft.players) {
                draft.players = [];
            }
            draft.players.push({});
        });
        this.dispatchEvent(new Event("change"));
    }

    private removePlayer(i: number) {
        this.gameRecord = produce(this.gameRecord, (draft) => {
            draft.players?.splice(i, 1);
        });
        this.dispatchEvent(new Event("change"));
    }

    private _onPlayerChange(event: Event, i: number) {
        const target = event.target as RootPlayerEditor;
        this.gameRecord = produce(this.gameRecord, (draft) => {
            if (draft.players) {
                draft.players[i] = {
                    name: target.name,
                    faction: target.faction,
                };
            }
        });
        this.dispatchEvent(new Event("change"));
    }

    private _onMapChange(event: Event) {
        this.gameRecord = produce(this.gameRecord, (draft) => {
            draft.mapName = (event.target as RootMapSelect).value;
        });
        this.dispatchEvent(new Event("change"));
    }

    private _onDeckChange(event: Event) {
        this.gameRecord = produce(this.gameRecord, (draft) => {
            draft.deckName = (event.target as RootDeckSelect).value;
        });
        this.dispatchEvent(new Event("change"));
    }

    private _onLandmarksChange(event: Event) {
        this.gameRecord = produce(this.gameRecord, (draft) => {
            draft.landmarks = (event.target as RootLandmarkSelect).value;
        });
        this.dispatchEvent(new Event("change"));
    }

    private _onActionChange(event: Event) {
        this.turns = (event.target as RootActionEditor).turns;
        this.dispatchEvent(new Event("change"));
    }
}
