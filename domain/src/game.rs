use crate::action::Action;
use crate::map::Map;
use crate::piece::{Faction, Landmark, Suit, VagabondCharacter};
use crate::state::{State, Turn};
use serde::{Deserialize, Serialize};

#[derive(PartialEq, Serialize, Deserialize, Debug)]
pub struct TurnRecord {
    pub faction: Faction,
    pub actions: Vec<Action>,
}

#[derive(PartialEq, Serialize, Deserialize, Debug)]
pub struct PlayerRecord {
    #[serde(default)]
    pub name: Option<String>,
    #[serde(default)]
    pub faction: Option<Faction>,
}

#[derive(PartialEq, Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub enum DraftPoolEntry {
    Faction(Faction),
    VagabondCharacter(VagabondCharacter),
}

#[derive(PartialEq, Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct GameRecord {
    pub map_name: String,
    pub deck_name: String,
    #[serde(default)]
    pub clearing_suits: Option<Vec<Suit>>,
    #[serde(default)]
    pub landmarks: Vec<Landmark>,
    #[serde(default)]
    pub draft_pool: Option<Vec<DraftPoolEntry>>,
    #[serde(default)]
    pub players: Vec<PlayerRecord>,
    #[serde(default)]
    pub turns: Vec<TurnRecord>,
    #[serde(default)]
    pub winner: Option<Faction>,
}

impl GameRecord {
    pub fn states(&self) -> Vec<Turn> {
        let mut sb = State::builder().deck().default_items();

        match self.map_name.as_str() {
            "autumn" => sb = sb.map(Map::autumn()),
            "winter" => sb = sb.map(Map::winter()),
            "lake" => sb = sb.map(Map::lake()),
            "mountain" => sb = sb.map(Map::mountain()),
            _ => (),
        }

        for f in self.players.iter().filter_map(|player| player.faction) {
            sb = sb.faction(f);
        }

        for landmark in &self.landmarks {
            sb = sb.landmark(*landmark);
        }

        self.turns
            .iter()
            .scan(sb.build(), |state, turn| {
                Some(Turn {
                    faction: turn.faction,
                    states: turn
                        .actions
                        .iter()
                        .scan(state, |state, action| {
                            **state = state.act(action);
                            Some(state.clone())
                        })
                        .collect(),
                })
            })
            .collect()
    }
}
