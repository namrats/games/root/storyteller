use crate::action::{Action, Pieces, Quantity};
use crate::map::Map;
use crate::piece::{
    Card, CardSuit, ClearingNumber, EyrieDecreeColumn, EyrieLeader, Faction, HundredsMood, Item,
    ItemState, Landmark, Location, Marker, Piece, Relationship, Suit, Token, VagabondCharacter,
};
use counter::Counter;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(PartialEq, Serialize, Deserialize, Clone, Copy, Debug)]
#[serde(rename_all = "camelCase")]
pub enum PlayerScore {
    Points(usize),
    Dominance(CardSuit),
    Coalition(Faction),
}

#[derive(PartialEq, Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Outcast {
    suit: Suit,
    hated: bool,
}

#[derive(PartialEq, Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct ServicePrices {
    hand_card: usize,
    riverboats: usize,
    mercenaries: usize,
}

#[derive(PartialEq, Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct State {
    #[serde(with = "serde_pieces_by_location")]
    pub pieces_by_location: HashMap<Location, Counter<Piece>>,
    pub scores: HashMap<Faction, PlayerScore>,
    pub relationship: HashMap<usize, HashMap<Faction, Relationship>>,
    pub outcast: Option<Outcast>,
    pub service_prices: Option<ServicePrices>,
}

mod serde_pieces_by_location {
    use super::*;
    use serde::de::Deserializer;
    use serde::ser::Serializer;

    #[derive(Serialize)]
    struct Locations<'a> {
        location: &'a Location,
        pieces: Vec<Pieces<'a>>,
    }

    #[derive(Serialize)]
    struct Pieces<'a> {
        piece: &'a Piece,
        count: usize,
    }

    pub fn serialize<S>(
        pieces_by_location: &HashMap<Location, Counter<Piece>>,
        serializer: S,
    ) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let locations: Vec<Locations> = pieces_by_location
            .iter()
            .map(|(l, ps)| Locations {
                location: l,
                pieces: ps
                    .iter()
                    .map(|(p, n)| Pieces {
                        piece: p,
                        count: *n,
                    })
                    .collect(),
            })
            .collect();

        locations.serialize(serializer)
    }

    pub fn deserialize<'de, D>(_: D) -> Result<HashMap<Location, Counter<Piece>>, D::Error>
    where
        D: Deserializer<'de>,
    {
        todo!()
    }
}

#[derive(Debug)]
pub struct StateBuilder {
    pieces_by_location: HashMap<Location, Counter<Piece>>,
    scores: HashMap<Faction, PlayerScore>,
    relationship: HashMap<usize, HashMap<Faction, Relationship>>,
    outcast: Option<Outcast>,
    service_prices: Option<ServicePrices>,
}

#[derive(PartialEq, Serialize, Deserialize, Debug)]
pub struct Turn {
    pub faction: Faction,
    pub states: Vec<State>,
}

impl Default for StateBuilder {
    fn default() -> Self {
        Self::new()
    }
}

impl StateBuilder {
    fn new() -> Self {
        Self {
            pieces_by_location: vec![
                Location::DrawPile,
                Location::Discard,
                Location::AvailableDominanceCards,
                Location::AvailableQuests,
                Location::Supply(None),
            ]
            .into_iter()
            .map(|location| (location, Default::default()))
            .collect(),
            scores: Default::default(),
            relationship: Default::default(),
            outcast: None,
            service_prices: None,
        }
    }

    pub fn map(mut self, map: Map) -> Self {
        for i in 1..=12 {
            self.pieces_by_location
                .insert(Location::Clearing(i), Default::default());
        }
        for c in map.ruins {
            self.pieces_by_location
                .entry(Location::Clearing(c))
                .or_default()
                .insert(Piece::Marker(Marker::Ruins), 1);
        }
        for path in map.paths {
            let pieces = if path.is_closed() {
                Counter::init(vec![Piece::Marker(Marker::ClosedPath)])
            } else {
                Default::default()
            };
            self.pieces_by_location
                .insert(Location::Path(path.from, path.to), pieces);
        }
        self
    }

    pub fn deck(mut self) -> Self {
        self.pieces_by_location
            .entry(Location::DrawPile)
            .or_default()
            .insert(Piece::Card(Card::DeckCard(None, None)), 54);
        self
    }

    pub fn default_items(mut self) -> Self {
        let supply = self
            .pieces_by_location
            .entry(Location::Supply(None))
            .or_default();
        let items = vec![
            (Item::Bag, 2),
            (Item::Boot, 2),
            (Item::Coins, 2),
            (Item::Crossbow, 1),
            (Item::Hammer, 1),
            (Item::Sword, 2),
            (Item::Tea, 2),
        ]
        .into_iter()
        .map(|(item, n)| (Piece::Item(item, Some(ItemState::Refreshed)), n))
        .collect::<Counter<Piece>>();

        *supply += items;

        self
    }

    pub fn landmark(mut self, landmark: Landmark) -> Self {
        self.pieces_by_location
            .entry(Location::Supply(None))
            .or_default()
            .insert(Piece::Landmark(landmark), 1);

        self
    }

    pub fn item(mut self, item: Item, n: usize) -> Self {
        self.pieces_by_location
            .entry(Location::Supply(None))
            .or_default()
            .insert(Piece::Item(item, Some(ItemState::Refreshed)), n);

        self
    }

    pub fn draw_pile(mut self, n: usize) -> Self {
        self.pieces_by_location
            .entry(Location::DrawPile)
            .or_default()[&Piece::Card(Card::DeckCard(None, None))] = n;
        self
    }

    pub fn discard(mut self, cards: Vec<Card>) -> Self {
        self.pieces_by_location.insert(
            Location::Discard,
            cards.into_iter().map(Piece::Card).collect(),
        );
        self
    }

    pub fn faction(mut self, faction: Faction) -> Self {
        self.scores.insert(faction, PlayerScore::Points(0));
        self.pieces_by_location
            .insert(Location::Supply(Some(faction)), faction.starting_pieces());

        self.pieces_by_location.extend(
            faction
                .faction_locations()
                .into_iter()
                .map(|location| (location, Default::default())),
        );

        self
    }

    pub fn points(mut self, faction: Faction, points: usize) -> Self {
        self.scores.insert(faction, PlayerScore::Points(points));
        self
    }

    pub fn dominance(mut self, faction: Faction, suit: CardSuit) -> Self {
        self.scores.insert(faction, PlayerScore::Dominance(suit));
        self
    }

    pub fn coalition(mut self, faction: Faction, with: Faction) -> Self {
        self.scores.insert(faction, PlayerScore::Coalition(with));
        self
    }

    pub fn hand(mut self, faction: Faction, n: usize) -> Self {
        let hand = if n == 0 {
            Default::default()
        } else {
            vec![(Piece::Card(Card::DeckCard(None, None)), n)]
                .into_iter()
                .collect()
        };
        self.pieces_by_location
            .insert(Location::Hand(faction), hand);
        self
    }

    pub fn hand_cards(mut self, faction: Faction, cards: Vec<Card>) -> Self {
        self.pieces_by_location.insert(
            Location::Hand(faction),
            cards.into_iter().map(Piece::Card).collect(),
        );
        self
    }

    pub fn crafted(mut self, faction: Faction, pieces: Vec<Quantity>) -> Self {
        *self
            .pieces_by_location
            .entry(Location::Crafted(faction))
            .or_default() += pieces.into_iter().collect::<Counter<Piece>>();
        self
    }

    pub fn supply(mut self, faction: Faction, piece: Piece, n: usize) -> Self {
        if n == 0 {
            self.pieces_by_location
                .entry(Location::Supply(Some(faction)))
                .or_default()
                .remove(&piece);
        } else {
            self.pieces_by_location
                .entry(Location::Supply(Some(faction)))
                .or_default()[&piece] = n;
        }
        self
    }

    pub fn common_supply(mut self, piece: Piece, n: usize) -> Self {
        self.pieces_by_location
            .entry(Location::Supply(None))
            .or_default()[&piece] = n;
        self
    }

    pub fn eyrie_leader(mut self, leader: EyrieLeader) -> Self {
        self.pieces_by_location.insert(
            Location::EyrieLeader,
            vec![Piece::Card(Card::EyrieLeader(leader))]
                .into_iter()
                .collect(),
        );
        self
    }

    pub fn eyrie_decree(mut self, column: EyrieDecreeColumn, cards: Vec<Card>) -> Self {
        self.pieces_by_location.insert(
            Location::EyrieDecree(column),
            cards.into_iter().map(Piece::Card).collect(),
        );
        self
    }

    pub fn vagabond_character(mut self, i: usize, character: VagabondCharacter) -> Self {
        self.pieces_by_location.insert(
            Location::VagabondCharacter(i),
            vec![Piece::Card(Card::VagabondCharacter(character))]
                .into_iter()
                .collect(),
        );
        self
    }

    pub fn satchel_item(
        mut self,
        _faction: Faction,
        n: usize,
        item: Item,
        item_state: ItemState,
    ) -> Self {
        self.pieces_by_location
            .entry(Location::VagabondSatchel(1))
            .or_default()[&Piece::Item(item, Some(item_state))] = n;
        self
    }

    pub fn track_item(
        mut self,
        _faction: Faction,
        n: usize,
        item: Item,
        item_state: ItemState,
    ) -> Self {
        self.pieces_by_location
            .entry(Location::VagabondTrack(1))
            .or_default()[&Piece::Item(item, Some(item_state))] = n;
        self
    }

    pub fn lost_souls(mut self, cards: Vec<Card>) -> Self {
        self.pieces_by_location.insert(
            Location::CultLostSouls,
            cards.into_iter().map(Piece::Card).collect(),
        );
        self
    }

    pub fn hundreds_mood(mut self, mood: HundredsMood) -> Self {
        self.pieces_by_location.insert(
            Location::HundredsMood,
            vec![Piece::Card(Card::HundredsMood(mood))]
                .into_iter()
                .collect(),
        );
        self
    }

    pub fn hundreds_hoard(mut self, items: Vec<(Piece, usize)>) -> Self {
        self.pieces_by_location
            .insert(Location::HundredsHoard, items.into_iter().collect());
        self
    }

    pub fn clearing(mut self, clearing: ClearingNumber, pieces: Vec<Quantity>) -> Self {
        self.pieces_by_location
            .insert(Location::Clearing(clearing), pieces.into_iter().collect());
        self
    }

    pub fn path(mut self, from: ClearingNumber, to: ClearingNumber, pieces: Vec<Quantity>) -> Self {
        self.pieces_by_location
            .insert(Location::Path(from, to), pieces.into_iter().collect());
        self
    }

    pub fn build(self) -> State {
        State {
            pieces_by_location: self.pieces_by_location,
            scores: self.scores,
            relationship: self.relationship,
            outcast: self.outcast,
            service_prices: self.service_prices,
        }
    }
}

fn normalize(location: &Location, piece: Piece) -> Piece {
    match (piece, location) {
        (Piece::Card(Card::DeckCard(_, _)), Location::Hand(Faction::Riverfolk)) => piece,
        (
            Piece::Card(Card::DeckCard(_, _)),
            Location::Hand(_) | Location::AllianceSupporters | Location::DrawPile,
        ) => Piece::Card(Card::DeckCard(None, None)),
        (Piece::Token(Token::Plot { .. }), Location::Supply(Some(Faction::Corvid))) => {
            Piece::Token(Token::Plot { plot: None })
        }
        (Piece::Item(_, None), _) => piece.refreshed(),
        _ => piece,
    }
}

impl State {
    pub fn builder() -> StateBuilder {
        StateBuilder::default()
    }

    fn move_(&mut self, from: &Location, to: &Location, n: usize, piece: &Piece) {
        self.take(from, n, piece);
        self.put(to, n, piece);
    }

    fn move_all(&mut self, from: &Location, to: &Location) {
        let pieces = self.take_all(from);
        self.put_many(to, pieces);
    }

    fn move_many<I>(&mut self, from: &Location, to: &Location, pieces: I)
    where
        I: IntoIterator<Item = Quantity> + Clone,
    {
        self.take_many(from, pieces.clone());
        self.put_many(to, pieces);
    }

    fn take(&mut self, from: &Location, n: usize, piece: &Piece) {
        let piece = normalize(from, *piece);
        *self.pieces_by_location.entry(from.clone()).or_default() -=
            vec![(piece, n)].into_iter().collect::<Counter<Piece>>();
    }

    fn take_many<I>(&mut self, from: &Location, pieces: I)
    where
        I: IntoIterator<Item = Quantity>,
    {
        let normalized_pieces = pieces
            .into_iter()
            .map(|(piece, n)| (normalize(from, piece), n))
            .collect::<Counter<Piece>>();
        *self.pieces_by_location.entry(from.clone()).or_default() -= normalized_pieces;
    }

    fn take_all(&mut self, from: &Location) -> Counter<Piece> {
        self.pieces_by_location
            .insert(from.clone(), Default::default())
            .unwrap_or_default()
    }

    fn put(&mut self, to: &Location, n: usize, piece: &Piece) {
        let piece = normalize(to, *piece);
        self.pieces_by_location.entry(to.clone()).or_default()[&piece] += n;
    }

    fn put_many<'a, I>(&'_ mut self, to: &'_ Location, pieces: I)
    where
        I: IntoIterator<Item = Quantity>,
    {
        let normalized_pieces = pieces
            .into_iter()
            .map(|(piece, n)| (normalize(to, piece), n))
            .collect::<Counter<Piece>>();
        *self.pieces_by_location.entry(to.clone()).or_default() += normalized_pieces;
    }

    pub fn piece_count(&self, at: &Location, piece: &Piece) -> usize {
        self.pieces_by_location
            .get(at)
            .map(|l| l[piece])
            .unwrap_or(0)
    }

    pub fn score(&self, faction: &Faction) -> PlayerScore {
        self.scores[faction]
    }

    pub fn act(&self, action: &Action) -> Self {
        let mut result = self.clone();
        match action {
            Action::Place { pieces, at, .. } => {
                for (piece, n) in pieces {
                    for at in at {
                        match (at, piece) {
                            (
                                Location::VagabondCharacter(i),
                                Piece::Card(Card::VagabondCharacter(character)),
                            ) => {
                                result.put_many(
                                    &Location::VagabondTrack(*i),
                                    character
                                        .starting_track_items()
                                        .into_iter()
                                        .map(|item| Piece::Item(item, Some(ItemState::Refreshed)))
                                        .collect::<Counter<Piece>>(),
                                );
                                result.put_many(
                                    &Location::VagabondSatchel(*i),
                                    character
                                        .starting_satchel_items()
                                        .into_iter()
                                        .map(|item| Piece::Item(item, Some(ItemState::Refreshed)))
                                        .collect::<Counter<Piece>>(),
                                );
                            }
                            (Location::EyrieLeader, _) => {
                                result.move_all(
                                    &Location::EyrieLeader,
                                    &Location::Supply(Some(Faction::Eyrie)),
                                );
                            }
                            (Location::HundredsMood, _) => {
                                result.move_all(
                                    &Location::HundredsMood,
                                    &Location::Supply(Some(Faction::Hundreds)),
                                );
                            }
                            _ => (),
                        }
                        match piece {
                            Piece::Card(Card::DeckCard(_, _)) => {
                                result.move_(&Location::DrawPile, at, *n, piece)
                            }
                            Piece::Item(i, _) => result.move_(
                                &Location::Supply(None),
                                at,
                                *n,
                                &Piece::Item(*i, Some(ItemState::Refreshed)),
                            ),
                            _ => result.move_(&Location::Supply(piece.owner()), at, *n, piece),
                        }
                    }
                }
            }
            Action::Move {
                pieces, from, to, ..
            } => {
                match pieces {
                    Pieces::All => result.move_all(from, to),
                    Pieces::Specific(pieces) => result.move_many(from, to, pieces.iter().cloned()),
                };
            }
            Action::Remove { pieces, from, .. } => {
                for from in from {
                    let pieces = match pieces {
                        Pieces::All => result.take_all(from),
                        Pieces::Specific(pieces) => pieces.iter().cloned().collect(),
                    };
                    for (piece, n) in pieces {
                        match piece {
                            Piece::Card(Card::DeckCard(_, _)) => {
                                result.move_(from, &Location::Discard, n, &piece)
                            }
                            _ => result.move_(from, &Location::Supply(piece.owner()), n, &piece),
                        }
                    }
                }
            }
            Action::RemoveFromGame { pieces, from, .. } => {
                for (piece, n) in pieces {
                    match piece {
                        Piece::Card(Card::DeckCard(_, _)) => {
                            result.take(from.as_ref().unwrap_or(&Location::Discard), *n, piece)
                        }
                        _ => result.take(
                            from.as_ref().unwrap_or(&Location::Supply(piece.owner())),
                            *n,
                            piece,
                        ),
                    }
                }
            }
            Action::ScorePoints {
                faction, amount, ..
            } => match result.scores.get_mut(faction).unwrap() {
                PlayerScore::Points(ref mut n) => *n += amount,
                _ => (),
            },
            Action::LosePoints {
                faction, amount, ..
            } => match result.scores.get_mut(faction).unwrap() {
                PlayerScore::Points(ref mut n) => {
                    if *n > *amount {
                        *n -= amount
                    } else {
                        *n = 0
                    }
                }
                _ => (),
            },
            Action::ActivateDominance {
                faction,
                suit,
                coalition: None,
                ..
            } => {
                result
                    .scores
                    .insert(*faction, PlayerScore::Dominance(*suit));
                result.take(
                    &Location::Hand(*faction),
                    1,
                    &Piece::Card(Card::DeckCard(None, None)),
                )
            }
            Action::ActivateDominance {
                faction,
                suit: _,
                coalition: Some(coalition),
                ..
            } => {
                result
                    .scores
                    .insert(*faction, PlayerScore::Coalition(*coalition));
                result.take(
                    &Location::Hand(*faction),
                    1,
                    &Piece::Card(Card::DeckCard(None, None)),
                )
            }
            Action::Refresh {
                faction,
                pieces,
                in_,
                ..
            } => {
                let in_ = in_.clone().unwrap_or_else(|| match *faction {
                    Faction::Vagabond(i) => Location::VagabondSatchel(i),
                    _ => Location::Crafted(*faction),
                });
                match pieces {
                    Pieces::All => {
                        let pieces = result.take_all(&in_);
                        result.put_many(
                            &in_,
                            pieces.into_iter().map(|(piece, n)| (piece.refreshed(), n)),
                        );
                    }
                    Pieces::Specific(pieces) => {
                        result.take_many(
                            &in_,
                            pieces.into_iter().map(|(piece, n)| (piece.exhausted(), *n)),
                        );
                        result.put_many(
                            &in_,
                            pieces.into_iter().map(|(piece, n)| (piece.refreshed(), *n)),
                        )
                    }
                }
            }
            Action::MoveRefresh {
                pieces, from, to, ..
            } => match pieces {
                Pieces::All => {
                    let pieces = result.take_all(from);
                    result.put_many(
                        to,
                        pieces.into_iter().map(|(piece, n)| (piece.refreshed(), n)),
                    );
                }
                Pieces::Specific(pieces) => {
                    result.take_many(
                        from,
                        pieces.iter().map(|(piece, n)| (piece.exhausted(), *n)),
                    );
                    result.put_many(to, pieces.iter().map(|(piece, n)| (piece.refreshed(), *n)))
                }
            },
            Action::Exhaust {
                faction,
                pieces,
                in_,
                ..
            } => {
                let in_ = in_.clone().unwrap_or_else(|| match *faction {
                    Faction::Vagabond(i) => Location::VagabondSatchel(i),
                    _ => Location::Crafted(*faction),
                });
                match pieces {
                    Pieces::All => {
                        let pieces = result.take_all(&in_);
                        result.put_many(&in_, pieces);
                    }
                    Pieces::Specific(pieces) => {
                        result.take_many(
                            &in_,
                            pieces.into_iter().map(|(piece, n)| (piece.refreshed(), *n)),
                        );
                        result.put_many(
                            &in_,
                            pieces.into_iter().map(|(piece, n)| (piece.exhausted(), *n)),
                        )
                    }
                }
            }
            Action::MoveExhaust {
                pieces, from, to, ..
            } => match pieces {
                Pieces::All => {
                    let pieces = result.take_all(from);
                    result.put_many(
                        to,
                        pieces.into_iter().map(|(piece, n)| (piece.exhausted(), n)),
                    );
                }
                Pieces::Specific(pieces) => {
                    result.take_many(
                        from,
                        pieces.into_iter().map(|(piece, n)| (piece.refreshed(), *n)),
                    );
                    result.put_many(
                        to,
                        pieces.into_iter().map(|(piece, n)| (piece.exhausted(), *n)),
                    )
                }
            },
            Action::RefillDrawPile { .. } => {
                let n = result
                    .pieces_by_location
                    .entry(Location::Discard)
                    .or_default()
                    .drain()
                    .map(|(_, n)| n)
                    .sum();
                result.put(
                    &Location::DrawPile,
                    n,
                    &Piece::Card(Card::DeckCard(None, None)),
                );
            }
            Action::Flip {
                at,
                obverse,
                reverse,
                ..
            } => {
                result.take(at, 1, obverse);
                result.put(at, 1, reverse);
            }
            Action::Swap {
                pieces, locations, ..
            } => {
                result.move_(&locations[0], &locations[1], 1, &pieces[0]);
                result.move_(&locations[1], &locations[0], 1, &pieces[1]);
            }
            Action::SetRelationship {
                faction, with, to, ..
            } => match faction {
                Faction::Vagabond(i) => {
                    result
                        .relationship
                        .entry(*i)
                        .or_default()
                        .insert(*with, *to);
                }
                _ => (),
            },
            Action::SetOutcast { suit, hated, .. } => {
                result.outcast = Some(Outcast {
                    suit: *suit,
                    hated: *hated,
                });
            }
            Action::SetServicePrices {
                hand_card,
                riverboats,
                mercenaries,
                ..
            } => {
                result.service_prices = Some(ServicePrices {
                    hand_card: *hand_card,
                    riverboats: *riverboats,
                    mercenaries: *mercenaries,
                });
            }
            Action::Battle { .. }
            | Action::Reveal { .. }
            | Action::Expose { .. }
            | Action::RollBattleDice { .. }
            | Action::RollMobDie { .. } => (),
        }
        result
    }
}
