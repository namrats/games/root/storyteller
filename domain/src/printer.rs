use crate::action::{Action, Pieces, Quantity};
use crate::game::{DraftPoolEntry, GameRecord, PlayerRecord};
use crate::piece::{
    Building, Card, CardName, CardSuit, DuchyMinister, EyrieCard, EyrieDecreeColumn, EyrieLeader,
    Faction, HundredsMood, HundredsPiece, Item, ItemState, KeepersCard, KeepersRetinueColumn,
    Landmark, Location, Marker, Piece, Plot, Quest, Relationship, Relic, Suit, Token,
    VagabondCharacter,
};
use itertools::{Itertools, Position};
use std::fmt::{Display, Formatter, Result};

pub struct FactionScoped<T> {
    faction: Faction,
    value: T,
}

impl<T> FactionScoped<T> {
    pub fn with(faction: Faction, value: T) -> Self {
        FactionScoped { faction, value }
    }
}

impl Display for GameRecord {
    fn fmt(&self, f: &mut Formatter) -> Result {
        writeln!(f, "Map: {}", self.map_name)?;
        writeln!(f, "Deck: {}", self.deck_name)?;
        if let Some(clearing_suits) = &self.clearing_suits {
            write!(f, "Clearings: ")?;
            for clearing_suit in clearing_suits {
                write!(f, "{}", clearing_suit)?;
            }
            writeln!(f)?;
        }
        if let Some(draft_pool) = &self.draft_pool {
            write!(f, "DraftPool: ")?;
            for entry in draft_pool {
                write!(f, "{}", entry)?;
            }
            writeln!(f)?;
        }
        if !self.players.is_empty() {
            writeln!(f, "Players:")?;
            for player in &self.players {
                writeln!(f, "  {}", player)?;
            }
        }
        if let Some(winner) = &self.winner {
            write!(f, "Winner: {}", winner)?;
        }
        writeln!(f)?;
        for turn in &self.turns {
            write!(f, "{}: ", turn.faction)?;
            for action in turn.actions.iter().with_position() {
                match action {
                    Position::First(action) | Position::Middle(action) => {
                        write!(f, "{}/", FactionScoped::with(turn.faction, action))?;
                    }
                    Position::Last(action) | Position::Only(action) => {
                        write!(f, "{}", FactionScoped::with(turn.faction, action))?;
                    }
                }
            }
            writeln!(f)?;
        }
        Ok(())
    }
}

impl Display for PlayerRecord {
    fn fmt(&self, f: &mut Formatter) -> Result {
        if let Some(faction) = &self.faction {
            write!(f, "{}: ", faction)?;
            if let Some(name) = &self.name {
                write!(f, "{}", name)?;
            }
        }
        Ok(())
    }
}

impl Display for Suit {
    fn fmt(&self, f: &mut Formatter) -> Result {
        match self {
            Suit::Fox => write!(f, "f"),
            Suit::Mouse => write!(f, "m"),
            Suit::Rabbit => write!(f, "r"),
        }
    }
}

impl Display for CardSuit {
    fn fmt(&self, f: &mut Formatter) -> Result {
        match self {
            Self::Bird => write!(f, "b"),
            Self::Fox => write!(f, "f"),
            Self::Mouse => write!(f, "m"),
            Self::Rabbit => write!(f, "r"),
        }
    }
}

impl Display for CardName {
    fn fmt(&self, f: &mut Formatter) -> Result {
        match self {
            Self::Ambush => write!(f, "a")?,
            Self::Bag => write!(f, "b")?,
            Self::Boot => write!(f, "m")?,
            Self::Coins => write!(f, "c")?,
            Self::Crossbow => write!(f, "x")?,
            Self::Hammer => write!(f, "h")?,
            Self::Sword => write!(f, "s")?,
            Self::Tea => write!(f, "t")?,
            Self::Dominance => write!(f, "dom")?,
            Self::Armorers => write!(f, "armor")?,
            Self::BetterBurrowBank => write!(f, "bank")?,
            Self::BrutalTactics => write!(f, "brutal")?,
            Self::CommandWarren => write!(f, "command")?,
            Self::Cobbler => write!(f, "cob")?,
            Self::Codebreakers => write!(f, "codeb")?,
            Self::Favor => write!(f, "favor")?,
            Self::RoyalClaim => write!(f, "royal")?,
            Self::Sappers => write!(f, "sap")?,
            Self::ScoutingParty => write!(f, "scout")?,
            Self::StandAndDeliver => write!(f, "stand")?,
            Self::TaxCollector => write!(f, "tax")?,
            Self::BoatBuilders => write!(f, "boat")?,
            Self::CharmOffensive => write!(f, "charm")?,
            Self::CoffinMakers => write!(f, "coffin")?,
            Self::CorvidPlanners => write!(f, "cplans")?,
            Self::EyrieEmigree => write!(f, "emi")?,
            Self::FalseOrders => write!(f, "false")?,
            Self::Partisans => write!(f, "part")?,
            Self::Informants => write!(f, "inform")?,
            Self::LeagueOfAdventurousMice => write!(f, "league")?,
            Self::MasterEngravers => write!(f, "engrave")?,
            Self::MurineBroker => write!(f, "murine")?,
            Self::PropagandaBureau => write!(f, "prop")?,
            Self::Saboteurs => write!(f, "sabo")?,
            Self::SoupKitchens => write!(f, "soup")?,
            Self::SwapMeet => write!(f, "swap")?,
            Self::Tunnels => write!(f, "tun")?,
        }
        Ok(())
    }
}

impl Display for Faction {
    fn fmt(&self, f: &mut Formatter) -> Result {
        use Faction::*;
        match self {
            Marquise => write!(f, "M"),
            Eyrie => write!(f, "E"),
            Alliance => write!(f, "A"),
            Vagabond(i) => write!(f, "V{i}"),
            Cult => write!(f, "L"),
            Riverfolk => write!(f, "O"),
            Duchy => write!(f, "D"),
            Corvid => write!(f, "C"),
            Hundreds => write!(f, "H"),
            Keepers => write!(f, "K"),
        }
    }
}

impl Display for DraftPoolEntry {
    fn fmt(&self, f: &mut Formatter) -> Result {
        match self {
            Self::Faction(faction) => write!(f, "{}", faction),
            Self::VagabondCharacter(character) => write!(f, "#{}", character),
        }
    }
}

fn fmt_paren<T>(f: &mut Formatter, faction: Faction, xs: &Vec<T>) -> Result
where
    for<'a> FactionScoped<&'a T>: Display,
{
    for x in xs.iter().with_position() {
        match x {
            Position::Only(x) => write!(f, "{}", FactionScoped::with(faction, x))?,
            Position::First(x) => {
                write!(f, "({},", FactionScoped::with(faction, x))?;
            }
            Position::Middle(x) => {
                write!(f, "{},", FactionScoped::with(faction, x))?;
            }
            Position::Last(x) => {
                write!(f, "{})", FactionScoped::with(faction, x))?;
            }
        }
    }
    Ok(())
}

fn fmt_commentary(f: &mut Formatter, commentary: &Option<String>) -> Result {
    match commentary {
        None => Ok(()),
        Some(commentary) => write!(f, "{{{}}}", commentary),
    }
}

impl Display for FactionScoped<&Action> {
    fn fmt(&self, f: &mut Formatter) -> Result {
        let faction = self.faction;
        match self.value {
            Action::Place {
                pieces,
                at,
                commentary,
            } => {
                fmt_paren(f, faction, pieces)?;
                write!(f, "->")?;
                fmt_paren(f, faction, at)?;
                fmt_commentary(f, &commentary)
            }
            Action::Move {
                pieces,
                from,
                to,
                commentary,
            } => {
                write!(
                    f,
                    "{}{}->{}",
                    FactionScoped::with(faction, pieces),
                    FactionScoped::with(faction, from),
                    FactionScoped::with(faction, to)
                )?;
                fmt_commentary(f, &commentary)
            }
            Action::Remove {
                pieces,
                from,
                commentary,
            } => {
                write!(f, "{}", FactionScoped::with(faction, pieces))?;
                fmt_paren(f, faction, from)?;
                write!(f, "->")?;
                fmt_commentary(f, &commentary)
            }
            Action::RemoveFromGame {
                pieces,
                from,
                commentary,
            } => {
                fmt_paren(f, faction, pieces)?;
                if let Some(from) = from {
                    write!(f, "{}", FactionScoped::with(faction, from))?;
                }
                write!(f, "->*")?;
                fmt_commentary(f, &commentary)
            }
            Action::Reveal {
                pieces,
                to,
                commentary,
                ..
            } => {
                if let Some(pieces) = pieces {
                    fmt_paren(f, faction, pieces)?;
                }
                write!(f, "^")?;
                if let Some(to) = to {
                    write!(f, "{}", to)?;
                }
                fmt_commentary(f, &commentary)
            }
            Action::Expose {
                piece,
                at,
                commentary,
                ..
            } => {
                write!(f, "{}{}?", FactionScoped::with(faction, *piece), at)?;
                fmt_commentary(f, &commentary)
            }
            Action::Flip {
                at,
                obverse,
                reverse,
                commentary,
                ..
            } => {
                write!(
                    f,
                    "{}{}^{}",
                    FactionScoped::with(faction, *obverse),
                    FactionScoped::with(faction, at),
                    FactionScoped::with(faction, *reverse)
                )?;
                fmt_commentary(f, &commentary)
            }
            Action::Swap {
                pieces,
                locations,
                commentary,
            } => {
                write!(
                    f,
                    "{}{}<->{}{}",
                    FactionScoped::with(faction, pieces[0]),
                    FactionScoped::with(faction, &locations[0]),
                    FactionScoped::with(faction, pieces[1]),
                    FactionScoped::with(faction, &locations[1])
                )?;
                fmt_commentary(f, &commentary)
            }
            Action::Battle {
                attacker,
                defender,
                in_,
                loot,
                commentary,
            } => {
                if attacker != &faction {
                    write!(f, "{}", attacker)?;
                }
                write!(f, "{}x{}{}", defender, if *loot { "l" } else { "" }, in_)?;
                fmt_commentary(f, &commentary)
            }
            Action::RollBattleDice {
                attacker,
                defender,
                commentary,
            } => {
                write!(f, "r{}{}", attacker, defender)?;
                fmt_commentary(f, &commentary)
            }
            Action::RollMobDie { suit, commentary } => {
                write!(f, "r{}", suit)?;
                fmt_commentary(f, &commentary)
            }
            Action::Exhaust {
                pieces,
                in_,
                commentary,
                ..
            } => {
                write!(f, "{}", FactionScoped::with(faction, pieces))?;
                if let Some(in_) = in_ {
                    write!(f, "{}", FactionScoped::with(faction, in_))?;
                }
                write!(f, "e")?;
                fmt_commentary(f, &commentary)
            }
            Action::MoveExhaust {
                pieces,
                from,
                to,
                commentary,
            } => {
                write!(
                    f,
                    "{}{}-e>{}",
                    FactionScoped::with(faction, pieces),
                    FactionScoped::with(faction, from),
                    FactionScoped::with(faction, to)
                )?;
                fmt_commentary(f, &commentary)
            }
            Action::Refresh {
                pieces,
                in_,
                commentary,
                ..
            } => {
                write!(f, "{}", FactionScoped::with(faction, pieces))?;
                if let Some(in_) = in_ {
                    write!(f, "{}", FactionScoped::with(faction, in_))?;
                }
                write!(f, "r")?;
                fmt_commentary(f, &commentary)
            }
            Action::MoveRefresh {
                pieces,
                from,
                to,
                commentary,
            } => {
                write!(
                    f,
                    "{}{}-r>{}",
                    FactionScoped::with(faction, pieces),
                    FactionScoped::with(faction, from),
                    FactionScoped::with(faction, to)
                )?;
                fmt_commentary(f, &commentary)
            }
            Action::ScorePoints {
                faction: scorer,
                amount,
                commentary,
            } => {
                if scorer != &faction {
                    write!(f, "{}", scorer)?;
                }
                write!(f, "+{}", amount)?;
                fmt_commentary(f, &commentary)
            }
            Action::LosePoints {
                faction: scorer,
                amount,
                commentary,
            } => {
                if scorer != &faction {
                    write!(f, "{}", scorer)?;
                }
                write!(f, "-{}", amount)?;
                fmt_commentary(f, &commentary)
            }
            Action::SetRelationship {
                faction: actor,
                with,
                to,
                commentary,
            } => {
                if actor != &faction {
                    write!(f, "{}", actor)?;
                }
                write!(f, "m{}={}", with, to)?;
                fmt_commentary(f, &commentary)
            }
            Action::SetOutcast {
                suit,
                hated,
                commentary,
                ..
            } => {
                write!(f, "m={}{}", if *hated { "h" } else { "" }, suit)?;
                fmt_commentary(f, &commentary)
            }
            Action::SetServicePrices {
                hand_card,
                riverboats,
                mercenaries,
                commentary,
                ..
            } => {
                write!(f, "m=({},{},{})", hand_card, riverboats, mercenaries)?;
                fmt_commentary(f, &commentary)
            }
            Action::ActivateDominance {
                coalition,
                suit,
                commentary,
                ..
            } => {
                write!(f, "#{}dom", suit)?;
                if let Some(coalition) = coalition {
                    write!(f, "{}", coalition)?;
                }
                fmt_commentary(f, &commentary)
            }
            Action::RefillDrawPile { commentary } => {
                write!(f, "##")?;
                fmt_commentary(f, &commentary)
            }
        }
    }
}

impl Display for FactionScoped<&Pieces> {
    fn fmt(&self, f: &mut Formatter) -> Result {
        let faction = self.faction;
        let pieces = self.value;

        match pieces {
            Pieces::All => write!(f, "_"),
            Pieces::Specific(qs) => fmt_paren(f, faction, qs),
        }
    }
}

impl Display for FactionScoped<&Quantity> {
    fn fmt(&self, f: &mut Formatter) -> Result {
        let faction = self.faction;
        if self.value.1 > 1 {
            write!(f, "{}", self.value.1)?;
        }
        write!(
            f,
            "{}",
            FactionScoped {
                faction,
                value: self.value.0
            }
        )
    }
}

impl Display for FactionScoped<Piece> {
    fn fmt(&self, f: &mut Formatter) -> Result {
        let faction = self.faction;
        let piece = self.value;
        if let Some(owner) = piece.owner() {
            if owner != faction {
                write!(f, "{}", owner)?;
            }
        }
        match piece {
            Piece::Warrior(_) => write!(f, "w")?,
            Piece::HundredsPiece(HundredsPiece::Warlord) => write!(f, "ww")?,
            Piece::Pawn(_) => write!(f, "p")?,
            Piece::Building(b) => write!(f, "b{}", b)?,
            Piece::Token(t) => write!(f, "t{}", t)?,
            Piece::Item(i, Some(item_state)) => write!(f, "%{}{}", item_state, i)?,
            Piece::Item(i, None) => write!(f, "%{}", i)?,
            Piece::Card(c) => write!(f, "#{}", c)?,
            Piece::Landmark(l) => write!(f, "l{}", l)?,
            Piece::Marker(m) => write!(f, "m{}", m)?,
        }
        Ok(())
    }
}

impl Display for Building {
    fn fmt(&self, f: &mut Formatter) -> Result {
        match self {
            Self::Recruiter => write!(f, "r")?,
            Self::Sawmill => write!(f, "s")?,
            Self::Workshop => write!(f, "w")?,
            Self::Roost => (),
            Self::Base { suit } | Self::Garden { suit } => write!(f, "{}", suit)?,
            Self::Citadel => write!(f, "c")?,
            Self::Market => write!(f, "m")?,
            Self::Stronghold => (),
            Self::Waystation { obverse, reverse } => write!(f, "{}{}", obverse, reverse)?,
        }
        Ok(())
    }
}

impl Display for Token {
    fn fmt(&self, f: &mut Formatter) -> Result {
        match self {
            Self::Wood | Self::Sympathy | Self::Tunnel | Self::Plot { plot: None } | Self::Mob => {
                ()
            }
            Self::Keep => write!(f, "k")?,
            Self::TradePost { suit } => write!(f, "{}", suit)?,
            Self::Plot { plot: Some(plot) } => write!(f, "{}", plot)?,
            Self::Relic { relic, value: None } => write!(f, "{}", relic)?,
            Self::Relic {
                relic,
                value: Some(value),
            } => write!(f, "{}{}", value, relic)?,
        }
        Ok(())
    }
}

impl Display for Plot {
    fn fmt(&self, f: &mut Formatter) -> Result {
        match self {
            Self::Bomb => write!(f, "b")?,
            Self::Extortion => write!(f, "e")?,
            Self::Raid => write!(f, "r")?,
            Self::Snare => write!(f, "s")?,
        }
        Ok(())
    }
}

impl Display for Relic {
    fn fmt(&self, f: &mut Formatter) -> Result {
        match self {
            Self::Figure => write!(f, "f")?,
            Self::Jewelry => write!(f, "j")?,
            Self::Tablet => write!(f, "t")?,
        }
        Ok(())
    }
}

impl Display for Item {
    fn fmt(&self, f: &mut Formatter) -> Result {
        match self {
            Self::Bag => write!(f, "b")?,
            Self::Boot => write!(f, "m")?,
            Self::Coins => write!(f, "c")?,
            Self::Crossbow => write!(f, "x")?,
            Self::Hammer => write!(f, "h")?,
            Self::Sword => write!(f, "s")?,
            Self::Tea => write!(f, "t")?,
            Self::Torch => write!(f, "f")?,
        }
        Ok(())
    }
}

impl Display for ItemState {
    fn fmt(&self, f: &mut Formatter) -> Result {
        match self {
            Self::Refreshed => write!(f, "r")?,
            Self::Exhausted => write!(f, "e")?,
        }
        Ok(())
    }
}

impl Display for Card {
    fn fmt(&self, f: &mut Formatter) -> Result {
        match self {
            Self::DeckCard(suit, name) => {
                if let Some(suit) = suit {
                    write!(f, "{}", suit)?;
                }
                if let Some(name) = name {
                    write!(f, "{}", name)?;
                }
            }
            Self::EyrieLeader(leader) => write!(f, "{}", leader)?,
            Self::EyrieCard(EyrieCard::LoyalVizier) => write!(f, "viz")?,
            Self::VagabondCharacter(character) => write!(f, "{}", character)?,
            Self::DuchyMinister(minister) => write!(f, "{}", minister)?,
            Self::HundredsMood(mood) => write!(f, "{}", mood)?,
            Self::KeepersCard(KeepersCard::FaithfulRetainer) => write!(f, "faith")?,
            Self::Quest(suit, quest) => write!(f, "{}{}", suit, quest)?,
        }
        Ok(())
    }
}

impl Display for EyrieLeader {
    fn fmt(&self, f: &mut Formatter) -> Result {
        match self {
            Self::Builder => write!(f, "builder")?,
            Self::Charismatic => write!(f, "charismatic")?,
            Self::Commander => write!(f, "commander")?,
            Self::Despot => write!(f, "despot")?,
        }
        Ok(())
    }
}

impl Display for VagabondCharacter {
    fn fmt(&self, f: &mut Formatter) -> Result {
        match self {
            Self::Adventurer => write!(f, "adventurer")?,
            Self::Arbiter => write!(f, "arbiter")?,
            Self::Harrier => write!(f, "harrier")?,
            Self::Ranger => write!(f, "ranger")?,
            Self::Ronin => write!(f, "ronin")?,
            Self::Scoundrel => write!(f, "scoundrel")?,
            Self::Thief => write!(f, "thief")?,
            Self::Tinker => write!(f, "tinker")?,
            Self::Vagrant => write!(f, "vagrant")?,
        }
        Ok(())
    }
}

impl Display for DuchyMinister {
    fn fmt(&self, f: &mut Formatter) -> Result {
        match self {
            Self::Foremole => write!(f, "foremole")?,
            Self::Captain => write!(f, "captain")?,
            Self::Marshal => write!(f, "marshal")?,
            Self::Brigadier => write!(f, "brig")?,
            Self::Banker => write!(f, "banker")?,
            Self::Mayor => write!(f, "mayor")?,
            Self::DuchessOfMud => write!(f, "mud")?,
            Self::BaronOfDirt => write!(f, "dirt")?,
            Self::EarlOfStone => write!(f, "stone")?,
        }
        Ok(())
    }
}

impl Display for HundredsMood {
    fn fmt(&self, f: &mut Formatter) -> Result {
        match self {
            Self::Bitter => write!(f, "bitter")?,
            Self::Grandiose => write!(f, "grandiose")?,
            Self::Jubilant => write!(f, "jubilant")?,
            Self::Lavish => write!(f, "lavish")?,
            Self::Relentless => write!(f, "relent")?,
            Self::Rowdy => write!(f, "rowdy")?,
            Self::Stubborn => write!(f, "stubborn")?,
            Self::Wrathful => write!(f, "wrath")?,
        }
        Ok(())
    }
}

impl Display for Landmark {
    fn fmt(&self, f: &mut Formatter) -> Result {
        match self {
            Self::BlackMarket => write!(f, "b")?,
            Self::ElderTreetop => write!(f, "e")?,
            Self::Ferry => write!(f, "f")?,
            Self::LegendaryForge => write!(f, "h")?,
            Self::LostCity => write!(f, "c")?,
            Self::Tower => write!(f, "t")?,
        }
        Ok(())
    }
}

impl Display for Marker {
    fn fmt(&self, f: &mut Formatter) -> Result {
        match self {
            Self::Ruins => write!(f, "r")?,
            Self::ClosedPath => write!(f, "c")?,
        }
        Ok(())
    }
}

impl Display for FactionScoped<&Location> {
    fn fmt(&self, f: &mut Formatter) -> Result {
        use Location::*;
        let faction = self.faction;
        let location = self.value;
        if let Some(owner) = location.owner() {
            match location {
                Hand(_) => (),
                _ if owner != faction => write!(f, "{}", owner)?,
                _ => (),
            }
        }
        match location {
            Clearing(i) => write!(f, "{}", i)?,
            Path(from, to) => write!(f, "{}_{}", from, to)?,
            Forest(clearings) => {
                for c in clearings.iter().with_position() {
                    match c {
                        Position::First(c) | Position::Middle(c) => write!(f, "{}_", c)?,
                        Position::Only(c) | Position::Last(c) => write!(f, "{}", c)?,
                    }
                }
            }
            Crafted(_) => write!(f, "$i")?,
            Hand(owner) => write!(f, "{}", owner)?,
            AvailableDominanceCards => write!(f, "@")?,
            EyrieDecree(column) => write!(f, "${}", column)?,
            EyrieLeader => write!(f, "$")?,
            AllianceSupporters => write!(f, "$")?,
            AllianceOfficers => write!(f, "$o")?,
            VagabondSatchel(_) => write!(f, "$s")?,
            VagabondTrack(_) => write!(f, "$t")?,
            VagabondDamaged(_) => write!(f, "$d")?,
            AvailableQuests => write!(f, "Q")?,
            VagabondCompletedQuests(_) => write!(f, "$q")?,
            VagabondCharacter(_) => write!(f, "$")?,
            CultAcolytes => write!(f, "$a")?,
            CultLostSouls => write!(f, "$")?,
            RiverfolkPayments => write!(f, "$p")?,
            RiverfolkFunds => write!(f, "$f")?,
            RiverfolkCommitted(suit) => {
                write!(f, "$c")?;
                if let Some(suit) = suit {
                    write!(f, "{}", suit)?;
                }
            }
            Burrow => write!(f, "0")?,
            DuchySwayedMinisters => write!(f, "$")?,
            HundredsMood => write!(f, "$")?,
            HundredsHoard => write!(f, "$h")?,
            KeepersRetinue(column) => write!(f, "${}", column)?,
            Supply(_) | DrawPile | Discard => (),
        }
        Ok(())
    }
}

impl Display for EyrieDecreeColumn {
    fn fmt(&self, f: &mut Formatter) -> Result {
        use EyrieDecreeColumn::*;
        match self {
            Recruit => write!(f, "r"),
            Move => write!(f, "m"),
            Battle => write!(f, "x"),
            Build => write!(f, "b"),
        }
    }
}

impl Display for KeepersRetinueColumn {
    fn fmt(&self, f: &mut Formatter) -> Result {
        match self {
            Self::Move => write!(f, "m"),
            Self::BattleDelve => write!(f, "x"),
            Self::MoveRecover => write!(f, "r"),
        }
    }
}

impl Display for Relationship {
    fn fmt(&self, f: &mut Formatter) -> Result {
        match self {
            Self::Hostile => write!(f, "h"),
            Self::Indifferent => write!(f, "0"),
            Self::Allied1 => write!(f, "1"),
            Self::Allied2 => write!(f, "2"),
            Self::Allied => write!(f, "a"),
        }
    }
}

impl Display for Quest {
    fn fmt(&self, f: &mut Formatter) -> Result {
        match self {
            Self::Errand => write!(f, "errand"),
            Self::Escort => write!(f, "escort"),
            Self::ExpelBandits => write!(f, "bandits"),
            Self::FendOffABear => write!(f, "bear"),
            Self::Fundraising => write!(f, "funds"),
            Self::GiveASpeech => write!(f, "speech"),
            Self::GuardDuty => write!(f, "guard"),
            Self::LogisticsHelp => write!(f, "logs"),
            Self::RepairAShed => write!(f, "shed"),
        }
    }
}
