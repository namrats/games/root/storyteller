pub mod action;
pub mod game;
pub mod map;
pub mod parser;
pub mod piece;
pub mod printer;
pub mod state;

#[cfg(target_arch = "wasm32")]
mod web;
