use counter::Counter;
use serde::{
    de::{self, Deserializer},
    ser::Serializer,
    Deserialize, Serialize,
};
use std::fmt;
use strum::{EnumIter, IntoEnumIterator};

pub type ClearingNumber = usize;

pub type Roll = u8;

#[derive(PartialEq, Eq, EnumIter, Serialize, Deserialize, Hash, Clone, Copy, Debug)]
#[serde(rename_all = "camelCase")]
pub enum Suit {
    Fox,
    Mouse,
    Rabbit,
}

#[derive(PartialEq, Eq, Hash, Clone, Copy, Debug)]
pub enum Faction {
    Marquise,
    Eyrie,
    Alliance,
    Vagabond(usize),
    Cult,
    Riverfolk,
    Duchy,
    Corvid,
    Hundreds,
    Keepers,
}

impl Serialize for Faction {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        use Faction::*;
        let s = match self {
            Marquise => "marquise".to_owned(),
            Eyrie => "eyrie".to_owned(),
            Alliance => "alliance".to_owned(),
            Vagabond(i) => format!("vagabond{i}"),
            Cult => "cult".to_owned(),
            Riverfolk => "riverfolk".to_owned(),
            Duchy => "duchy".to_owned(),
            Corvid => "corvid".to_owned(),
            Hundreds => "hundreds".to_owned(),
            Keepers => "keepers".to_owned(),
        };
        serializer.serialize_str(&s)
    }
}

struct FactionVisitor;

impl<'de> de::Visitor<'de> for FactionVisitor {
    type Value = Faction;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str("a faction")
    }

    fn visit_str<E>(self, value: &str) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        value.parse().map_err(|e| de::Error::custom(e))
    }
}

impl<'de> Deserialize<'de> for Faction {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_string(FactionVisitor)
    }
}

impl Faction {
    pub fn starting_pieces(&self) -> Counter<Piece> {
        match self {
            Faction::Marquise => vec![
                (Piece::Warrior(Faction::Marquise), 25),
                (Piece::Building(Building::Recruiter), 6),
                (Piece::Building(Building::Sawmill), 6),
                (Piece::Building(Building::Workshop), 6),
                (Piece::Token(Token::Keep), 1),
                (Piece::Token(Token::Wood), 8),
            ]
            .into_iter()
            .collect(),
            Faction::Eyrie => vec![
                (Piece::Warrior(Faction::Eyrie), 20),
                (Piece::Building(Building::Roost), 7),
            ]
            .into_iter()
            .chain(EyrieLeader::iter().map(|leader| (Piece::Card(Card::EyrieLeader(leader)), 1)))
            .collect(),
            Faction::Alliance => vec![
                (Piece::Warrior(Faction::Alliance), 10),
                (Piece::Token(Token::Sympathy), 10),
            ]
            .into_iter()
            .chain(Suit::iter().map(|suit| (Piece::Building(Building::Base { suit }), 1)))
            .collect(),
            Faction::Vagabond(i) => vec![(Piece::Pawn(Faction::Vagabond(*i)), 1)]
                .into_iter()
                .collect(),
            Faction::Cult => vec![(Piece::Warrior(Faction::Cult), 25)]
                .into_iter()
                .chain(Suit::iter().map(|suit| (Piece::Building(Building::Garden { suit }), 5)))
                .collect(),
            Faction::Riverfolk => vec![(Piece::Warrior(Faction::Riverfolk), 15)]
                .into_iter()
                .chain(Suit::iter().map(|suit| (Piece::Token(Token::TradePost { suit }), 3)))
                .collect(),
            Faction::Duchy => vec![
                (Piece::Warrior(Faction::Duchy), 20),
                (Piece::Token(Token::Tunnel), 3),
                (Piece::Building(Building::Citadel), 3),
                (Piece::Building(Building::Market), 3),
            ]
            .into_iter()
            .chain(
                DuchyMinister::iter()
                    .map(|minister| (Piece::Card(Card::DuchyMinister(minister)), 1)),
            )
            .collect(),
            Faction::Corvid => vec![
                (Piece::Warrior(Faction::Corvid), 15),
                (Piece::Token(Token::Plot { plot: None }), 12),
            ]
            .into_iter()
            .collect(),
            Faction::Hundreds => vec![
                (Piece::Warrior(Faction::Hundreds), 20),
                (Piece::Building(Building::Stronghold), 6),
                (Piece::Token(Token::Mob), 5),
            ]
            .into_iter()
            .chain(HundredsMood::iter().map(|mood| (Piece::Card(Card::HundredsMood(mood)), 1)))
            .collect(),
            Faction::Keepers => vec![
                (Piece::Warrior(Faction::Keepers), 15),
                (
                    Piece::Building(Building::Waystation {
                        obverse: Relic::Figure,
                        reverse: Relic::Jewelry,
                    }),
                    1,
                ),
                (
                    Piece::Building(Building::Waystation {
                        obverse: Relic::Jewelry,
                        reverse: Relic::Tablet,
                    }),
                    1,
                ),
                (
                    Piece::Building(Building::Waystation {
                        obverse: Relic::Tablet,
                        reverse: Relic::Figure,
                    }),
                    1,
                ),
            ]
            .into_iter()
            .chain(
                Relic::iter().map(|relic| (Piece::Token(Token::Relic { relic, value: None }), 4)),
            )
            .collect(),
        }
    }

    pub fn faction_locations(&self) -> Vec<Location> {
        match self {
            Self::Eyrie => vec![
                Location::Crafted(*self),
                Location::EyrieLeader,
                Location::EyrieDecree(EyrieDecreeColumn::Recruit),
            ],
            Self::Alliance => vec![
                Location::Crafted(*self),
                Location::AllianceOfficers,
                Location::AllianceSupporters,
            ],
            &Self::Vagabond(i) => vec![
                Location::VagabondCharacter(i),
                Location::VagabondTrack(i),
                Location::VagabondSatchel(i),
                Location::VagabondDamaged(i),
                Location::VagabondCompletedQuests(i),
            ],
            Self::Cult => vec![
                Location::Crafted(*self),
                Location::CultAcolytes,
                Location::CultLostSouls,
            ],
            Self::Riverfolk => vec![
                Location::Crafted(*self),
                Location::RiverfolkPayments,
                Location::RiverfolkFunds,
                Location::RiverfolkCommitted(None),
                Location::RiverfolkCommitted(Some(Suit::Fox)),
                Location::RiverfolkCommitted(Some(Suit::Mouse)),
                Location::RiverfolkCommitted(Some(Suit::Rabbit)),
            ],
            Self::Duchy => vec![
                Location::Crafted(*self),
                Location::Burrow,
                Location::DuchySwayedMinisters,
            ],
            Self::Hundreds => vec![Location::HundredsMood, Location::HundredsHoard],
            Self::Keepers => vec![
                Location::Crafted(*self),
                Location::KeepersRetinue(KeepersRetinueColumn::Move),
                Location::KeepersRetinue(KeepersRetinueColumn::BattleDelve),
                Location::KeepersRetinue(KeepersRetinueColumn::MoveRecover),
            ],
            _ => vec![Location::Crafted(*self)],
        }
    }
}

#[derive(PartialEq, Eq, EnumIter, Serialize, Deserialize, Hash, Clone, Copy, Debug)]
pub enum EyrieLeader {
    Builder,
    Charismatic,
    Commander,
    Despot,
}

#[derive(PartialEq, Eq, Serialize, Deserialize, Hash, Clone, Copy, Debug)]
#[serde(rename_all = "camelCase")]
pub enum EyrieDecreeColumn {
    Recruit,
    Move,
    Battle,
    Build,
}

#[derive(PartialEq, Eq, EnumIter, Serialize, Deserialize, Hash, Clone, Copy, Debug)]
pub enum VagabondCharacter {
    Adventurer,
    Arbiter,
    Harrier,
    Ranger,
    Ronin,
    Scoundrel,
    Thief,
    Tinker,
    Vagrant,
}

impl VagabondCharacter {
    pub fn starting_satchel_items(&self) -> Vec<Item> {
        match self {
            Self::Adventurer => vec![Item::Torch, Item::Boot, Item::Hammer],
            Self::Arbiter => vec![Item::Torch, Item::Boot, Item::Sword, Item::Sword],
            Self::Harrier => vec![Item::Torch, Item::Crossbow, Item::Sword],
            Self::Ranger => vec![Item::Torch, Item::Boot, Item::Crossbow, Item::Sword],
            Self::Ronin => vec![Item::Torch, Item::Boot, Item::Boot, Item::Sword],
            Self::Scoundrel => vec![Item::Torch, Item::Boot, Item::Boot, Item::Crossbow],
            Self::Thief => vec![Item::Torch, Item::Boot, Item::Sword],
            Self::Tinker => vec![Item::Torch, Item::Boot, Item::Hammer],
            Self::Vagrant => vec![Item::Torch, Item::Boot],
        }
    }

    pub fn starting_track_items(&self) -> Vec<Item> {
        match self {
            Self::Harrier | Self::Vagrant => vec![Item::Coins],
            Self::Thief => vec![Item::Tea],
            Self::Tinker => vec![Item::Bag],
            _ => vec![],
        }
    }
}

#[derive(PartialEq, Eq, Serialize, Deserialize, Hash, Clone, Copy, Debug)]
#[serde(rename_all = "camelCase", tag = "type")]
pub enum Building {
    Roost,
    Recruiter,
    Sawmill,
    Workshop,
    Base { suit: Suit },
    Garden { suit: Suit },
    Citadel,
    Market,
    Stronghold,
    Waystation { obverse: Relic, reverse: Relic },
}

impl Building {
    pub fn owner(&self) -> Faction {
        match &self {
            Building::Recruiter | Building::Sawmill | Building::Workshop => Faction::Marquise,
            Building::Roost => Faction::Eyrie,
            Building::Base { .. } => Faction::Alliance,
            Building::Garden { .. } => Faction::Cult,
            Building::Citadel | Building::Market => Faction::Duchy,
            Building::Stronghold => Faction::Hundreds,
            Building::Waystation { .. } => Faction::Keepers,
        }
    }
}

#[derive(PartialEq, Eq, EnumIter, Serialize, Deserialize, Hash, Clone, Copy, Debug)]
#[serde(rename_all = "camelCase")]
pub enum Plot {
    Bomb,
    Snare,
    Extortion,
    Raid,
}

#[derive(PartialEq, Eq, EnumIter, Serialize, Deserialize, Hash, Clone, Copy, Debug)]
#[serde(rename_all = "camelCase")]
pub enum Relic {
    Figure,
    Jewelry,
    Tablet,
}

#[derive(PartialEq, Eq, Serialize, Deserialize, Hash, Clone, Copy, Debug)]
#[serde(rename_all = "camelCase", tag = "type")]
pub enum Token {
    Keep,
    Wood,
    Sympathy,
    TradePost { suit: Suit },
    Tunnel,
    Plot { plot: Option<Plot> },
    Mob,
    Relic { relic: Relic, value: Option<usize> },
}

impl Token {
    pub fn owner(&self) -> Faction {
        match &self {
            Token::Keep | Token::Wood => Faction::Marquise,
            Token::Sympathy => Faction::Alliance,
            Token::TradePost { .. } => Faction::Riverfolk,
            Token::Tunnel => Faction::Duchy,
            Token::Plot { .. } => Faction::Corvid,
            Token::Mob => Faction::Hundreds,
            Token::Relic { .. } => Faction::Keepers,
        }
    }
}

#[derive(PartialEq, Eq, Serialize, Deserialize, Hash, Clone, Copy, Debug)]
#[serde(rename_all = "camelCase")]
pub enum Item {
    Bag,
    Boot,
    Coins,
    Crossbow,
    Hammer,
    Sword,
    Tea,
    Torch,
}

#[derive(PartialEq, Eq, Serialize, Deserialize, Hash, Clone, Copy, Debug)]
#[serde(rename_all = "camelCase")]
pub enum ItemState {
    Exhausted,
    Refreshed,
}

#[derive(PartialEq, Eq, Serialize, Deserialize, Hash, Clone, Copy, Debug)]
#[serde(rename_all = "camelCase")]
pub enum CardSuit {
    Bird,
    Fox,
    Mouse,
    Rabbit,
}

#[derive(PartialEq, Eq, Serialize, Deserialize, Hash, Clone, Copy, Debug)]
pub enum CardName {
    Ambush,
    Dominance,
    Bag,
    Boot,
    Coins,
    Crossbow,
    Hammer,
    Sword,
    Tea,
    Armorers,
    BetterBurrowBank,
    BrutalTactics,
    CommandWarren,
    Cobbler,
    Codebreakers,
    Favor,
    RoyalClaim,
    Sappers,
    ScoutingParty,
    StandAndDeliver,
    TaxCollector,
    BoatBuilders,
    CharmOffensive,
    CoffinMakers,
    CorvidPlanners,
    EyrieEmigree,
    FalseOrders,
    Partisans,
    Informants,
    LeagueOfAdventurousMice,
    MasterEngravers,
    MurineBroker,
    PropagandaBureau,
    Saboteurs,
    SoupKitchens,
    SwapMeet,
    Tunnels,
}

#[derive(PartialEq, Eq, Serialize, Deserialize, Hash, Clone, Copy, Debug)]
pub enum EyrieCard {
    LoyalVizier,
}

#[derive(PartialEq, Eq, Serialize, Deserialize, Hash, Clone, Copy, Debug)]
pub enum Quest {
    Errand,
    Escort,
    ExpelBandits,
    FendOffABear,
    Fundraising,
    GiveASpeech,
    GuardDuty,
    LogisticsHelp,
    RepairAShed,
}

#[derive(PartialEq, Eq, EnumIter, Serialize, Deserialize, Hash, Clone, Copy, Debug)]
pub enum DuchyMinister {
    Foremole,
    Captain,
    Marshal,
    Brigadier,
    Banker,
    Mayor,
    DuchessOfMud,
    BaronOfDirt,
    EarlOfStone,
}

#[derive(PartialEq, Eq, EnumIter, Serialize, Deserialize, Hash, Clone, Copy, Debug)]
pub enum HundredsMood {
    Bitter,
    Grandiose,
    Jubilant,
    Lavish,
    Relentless,
    Rowdy,
    Stubborn,
    Wrathful,
}

#[derive(PartialEq, Eq, EnumIter, Serialize, Deserialize, Hash, Clone, Copy, Debug)]
pub enum KeepersCard {
    FaithfulRetainer,
}

#[derive(PartialEq, Eq, Serialize, Deserialize, Hash, Clone, Copy, Debug)]
#[serde(rename_all = "camelCase")]
pub enum Card {
    DeckCard(Option<CardSuit>, Option<CardName>),
    EyrieLeader(EyrieLeader),
    EyrieCard(EyrieCard),
    VagabondCharacter(VagabondCharacter),
    Quest(Suit, Quest),
    DuchyMinister(DuchyMinister),
    HundredsMood(HundredsMood),
    KeepersCard(KeepersCard),
}

#[derive(PartialEq, Eq, Serialize, Deserialize, Hash, Clone, Copy, Debug)]
#[serde(rename_all = "camelCase")]
pub enum Landmark {
    BlackMarket,
    ElderTreetop,
    Ferry,
    LegendaryForge,
    LostCity,
    Tower,
}

#[derive(PartialEq, Eq, Serialize, Deserialize, Hash, Clone, Copy, Debug)]
#[serde(rename_all = "camelCase")]
pub enum Marker {
    Ruins,
    ClosedPath,
}

#[derive(PartialEq, Eq, Serialize, Deserialize, Hash, Clone, Copy, Debug)]
#[serde(rename_all = "camelCase")]
pub enum HundredsPiece {
    Warlord,
}

#[derive(PartialEq, Eq, Serialize, Deserialize, Hash, Clone, Copy, Debug)]
#[serde(rename_all = "camelCase")]
pub enum Piece {
    Warrior(Faction),
    HundredsPiece(HundredsPiece),
    Pawn(Faction),
    Building(Building),
    Token(Token),
    Item(Item, Option<ItemState>),
    Card(Card),
    Landmark(Landmark),
    Marker(Marker),
}

impl Piece {
    pub fn owner(&self) -> Option<Faction> {
        match self {
            Piece::Warrior(f) => Some(*f),
            Piece::Pawn(f) => Some(*f),
            Piece::Building(b) => Some(b.owner()),
            Piece::Token(t) => Some(t.owner()),
            Piece::Card(Card::EyrieLeader(_)) => Some(Faction::Eyrie),
            Piece::Card(Card::DuchyMinister(_)) => Some(Faction::Duchy),
            Piece::Card(Card::HundredsMood(_)) => Some(Faction::Hundreds),
            Piece::HundredsPiece(_) => Some(Faction::Hundreds),
            _ => None,
        }
    }

    pub fn exhausted(&self) -> Self {
        match self {
            Piece::Item(i, _) => Piece::Item(*i, Some(ItemState::Exhausted)),
            _ => *self,
        }
    }

    pub fn refreshed(&self) -> Self {
        match self {
            Piece::Item(i, _) => Piece::Item(*i, Some(ItemState::Refreshed)),
            _ => *self,
        }
    }
}

#[derive(PartialEq, Serialize, Deserialize, Eq, Clone, Copy, Debug)]
#[serde(rename_all = "camelCase")]
pub enum Relationship {
    Hostile,
    Indifferent,
    Allied1,
    Allied2,
    Allied,
}

#[derive(PartialEq, Eq, Serialize, Deserialize, Hash, Clone, Copy, Debug)]
#[serde(rename_all = "camelCase")]
pub enum KeepersRetinueColumn {
    Move,
    BattleDelve,
    MoveRecover,
}

#[derive(PartialEq, Eq, Serialize, Deserialize, Hash, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub enum Location {
    Clearing(ClearingNumber),
    Path(ClearingNumber, ClearingNumber),
    Forest(Vec<ClearingNumber>),
    Crafted(Faction),
    Hand(Faction),
    Supply(Option<Faction>),
    DrawPile,
    Discard,
    AvailableDominanceCards,
    EyrieDecree(EyrieDecreeColumn),
    EyrieLeader,
    AllianceSupporters,
    AllianceOfficers,
    VagabondSatchel(usize),
    VagabondTrack(usize),
    VagabondDamaged(usize),
    AvailableQuests,
    VagabondCompletedQuests(usize),
    VagabondCharacter(usize),
    CultAcolytes,
    CultLostSouls,
    RiverfolkPayments,
    RiverfolkFunds,
    RiverfolkCommitted(Option<Suit>),
    Burrow,
    DuchySwayedMinisters,
    HundredsMood,
    HundredsHoard,
    KeepersRetinue(KeepersRetinueColumn),
}

impl Location {
    pub fn owner(&self) -> Option<Faction> {
        use Location::*;
        match self {
            Hand(f) => Some(*f),
            Supply(Some(f)) => Some(*f),
            Crafted(f) => Some(*f),
            EyrieDecree(_) | EyrieLeader => Some(Faction::Eyrie),
            AllianceSupporters | AllianceOfficers => Some(Faction::Alliance),
            VagabondSatchel(i)
            | VagabondTrack(i)
            | VagabondDamaged(i)
            | VagabondCompletedQuests(i)
            | VagabondCharacter(i) => Some(Faction::Vagabond(*i)),
            CultAcolytes | CultLostSouls => Some(Faction::Cult),
            RiverfolkPayments | RiverfolkFunds | RiverfolkCommitted(_) => Some(Faction::Riverfolk),
            DuchySwayedMinisters => Some(Faction::Duchy),
            HundredsMood | HundredsHoard => Some(Faction::Hundreds),
            KeepersRetinue(_) => Some(Faction::Keepers),
            _ => None,
        }
    }
}
