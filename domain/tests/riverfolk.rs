use rstest::rstest;
use storyteller_domain::{
    action::{Action, Pieces},
    map::Map,
    piece::{Card, CardName, CardSuit, Faction, Location, Piece, Suit, Token},
    state::State,
};

#[rstest]
fn setup_service_prices() {
    let action = Action::parse(Faction::Riverfolk, "m=(4,1,2)").expect("couldn't parse action");

    let expected = Action::SetServicePrices {
        faction: Faction::Riverfolk,
        hand_card: 4,
        riverboats: 1,
        mercenaries: 2,
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn place_in_payments() {
    let action = Action::parse(Faction::Riverfolk, "3w->$p").expect("couldn't parse action");

    let expected = Action::Place {
        pieces: vec![(Piece::Warrior(Faction::Riverfolk), 3)],
        at: vec![Location::RiverfolkPayments],
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn pay_for_services() {
    let action = Action::parse(Faction::Duchy, "3w->O$p").expect("couldn't parse action");

    let expected = Action::Place {
        pieces: vec![(Piece::Warrior(Faction::Duchy), 3)],
        at: vec![Location::RiverfolkPayments],
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn gather_funds() {
    let action = Action::parse(Faction::Riverfolk, "3w$p->$f").expect("couldn't parse action");

    let expected = Action::Move {
        pieces: Pieces::Specific(vec![(Piece::Warrior(Faction::Riverfolk), 3)]),
        from: Location::RiverfolkPayments,
        to: Location::RiverfolkFunds,
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn commit_warrior() {
    let action = Action::parse(Faction::Riverfolk, "Lw$f->$c").expect("couldn't parse action");

    let expected = Action::Move {
        pieces: Pieces::Specific(vec![(Piece::Warrior(Faction::Cult), 1)]),
        from: Location::RiverfolkFunds,
        to: Location::RiverfolkCommitted(None),
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn craft_card() {
    let action =
        Action::parse(Faction::Riverfolk, "(w,Lw,Ew)$f->$cf").expect("couldn't parse action");

    let expected = Action::Move {
        pieces: Pieces::Specific(vec![
            (Piece::Warrior(Faction::Riverfolk), 1),
            (Piece::Warrior(Faction::Cult), 1),
            (Piece::Warrior(Faction::Eyrie), 1),
        ]),
        from: Location::RiverfolkFunds,
        to: Location::RiverfolkCommitted(Some(Suit::Fox)),
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn place_trade_post_with_garrison() {
    let action = Action::parse(Faction::Riverfolk, "(w,tf)->1").expect("couldn't parse action");

    let expected = Action::Place {
        pieces: vec![
            (Piece::Warrior(Faction::Riverfolk), 1),
            (Piece::Token(Token::TradePost { suit: Suit::Fox }), 1),
        ],
        at: vec![Location::Clearing(1)],
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn remove_trade_post() {
    let action = Action::parse(Faction::Riverfolk, "tm1->*").expect("couldn't parse action");

    let expected = Action::RemoveFromGame {
        pieces: vec![(Piece::Token(Token::TradePost { suit: Suit::Mouse }), 1)],
        from: Some(Location::Clearing(1)),
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn draw_cards() {
    let action =
        Action::parse(Faction::Riverfolk, "(#rc,#mc,#bs)->O").expect("couldn't parse action");

    let state = State::builder()
        .map(Map::autumn())
        .faction(Faction::Riverfolk)
        .deck()
        .build();

    let expected = State::builder()
        .map(Map::autumn())
        .faction(Faction::Riverfolk)
        .draw_pile(51)
        .hand_cards(
            Faction::Riverfolk,
            vec![
                Card::DeckCard(Some(CardSuit::Rabbit), Some(CardName::Coins)),
                Card::DeckCard(Some(CardSuit::Mouse), Some(CardName::Coins)),
                Card::DeckCard(Some(CardSuit::Bird), Some(CardName::Sword)),
            ],
        )
        .build();

    assert_eq!(expected, state.act(&action));
}

#[rstest]
fn buy_card() {
    let action = Action::parse(Faction::Riverfolk, "#bsO->M").expect("couldn't parse action");

    let state = State::builder()
        .map(Map::autumn())
        .faction(Faction::Riverfolk)
        .faction(Faction::Marquise)
        .draw_pile(51)
        .hand_cards(
            Faction::Riverfolk,
            vec![
                Card::DeckCard(Some(CardSuit::Rabbit), Some(CardName::Coins)),
                Card::DeckCard(Some(CardSuit::Mouse), Some(CardName::Coins)),
                Card::DeckCard(Some(CardSuit::Bird), Some(CardName::Sword)),
            ],
        )
        .build();

    let expected = State::builder()
        .map(Map::autumn())
        .faction(Faction::Riverfolk)
        .faction(Faction::Marquise)
        .draw_pile(51)
        .hand_cards(
            Faction::Riverfolk,
            vec![
                Card::DeckCard(Some(CardSuit::Rabbit), Some(CardName::Coins)),
                Card::DeckCard(Some(CardSuit::Mouse), Some(CardName::Coins)),
            ],
        )
        .hand_cards(Faction::Marquise, vec![Card::DeckCard(None, None)])
        .build();

    assert_eq!(expected, state.act(&action));
}
