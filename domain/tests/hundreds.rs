use rstest::rstest;
use storyteller_domain::{
    action::{Action, Pieces},
    piece::{
        Building, Card, Faction, HundredsMood, HundredsPiece, Item, Location, Piece, Suit, Token,
    },
};

#[rstest]
fn setup_garrison() {
    let action = Action::parse(Faction::Hundreds, "(ww,4w,b)->3").expect("couldn't parse action");

    let expected = Action::Place {
        pieces: vec![
            (Piece::HundredsPiece(HundredsPiece::Warlord), 1),
            (Piece::Warrior(Faction::Hundreds), 4),
            (Piece::Building(Building::Stronghold), 1),
        ],
        at: vec![Location::Clearing(3)],
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn setup_mood() {
    let action = Action::parse(Faction::Hundreds, "#stubborn->$").expect("couldn't parse action");

    let expected = Action::Place {
        pieces: vec![(Piece::Card(Card::HundredsMood(HundredsMood::Stubborn)), 1)],
        at: vec![Location::HundredsMood],
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn incite() {
    let action = Action::parse(Faction::Hundreds, "t->3").expect("couldn't parse action");

    let expected = Action::Place {
        pieces: vec![(Piece::Token(Token::Mob), 1)],
        at: vec![Location::Clearing(3)],
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn move_with_warlord() {
    let action = Action::parse(Faction::Hundreds, "ww3->10").expect("couldn't parse action");

    let expected = Action::Move {
        pieces: Pieces::Specific(vec![(Piece::HundredsPiece(HundredsPiece::Warlord), 1)]),
        from: Location::Clearing(3),
        to: Location::Clearing(10),
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn remove_warlord_on_defense() {
    let action = Action::parse(Faction::Alliance, "(Hww,2Hw,w)3->").expect("couldn't parse action");

    let expected = Action::Remove {
        pieces: Pieces::Specific(vec![
            (Piece::HundredsPiece(HundredsPiece::Warlord), 1),
            (Piece::Warrior(Faction::Hundreds), 2),
            (Piece::Warrior(Faction::Alliance), 1),
        ]),
        from: vec![Location::Clearing(3)],
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn roll_mob_die() {
    let action =
        Action::parse(Faction::Hundreds, "rf{it always rolls fox}").expect("couldn't parse action");

    let expected = Action::RollMobDie {
        suit: Suit::Fox,
        commentary: Some("it always rolls fox".to_string()),
    };

    assert_eq!(expected, action);
}

#[rstest]
fn craft_item_to_hoard() {
    let action = Action::parse(Faction::Hundreds, "%b->$h").expect("couldn't parse action");

    let expected = Action::Place {
        pieces: vec![(Piece::Item(Item::Bag, None), 1)],
        at: vec![Location::HundredsHoard],
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn gain_ruin_item() {
    let action = Action::parse(Faction::Hundreds, "%s10->$h").expect("couldn't parse action");

    let expected = Action::Move {
        pieces: Pieces::Specific(vec![(Piece::Item(Item::Sword, None), 1)]),
        from: Location::Clearing(10),
        to: Location::HundredsHoard,
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn lavish_item() {
    let action = Action::parse(Faction::Hundreds, "%s$h->*").expect("couldn't parse action");

    let expected = Action::RemoveFromGame {
        pieces: vec![(Piece::Item(Item::Sword, None), 1)],
        from: Some(Location::HundredsHoard),
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn craft_item_for_points() {
    let action = Action::parse(Faction::Hundreds, "%s->*").expect("couldn't parse action");

    let expected = Action::RemoveFromGame {
        pieces: vec![(Piece::Item(Item::Sword, None), 1)],
        from: None,
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn declare_loot() {
    let action = Action::parse(Faction::Hundreds, "Exl1").expect("couldn't parse action");

    let expected = Action::Battle {
        attacker: Faction::Hundreds,
        defender: Faction::Eyrie,
        in_: 1,
        loot: true,
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn loot_item() {
    let action = Action::parse(Faction::Hundreds, "%sM$i->$h").expect("couldn't parse action");

    let expected = Action::Move {
        pieces: Pieces::Specific(vec![(Piece::Item(Item::Sword, None), 1)]),
        from: Location::Crafted(Faction::Marquise),
        to: Location::HundredsHoard,
        commentary: None,
    };

    assert_eq!(expected, action);
}
