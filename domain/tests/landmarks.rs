use rstest::rstest;
use storyteller_domain::{
    action::Action,
    map::Map,
    piece::{Faction, Landmark, Marker, Piece},
    state::State,
};

#[rstest]
#[case("f", Landmark::Ferry)]
#[case("t", Landmark::Tower)]
#[case("c", Landmark::LostCity)]
fn place_landmark(#[case] l: &str, #[case] landmark: Landmark) {
    let state = State::builder()
        .map(Map::autumn())
        .landmark(landmark)
        .faction(Faction::Marquise)
        .build();
    let action = Action::parse(Faction::Marquise, &format!("l{l}->1")).unwrap();
    let expected = State::builder()
        .map(Map::autumn())
        .faction(Faction::Marquise)
        .clearing(1, vec![(Piece::Landmark(landmark), 1)])
        .build();

    assert_ne!(state, expected);
    assert_eq!(expected, state.act(&action));
}

#[rstest]
fn move_with_ferry() {
    let state = State::builder()
        .map(Map::autumn())
        .faction(Faction::Marquise)
        .clearing(
            1,
            vec![
                (Piece::Landmark(Landmark::Ferry), 1),
                (Piece::Warrior(Faction::Marquise), 1),
            ],
        )
        .build();
    let action = Action::parse(Faction::Marquise, "(w,lf)1->12").unwrap();
    let expected = State::builder()
        .map(Map::autumn())
        .faction(Faction::Marquise)
        .clearing(1, vec![])
        .clearing(
            12,
            vec![
                (Piece::Marker(Marker::Ruins), 1),
                (Piece::Landmark(Landmark::Ferry), 1),
                (Piece::Warrior(Faction::Marquise), 1),
            ],
        )
        .build();

    assert_ne!(state, expected);
    assert_eq!(expected, state.act(&action));
}
