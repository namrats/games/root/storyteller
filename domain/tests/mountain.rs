use rstest::rstest;
use storyteller_domain::{action::Action, map::Map, piece::Faction, state::State};

#[rstest]
fn open_path() {
    let state = State::builder()
        .map(Map::mountain())
        .faction(Faction::Marquise)
        .build();
    let action = Action::parse(Faction::Marquise, &format!("mc5_9->*")).unwrap();
    let expected = State::builder()
        .map(Map::mountain())
        .path(5, 9, vec![])
        .faction(Faction::Marquise)
        .build();

    assert_ne!(state, expected);
    assert_eq!(expected, state.act(&action));
}
