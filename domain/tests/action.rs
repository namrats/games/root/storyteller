use rstest::rstest;
use storyteller_domain::{
    action::Action,
    map::Map,
    piece::{
        Building, Card, CardName, CardSuit, Faction, HundredsMood, Item, ItemState, Marker, Piece,
        Plot, Token, VagabondCharacter,
    },
    state::State,
};

#[rstest]
fn place_wood() {
    let state = State::builder()
        .map(Map::autumn())
        .faction(Faction::Marquise)
        .build();
    let action = Action::parse(Faction::Marquise, "t->1").unwrap();
    let expected = State::builder()
        .map(Map::autumn())
        .faction(Faction::Marquise)
        .supply(Faction::Marquise, Piece::Token(Token::Wood), 7)
        .clearing(1, vec![(Piece::Token(Token::Wood), 1)])
        .build();

    assert_ne!(state, expected);
    assert_eq!(expected, state.act(&action));
}

#[rstest]
fn place_keep() {
    let state = State::builder()
        .map(Map::autumn())
        .faction(Faction::Marquise)
        .build();
    let action = Action::parse(Faction::Marquise, "tk->1").unwrap();
    let expected = State::builder()
        .map(Map::autumn())
        .faction(Faction::Marquise)
        .supply(Faction::Marquise, Piece::Token(Token::Keep), 0)
        .clearing(1, vec![(Piece::Token(Token::Keep), 1)])
        .build();

    assert_ne!(state, expected);
    assert_eq!(expected, state.act(&action));
}

#[rstest]
fn place_building() {
    let state = State::builder()
        .map(Map::autumn())
        .faction(Faction::Marquise)
        .build();
    let action = Action::parse(Faction::Marquise, "br->1").unwrap();
    let expected = State::builder()
        .map(Map::autumn())
        .faction(Faction::Marquise)
        .supply(Faction::Marquise, Piece::Building(Building::Recruiter), 5)
        .clearing(1, vec![(Piece::Building(Building::Recruiter), 1)])
        .build();

    assert_ne!(state, expected);
    assert_eq!(expected, state.act(&action));
}

#[rstest]
fn place_warriors() {
    let state = State::builder()
        .map(Map::autumn())
        .faction(Faction::Marquise)
        .build();
    let action = Action::parse(Faction::Marquise, "5w->1").unwrap();
    let expected = State::builder()
        .map(Map::autumn())
        .faction(Faction::Marquise)
        .supply(Faction::Marquise, Piece::Warrior(Faction::Marquise), 20)
        .clearing(1, vec![(Piece::Warrior(Faction::Marquise), 5)])
        .build();

    assert_ne!(state, expected);
    assert_eq!(expected, state.act(&action));
}

#[rstest]
fn draw_card() {
    let state = State::builder()
        .map(Map::autumn())
        .deck()
        .faction(Faction::Marquise)
        .build();
    let action = Action::parse(Faction::Marquise, "#->M").unwrap();
    let expected = State::builder()
        .map(Map::autumn())
        .faction(Faction::Marquise)
        .draw_pile(53)
        .hand(Faction::Marquise, 1)
        .build();

    assert_ne!(state, expected);
    assert_eq!(expected, state.act(&action));
}

#[rstest]
fn craft_item() {
    let state = State::builder()
        .map(Map::autumn())
        .default_items()
        .faction(Faction::Marquise)
        .build();
    let action = Action::parse(Faction::Marquise, "%s->$i").unwrap();
    let expected = State::builder()
        .map(Map::autumn())
        .default_items()
        .item(Item::Sword, 1)
        .faction(Faction::Marquise)
        .crafted(
            Faction::Marquise,
            vec![(Piece::Item(Item::Sword, Some(ItemState::Refreshed)), 1)],
        )
        .build();

    assert_ne!(state, expected);
    assert_eq!(expected, state.act(&action));
}

#[rstest]
fn craft_improvement() {
    let state = State::builder()
        .map(Map::autumn())
        .faction(Faction::Marquise)
        .hand(Faction::Marquise, 1)
        .build();
    let action = Action::parse(Faction::Marquise, "#bboatM->$i").unwrap();
    let expected = State::builder()
        .map(Map::autumn())
        .faction(Faction::Marquise)
        .hand(Faction::Marquise, 0)
        .crafted(
            Faction::Marquise,
            vec![(
                Piece::Card(Card::DeckCard(
                    Some(CardSuit::Bird),
                    Some(CardName::BoatBuilders),
                )),
                1,
            )],
        )
        .build();

    assert_ne!(state, expected);
    assert_eq!(expected, state.act(&action));
}

#[rstest]
fn move_many_warriors() {
    let state = State::builder()
        .map(Map::autumn())
        .faction(Faction::Marquise)
        .clearing(
            10,
            vec![
                (Piece::Marker(Marker::Ruins), 1),
                (Piece::Warrior(Faction::Marquise), 4),
            ],
        )
        .build();
    let action = Action::parse(Faction::Marquise, "3w10->11").unwrap();
    let expected = State::builder()
        .map(Map::autumn())
        .faction(Faction::Marquise)
        .clearing(
            10,
            vec![
                (Piece::Marker(Marker::Ruins), 1),
                (Piece::Warrior(Faction::Marquise), 1),
            ],
        )
        .clearing(
            11,
            vec![
                (Piece::Marker(Marker::Ruins), 1),
                (Piece::Warrior(Faction::Marquise), 3),
            ],
        )
        .build();

    assert_ne!(state, expected);
    assert_eq!(expected, state.act(&action));
}

#[rstest]
fn build() {
    let state = State::builder()
        .map(Map::autumn())
        .faction(Faction::Marquise)
        .clearing(10, vec![(Piece::Warrior(Faction::Marquise), 1)])
        .build();
    let action = Action::parse(Faction::Marquise, "br->10").unwrap();
    let expected = State::builder()
        .map(Map::autumn())
        .faction(Faction::Marquise)
        .supply(Faction::Marquise, Piece::Building(Building::Recruiter), 5)
        .clearing(
            10,
            vec![
                (Piece::Warrior(Faction::Marquise), 1),
                (Piece::Building(Building::Recruiter), 1),
            ],
        )
        .build();

    assert_ne!(state, expected);
    assert_eq!(expected, state.act(&action));
}

#[rstest]
fn battle() {
    let state = State::builder()
        .map(Map::autumn())
        .faction(Faction::Marquise)
        .clearing(
            10,
            vec![
                (Piece::Warrior(Faction::Marquise), 1),
                (Piece::Warrior(Faction::Eyrie), 1),
            ],
        )
        .build();
    let action = Action::parse(Faction::Marquise, "Ex10").unwrap();

    assert_eq!(state, state.act(&action));
}

#[rstest]
fn battle_aftermath() {
    let state = State::builder()
        .map(Map::autumn())
        .faction(Faction::Marquise)
        .supply(Faction::Marquise, Piece::Warrior(Faction::Marquise), 23)
        .faction(Faction::Eyrie)
        .supply(Faction::Eyrie, Piece::Building(Building::Roost), 6)
        .supply(Faction::Eyrie, Piece::Warrior(Faction::Eyrie), 19)
        .clearing(
            10,
            vec![
                (Piece::Warrior(Faction::Marquise), 2),
                (Piece::Warrior(Faction::Eyrie), 1),
                (Piece::Building(Building::Roost), 1),
            ],
        )
        .build();
    let action = Action::parse(Faction::Marquise, "(w,Ew,Eb)10->").unwrap();
    let expected = State::builder()
        .map(Map::autumn())
        .faction(Faction::Marquise)
        .supply(Faction::Marquise, Piece::Warrior(Faction::Marquise), 24)
        .faction(Faction::Eyrie)
        .clearing(10, vec![(Piece::Warrior(Faction::Marquise), 1)])
        .build();

    assert_eq!(expected, state.act(&action));
}

#[rstest]
fn score_points() {
    let state = State::builder()
        .map(Map::autumn())
        .faction(Faction::Marquise)
        .points(Faction::Marquise, 5)
        .build();
    let action = Action::parse(Faction::Marquise, "+1").unwrap();
    let expected = State::builder()
        .map(Map::autumn())
        .faction(Faction::Marquise)
        .points(Faction::Marquise, 6)
        .build();

    assert_eq!(expected, state.act(&action));
}

#[rstest]
fn lose_points() {
    let state = State::builder()
        .map(Map::autumn())
        .faction(Faction::Eyrie)
        .points(Faction::Eyrie, 5)
        .build();
    let action = Action::parse(Faction::Eyrie, "-4").unwrap();
    let expected = State::builder()
        .map(Map::autumn())
        .faction(Faction::Eyrie)
        .points(Faction::Eyrie, 1)
        .build();

    assert_eq!(expected, state.act(&action));
}

#[rstest]
fn lose_too_many_points() {
    let state = State::builder()
        .map(Map::autumn())
        .faction(Faction::Eyrie)
        .points(Faction::Eyrie, 2)
        .build();
    let action = Action::parse(Faction::Eyrie, "-4").unwrap();
    let expected = State::builder()
        .map(Map::autumn())
        .faction(Faction::Eyrie)
        .points(Faction::Eyrie, 0)
        .build();

    assert_eq!(expected, state.act(&action));
}

#[rstest]
fn activate_dominance() {
    let state = State::builder()
        .map(Map::autumn())
        .faction(Faction::Eyrie)
        .points(Faction::Eyrie, 11)
        .hand(Faction::Eyrie, 1)
        .build();
    let action = Action::parse(Faction::Eyrie, &format!("#rdom")).expect("couldn't parse action");
    let expected = State::builder()
        .map(Map::autumn())
        .faction(Faction::Eyrie)
        .dominance(Faction::Eyrie, CardSuit::Rabbit)
        .hand(Faction::Eyrie, 0)
        .build();

    assert_eq!(expected, state.act(&action));
}

#[rstest]
fn coalition() {
    let state = State::builder()
        .map(Map::autumn())
        .faction(Faction::Vagabond(1))
        .points(Faction::Vagabond(1), 11)
        .hand(Faction::Vagabond(1), 1)
        .build();
    let action =
        Action::parse(Faction::Vagabond(1), &format!("#rdomM")).expect("couldn't parse action");
    let expected = State::builder()
        .map(Map::autumn())
        .faction(Faction::Vagabond(1))
        .coalition(Faction::Vagabond(1), Faction::Marquise)
        .hand(Faction::Vagabond(1), 0)
        .build();

    assert_eq!(expected, state.act(&action));
}

#[rstest]
fn vagabond_choosing_character_gets_starting_items() {
    let state = State::builder().faction(Faction::Vagabond(1)).build();

    let action =
        Action::parse(Faction::Vagabond(1), "#adventurer->$").expect("couldn't parse action");

    let expected = State::builder()
        .faction(Faction::Vagabond(1))
        .vagabond_character(1, VagabondCharacter::Adventurer)
        .satchel_item(Faction::Vagabond(1), 1, Item::Boot, ItemState::Refreshed)
        .satchel_item(Faction::Vagabond(1), 1, Item::Torch, ItemState::Refreshed)
        .satchel_item(Faction::Vagabond(1), 1, Item::Hammer, ItemState::Refreshed)
        .build();

    assert_eq!(expected, state.act(&action));
}

#[rstest]
fn vagabond_refresh_item() {
    let state = State::builder()
        .map(Map::autumn())
        .faction(Faction::Vagabond(1))
        .satchel_item(Faction::Vagabond(1), 1, Item::Boot, ItemState::Exhausted)
        .build();
    let action =
        Action::parse(Faction::Vagabond(1), &format!("%mr")).expect("couldn't parse action");
    let expected = State::builder()
        .map(Map::autumn())
        .faction(Faction::Vagabond(1))
        .satchel_item(Faction::Vagabond(1), 1, Item::Boot, ItemState::Refreshed)
        .build();

    assert_eq!(expected, state.act(&action));
}

#[rstest]
fn vagabond_exhaust_item() {
    let state = State::builder()
        .map(Map::autumn())
        .faction(Faction::Vagabond(1))
        .satchel_item(Faction::Vagabond(1), 1, Item::Boot, ItemState::Refreshed)
        .build();
    let action =
        Action::parse(Faction::Vagabond(1), &format!("%me")).expect("couldn't parse action");
    let expected = State::builder()
        .map(Map::autumn())
        .faction(Faction::Vagabond(1))
        .satchel_item(Faction::Vagabond(1), 1, Item::Boot, ItemState::Exhausted)
        .build();

    assert_eq!(expected, state.act(&action));
}

#[rstest]
fn non_vagabond_exhaust_item() {
    let state = State::builder()
        .map(Map::autumn())
        .faction(Faction::Marquise)
        .crafted(
            Faction::Marquise,
            vec![(Piece::Item(Item::Boot, Some(ItemState::Refreshed)), 1)],
        )
        .build();
    let action = Action::parse(Faction::Marquise, &format!("%me")).expect("couldn't parse action");
    let expected = State::builder()
        .map(Map::autumn())
        .faction(Faction::Marquise)
        .crafted(
            Faction::Marquise,
            vec![(Piece::Item(Item::Boot, Some(ItemState::Exhausted)), 1)],
        )
        .build();

    assert_eq!(expected, state.act(&action));
}

#[rstest]
fn vagabond_take_item_in_specific_state() {
    let state = State::builder()
        .map(Map::autumn())
        .faction(Faction::Marquise)
        .crafted(
            Faction::Marquise,
            vec![
                (Piece::Item(Item::Bag, Some(ItemState::Exhausted)), 1),
                (Piece::Item(Item::Bag, Some(ItemState::Refreshed)), 1),
            ],
        )
        .faction(Faction::Vagabond(1))
        .build();
    let action = Action::parse(Faction::Vagabond(1), "%ebM$i->$t").expect("couldn't parse action");

    let expected = State::builder()
        .map(Map::autumn())
        .faction(Faction::Marquise)
        .crafted(
            Faction::Marquise,
            vec![(Piece::Item(Item::Bag, Some(ItemState::Refreshed)), 1)],
        )
        .faction(Faction::Vagabond(1))
        .track_item(Faction::Vagabond(1), 1, Item::Bag, ItemState::Exhausted)
        .build();

    assert_eq!(expected, state.act(&action));
}

#[rstest]
fn vagabond_take_item_and_refresh() {
    let state = State::builder()
        .map(Map::autumn())
        .faction(Faction::Marquise)
        .crafted(
            Faction::Marquise,
            vec![(Piece::Item(Item::Bag, Some(ItemState::Exhausted)), 1)],
        )
        .faction(Faction::Vagabond(1))
        .build();
    let action = Action::parse(Faction::Vagabond(1), "%bM$i-r>$t").expect("couldn't parse action");

    let expected = State::builder()
        .map(Map::autumn())
        .faction(Faction::Marquise)
        .crafted(Faction::Marquise, vec![])
        .faction(Faction::Vagabond(1))
        .track_item(Faction::Vagabond(1), 1, Item::Bag, ItemState::Refreshed)
        .build();

    assert_eq!(expected, state.act(&action));
}

#[rstest]
fn vagabond_move_track_item_while_exhausting() {
    let state = State::builder()
        .faction(Faction::Vagabond(1))
        .track_item(Faction::Vagabond(1), 1, Item::Bag, ItemState::Refreshed)
        .build();

    let action = Action::parse(Faction::Vagabond(1), "%b$t-e>$s").expect("couldn't parse action");

    let expected = State::builder()
        .faction(Faction::Vagabond(1))
        .satchel_item(Faction::Vagabond(1), 1, Item::Bag, ItemState::Exhausted)
        .build();

    assert_eq!(expected, state.act(&action));
}

#[rstest]
fn vagabond_take_item_from_ruin() {
    let state = State::builder()
        .map(Map::autumn())
        .faction(Faction::Vagabond(1))
        .build();

    let action = Action::parse(Faction::Vagabond(1), "%b12->$s").expect("couldn't parse action");

    let expected = State::builder()
        .map(Map::autumn())
        .faction(Faction::Vagabond(1))
        .satchel_item(Faction::Vagabond(1), 1, Item::Bag, ItemState::Refreshed)
        .clearing(12, vec![(Piece::Marker(Marker::Ruins), 1)])
        .build();

    assert_eq!(expected, state.act(&action));
}

#[rstest]
fn flip_plot() {
    let state = State::builder()
        .map(Map::autumn())
        .faction(Faction::Corvid)
        .clearing(10, vec![(Piece::Token(Token::Plot { plot: None }), 1)])
        .build();

    let action = Action::parse(Faction::Corvid, "t10^tb").expect("couldn't parse action");

    let expected = State::builder()
        .map(Map::autumn())
        .faction(Faction::Corvid)
        .clearing(
            10,
            vec![(
                Piece::Token(Token::Plot {
                    plot: Some(Plot::Bomb),
                }),
                1,
            )],
        )
        .build();

    assert_eq!(expected, state.act(&action));
}

#[rstest]
fn trick_facedown() {
    let state = State::builder()
        .map(Map::autumn())
        .faction(Faction::Corvid)
        .clearing(10, vec![(Piece::Token(Token::Plot { plot: None }), 1)])
        .clearing(5, vec![(Piece::Token(Token::Plot { plot: None }), 1)])
        .build();

    let action = Action::parse(Faction::Corvid, "t5<->t10").expect("couldn't parse action");

    let expected = State::builder()
        .map(Map::autumn())
        .faction(Faction::Corvid)
        .clearing(10, vec![(Piece::Token(Token::Plot { plot: None }), 1)])
        .clearing(5, vec![(Piece::Token(Token::Plot { plot: None }), 1)])
        .build();

    assert_eq!(expected, state.act(&action));
}

#[rstest]
fn trick_faceup() {
    let state = State::builder()
        .map(Map::autumn())
        .faction(Faction::Corvid)
        .clearing(
            10,
            vec![(
                Piece::Token(Token::Plot {
                    plot: Some(Plot::Bomb),
                }),
                1,
            )],
        )
        .clearing(
            5,
            vec![(
                Piece::Token(Token::Plot {
                    plot: Some(Plot::Snare),
                }),
                1,
            )],
        )
        .build();

    let action = Action::parse(Faction::Corvid, "ts5<->tb10").expect("couldn't parse action");

    let expected = State::builder()
        .map(Map::autumn())
        .faction(Faction::Corvid)
        .clearing(
            10,
            vec![(
                Piece::Token(Token::Plot {
                    plot: Some(Plot::Snare),
                }),
                1,
            )],
        )
        .clearing(
            5,
            vec![(
                Piece::Token(Token::Plot {
                    plot: Some(Plot::Bomb),
                }),
                1,
            )],
        )
        .build();

    assert_eq!(expected, state.act(&action));
}

#[rstest]
fn everything() {
    let action = Action::parse(Faction::Cult, "_$->").expect("couldn't parse action");

    let state = State::builder()
        .faction(Faction::Cult)
        .lost_souls(vec![
            Card::DeckCard(Some(CardSuit::Fox), Some(CardName::Ambush)),
            Card::DeckCard(Some(CardSuit::Mouse), Some(CardName::Dominance)),
        ])
        .build();

    let expected = State::builder()
        .faction(Faction::Cult)
        .discard(vec![
            Card::DeckCard(Some(CardSuit::Fox), Some(CardName::Ambush)),
            Card::DeckCard(Some(CardSuit::Mouse), Some(CardName::Dominance)),
        ])
        .build();

    assert_eq!(expected, state.act(&action));
}

#[rstest]
fn hundreds_loot_item() {
    let action = Action::parse(Faction::Hundreds, "%tL$i->$h").expect("couldn't parse action");

    let state = State::builder()
        .faction(Faction::Hundreds)
        .faction(Faction::Cult)
        .crafted(
            Faction::Cult,
            vec![(Piece::Item(Item::Tea, Some(ItemState::Refreshed)), 1)],
        )
        .build();
    let expected = State::builder()
        .faction(Faction::Hundreds)
        .hundreds_hoard(vec![(
            Piece::Item(Item::Tea, Some(ItemState::Refreshed)),
            1,
        )])
        .faction(Faction::Cult)
        .build();

    assert_eq!(expected, state.act(&action));
}

#[rstest]
fn hundreds_choosing_mood_removes_previous_mood() {
    let action = Action::parse(Faction::Hundreds, "#wrath->$").expect("couldn't parse action");

    let state = State::builder()
        .faction(Faction::Hundreds)
        .hundreds_mood(HundredsMood::Bitter)
        .supply(
            Faction::Hundreds,
            Piece::Card(Card::HundredsMood(HundredsMood::Bitter)),
            0,
        )
        .build();
    let expected = State::builder()
        .faction(Faction::Hundreds)
        .hundreds_mood(HundredsMood::Wrathful)
        .supply(
            Faction::Hundreds,
            Piece::Card(Card::HundredsMood(HundredsMood::Wrathful)),
            0,
        )
        .build();

    use storyteller_domain::piece::Location;
    println!(
        "{:?}",
        state.act(&action).pieces_by_location[&Location::Supply(Some(Faction::Hundreds))]
    );
    assert_eq!(expected, state.act(&action));
}
