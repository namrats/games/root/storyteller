use rstest::rstest;
use storyteller_domain::{
    action::{Action, Pieces},
    piece::{Building, Card, CardSuit, DuchyMinister, Faction, Location, Piece, Token},
};

#[rstest]
fn setup_tunnel_and_warriors() {
    let action = Action::parse(Faction::Duchy, "(2w,t)->5").expect("couldn't parse action");

    let expected = Action::Place {
        pieces: vec![
            (Piece::Warrior(Faction::Duchy), 2),
            (Piece::Token(Token::Tunnel), 1),
        ],
        at: vec![Location::Clearing(5)],
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn recruit_in_burrow() {
    let action = Action::parse(Faction::Duchy, "2w->0").expect("couldn't parse action");

    let expected = Action::Place {
        pieces: vec![(Piece::Warrior(Faction::Duchy), 2)],
        at: vec![Location::Burrow],
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn move_out_of_burrow() {
    let action = Action::parse(Faction::Duchy, "4w0->12").expect("couldn't parse action");

    let expected = Action::Move {
        pieces: Pieces::Specific(vec![(Piece::Warrior(Faction::Duchy), 4)]),
        from: Location::Burrow,
        to: Location::Clearing(12),
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn move_into_burrow() {
    let action = Action::parse(Faction::Duchy, "4w6->0").expect("couldn't parse action");

    let expected = Action::Move {
        pieces: Pieces::Specific(vec![(Piece::Warrior(Faction::Duchy), 4)]),
        from: Location::Clearing(6),
        to: Location::Burrow,
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn reveal_cards() {
    let action = Action::parse(Faction::Duchy, "(#f,#r)^").expect("couldn't parse action");

    let expected = Action::Reveal {
        faction: Faction::Duchy,
        pieces: Some(vec![
            (Piece::Card(Card::DeckCard(Some(CardSuit::Fox), None)), 1),
            (Piece::Card(Card::DeckCard(Some(CardSuit::Rabbit), None)), 1),
        ]),
        to: None,
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
#[case("c", Building::Citadel)]
#[case("m", Building::Market)]
fn build(#[case] b: &str, #[case] building: Building) {
    let action = Action::parse(Faction::Duchy, &format!("b{b}->$")).expect("couldn't parse action");

    let expected = Action::Place {
        pieces: vec![(Piece::Building(building), 1)],
        at: vec![Location::DuchySwayedMinisters],
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
#[case("foremole", DuchyMinister::Foremole)]
#[case("captain", DuchyMinister::Captain)]
#[case("marshal", DuchyMinister::Marshal)]
#[case("brig", DuchyMinister::Brigadier)]
#[case("banker", DuchyMinister::Banker)]
#[case("mayor", DuchyMinister::Mayor)]
#[case("mud", DuchyMinister::DuchessOfMud)]
#[case("dirt", DuchyMinister::BaronOfDirt)]
#[case("stone", DuchyMinister::EarlOfStone)]
fn sway_minister(#[case] m: &str, #[case] minister: DuchyMinister) {
    let action = Action::parse(Faction::Duchy, &format!("#{m}->$")).expect("couldn't parse action");

    let expected = Action::Place {
        pieces: vec![(Piece::Card(Card::DuchyMinister(minister)), 1)],
        at: vec![Location::DuchySwayedMinisters],
        commentary: None,
    };

    assert_eq!(expected, action);
}
