use rstest::rstest;
use storyteller_domain::{
    action::{Action, Pieces},
    piece::{
        Building, Card, CardName, CardSuit, EyrieDecreeColumn, EyrieLeader, Faction, Location,
        Piece,
    },
};

#[rstest]
#[case("#builder", EyrieLeader::Builder)]
#[case("#charismatic", EyrieLeader::Charismatic)]
#[case("#commander", EyrieLeader::Commander)]
#[case("#despot", EyrieLeader::Despot)]
fn choose_leader(#[case] card: &str, #[case] leader: EyrieLeader) {
    let action =
        Action::parse(Faction::Eyrie, &format!("{card}->$")).expect("couldn't parse action");

    let expected = Action::Place {
        pieces: vec![(Piece::Card(Card::EyrieLeader(leader)), 1)],
        at: vec![Location::EyrieLeader],
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
#[case("r", EyrieDecreeColumn::Recruit)]
#[case("m", EyrieDecreeColumn::Move)]
#[case("x", EyrieDecreeColumn::Battle)]
#[case("b", EyrieDecreeColumn::Build)]
fn add_to_decree(#[case] col: &str, #[case] column: EyrieDecreeColumn) {
    let card = Card::DeckCard(Some(CardSuit::Bird), Some(CardName::BoatBuilders));

    let action =
        Action::parse(Faction::Eyrie, &format!("#bboatE->${col}")).expect("couldn't parse action");

    let expected = Action::Move {
        pieces: Pieces::Specific(vec![(Piece::Card(card), 1)]),
        from: Location::Hand(Faction::Eyrie),
        to: Location::EyrieDecree(column),
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn build(#[values(1, 2, 10, 12)] c: usize) {
    let action = Action::parse(Faction::Eyrie, &format!("b->{c}")).expect("couldn't parse action");

    let expected = Action::Place {
        pieces: vec![(Piece::Building(Building::Roost), 1)],
        at: vec![Location::Clearing(c)],
        commentary: None,
    };

    assert_eq!(expected, action);
}
