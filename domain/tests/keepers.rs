use rstest::rstest;
use storyteller_domain::{
    action::{Action, Pieces},
    piece::{
        Building, Card, CardName, CardSuit, Faction, KeepersCard, KeepersRetinueColumn, Location,
        Piece, Relic, Token,
    },
};

#[rstest]
fn setup_relics() {
    let action = Action::parse(Faction::Keepers, "(2tf,tt)->1_3_5").expect("couldn't parse action");

    let expected = Action::Place {
        pieces: vec![
            (
                Piece::Token(Token::Relic {
                    relic: Relic::Figure,
                    value: None,
                }),
                2,
            ),
            (
                Piece::Token(Token::Relic {
                    relic: Relic::Tablet,
                    value: None,
                }),
                1,
            ),
        ],
        at: vec![Location::Forest(vec![1, 3, 5])],
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
#[case("m", KeepersRetinueColumn::Move)]
#[case("x", KeepersRetinueColumn::BattleDelve)]
#[case("r", KeepersRetinueColumn::MoveRecover)]
fn setup_faithful_retainers(#[case] c: &str, #[case] column: KeepersRetinueColumn) {
    let action =
        Action::parse(Faction::Keepers, &format!("#faith->${c}")).expect("couldn't parse action");

    let expected = Action::Place {
        pieces: vec![(
            Piece::Card(Card::KeepersCard(KeepersCard::FaithfulRetainer)),
            1,
        )],
        at: vec![Location::KeepersRetinue(column)],
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn encamp() {
    let action = Action::parse(Faction::Keepers, "btj->2").expect("couldn't parse action");

    let expected = Action::Place {
        pieces: vec![(
            Piece::Building(Building::Waystation {
                obverse: Relic::Tablet,
                reverse: Relic::Jewelry,
            }),
            1,
        )],
        at: vec![Location::Clearing(2)],
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn delve_relic() {
    let action = Action::parse(Faction::Keepers, "tt1_3_5->3").expect("couldn't parse action");

    let expected = Action::Move {
        pieces: Pieces::Specific(vec![(
            Piece::Token(Token::Relic {
                relic: Relic::Tablet,
                value: None,
            }),
            1,
        )]),
        from: Location::Forest(vec![1, 3, 5]),
        to: Location::Clearing(3),
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn flip_relic() {
    let action = Action::parse(Faction::Keepers, "tt3^t2t").expect("couldn't parse action");

    let expected = Action::Flip {
        faction: Faction::Keepers,
        at: Location::Clearing(3),
        obverse: Piece::Token(Token::Relic {
            relic: Relic::Tablet,
            value: None,
        }),
        reverse: Piece::Token(Token::Relic {
            relic: Relic::Tablet,
            value: Some(2),
        }),
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn move_with_relic() {
    let action = Action::parse(Faction::Keepers, "(3w,t2j)3->10").expect("couldn't parse action");

    let expected = Action::Move {
        pieces: Pieces::Specific(vec![
            (Piece::Warrior(Faction::Keepers), 3),
            (
                Piece::Token(Token::Relic {
                    relic: Relic::Jewelry,
                    value: Some(2),
                }),
                1,
            ),
        ]),
        from: Location::Clearing(3),
        to: Location::Clearing(10),
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn recover_relic() {
    let action = Action::parse(Faction::Keepers, "t2j3->").expect("couldn't parse action");

    let expected = Action::Remove {
        pieces: Pieces::Specific(vec![(
            Piece::Token(Token::Relic {
                relic: Relic::Jewelry,
                value: Some(2),
            }),
            1,
        )]),
        from: vec![Location::Clearing(3)],
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn enemy_removes_relic() {
    let action = Action::parse(Faction::Corvid, "Kt2j3->1_3_5").expect("couldn't parse action");

    let expected = Action::Move {
        pieces: Pieces::Specific(vec![(
            Piece::Token(Token::Relic {
                relic: Relic::Jewelry,
                value: Some(2),
            }),
            1,
        )]),
        from: Location::Clearing(3),
        to: Location::Forest(vec![1, 3, 5]),
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn add_card_to_retinue() {
    let action = Action::parse(Faction::Keepers, "#bboatK->$x").expect("couldn't parse action");

    let expected = Action::Move {
        pieces: Pieces::Specific(vec![(
            Piece::Card(Card::DeckCard(
                Some(CardSuit::Bird),
                Some(CardName::BoatBuilders),
            )),
            1,
        )]),
        from: Location::Hand(Faction::Keepers),
        to: Location::KeepersRetinue(KeepersRetinueColumn::BattleDelve),
        commentary: None,
    };

    assert_eq!(expected, action);
}
