use rstest::rstest;
use storyteller_domain::{
    action::{Action, Pieces},
    piece::{
        Card, CardSuit, ClearingNumber, Faction, Item, ItemState, Location, Marker, Piece, Quest,
        Relationship, Suit, VagabondCharacter,
    },
};

#[rstest]
#[case("#adventurer", VagabondCharacter::Adventurer)]
#[case("#arbiter", VagabondCharacter::Arbiter)]
#[case("#harrier", VagabondCharacter::Harrier)]
#[case("#ranger", VagabondCharacter::Ranger)]
#[case("#ronin", VagabondCharacter::Ronin)]
#[case("#scoundrel", VagabondCharacter::Scoundrel)]
#[case("#thief", VagabondCharacter::Thief)]
#[case("#tinker", VagabondCharacter::Tinker)]
#[case("#vagrant", VagabondCharacter::Vagrant)]
fn choose_character(
    #[case] card: &str,
    #[case] character: VagabondCharacter,
    #[values(1, 2)] i: usize,
) {
    let action =
        Action::parse(Faction::Vagabond(i), &format!("{card}->$")).expect("couldn't parse action");

    let expected = Action::Place {
        pieces: vec![(Piece::Card(Card::VagabondCharacter(character)), 1)],
        at: vec![Location::VagabondCharacter(i)],
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
#[case(vec![3, 6, 11])]
#[case(vec![1, 2, 5, 10])]
#[case(vec![2, 6, 10, 11, 12])]
fn setup_pawn(#[case] forest: Vec<ClearingNumber>, #[values(1, 2)] i: usize) {
    let f = forest
        .iter()
        .map(ToString::to_string)
        .collect::<Vec<String>>()
        .join("_");
    let action =
        Action::parse(Faction::Vagabond(i), &format!("p->{f}")).expect("couldn't parse action");

    let expected = Action::Place {
        pieces: vec![(Piece::Pawn(Faction::Vagabond(i)), 1)],
        at: vec![Location::Forest(forest)],
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
#[case("b", Item::Bag, "t", Location::VagabondTrack(1))]
#[case("m", Item::Boot, "s", Location::VagabondSatchel(1))]
#[case("c", Item::Coins, "t", Location::VagabondTrack(1))]
#[case("x", Item::Crossbow, "s", Location::VagabondSatchel(1))]
#[case("h", Item::Hammer, "s", Location::VagabondSatchel(1))]
#[case("s", Item::Sword, "s", Location::VagabondSatchel(1))]
#[case("t", Item::Tea, "t", Location::VagabondTrack(1))]
fn place_item(
    #[case] i: &str,
    #[case] expected_item: Item,
    #[case] l: &str,
    #[case] location: Location,
) {
    let action =
        Action::parse(Faction::Vagabond(1), &format!("%{i}->${l}")).expect("couldn't parse action");

    let expected = Action::Place {
        pieces: vec![(Piece::Item(expected_item, None), 1)],
        at: vec![location],
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn slip(
    #[values(("3_6_11", Location::Forest(vec![3, 6, 11])), ("3", Location::Clearing(3)))] from: (
        &str,
        Location,
    ),
    #[values(("3_7_11_12", Location::Forest(vec![3, 7, 11, 12])), ("7", Location::Clearing(7)))] to: (&str, Location),
) {
    let action = Action::parse(Faction::Vagabond(1), &format!("p{}->{}", from.0, to.0))
        .expect("couldn't parse action");

    let expected = Action::Move {
        pieces: Pieces::Specific(vec![(Piece::Pawn(Faction::Vagabond(1)), 1)]),
        from: from.1,
        to: to.1,
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
#[case("f", Item::Torch)]
#[case("m", Item::Boot)]
fn exhaust_item(#[case] i: &str, #[case] item: Item) {
    let action =
        Action::parse(Faction::Vagabond(1), &format!("%{i}e")).expect("couldn't parse action");

    let expected = Action::Exhaust {
        faction: Faction::Vagabond(1),
        pieces: Pieces::Specific(vec![(Piece::Item(item, None), 1)]),
        in_: None,
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
#[case("(2%h,%f)", vec![(2, Item::Hammer), (1, Item::Torch)])]
#[case("2%m", vec![(2, Item::Boot)])]
fn exhaust_many_items(#[case] is: &str, #[case] items: Vec<(usize, Item)>) {
    let action =
        Action::parse(Faction::Vagabond(1), &format!("{is}e")).expect("couldn't parse action");

    let expected = Action::Exhaust {
        faction: Faction::Vagabond(1),
        pieces: Pieces::Specific(
            items
                .into_iter()
                .map(|(n, item)| (Piece::Item(item, None), n))
                .collect(),
        ),
        in_: None,
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
#[case("(2%h,%f)", vec![(2, Item::Hammer), (1, Item::Torch)], "$d", Location::VagabondDamaged(1), 1)]
#[case("(2%h,%f)", vec![(2, Item::Hammer), (1, Item::Torch)], "$s", Location::VagabondSatchel(1), 1)]
#[case("2%m", vec![(2, Item::Boot)], "$d", Location::VagabondDamaged(1), 1)]
#[case("2%m", vec![(2, Item::Boot)], "$s", Location::VagabondSatchel(1), 1)]
#[case("(2%h,%f)", vec![(2, Item::Hammer), (1, Item::Torch)], "$d", Location::VagabondDamaged(2), 2)]
#[case("(2%h,%f)", vec![(2, Item::Hammer), (1, Item::Torch)], "$s", Location::VagabondSatchel(2), 2)]
#[case("2%m", vec![(2, Item::Boot)], "$d", Location::VagabondDamaged(2), 2)]
#[case("2%m", vec![(2, Item::Boot)], "$s", Location::VagabondSatchel(2), 2)]
fn exhaust_many_items_in_location(
    #[case] is: &str,
    #[case] items: Vec<(usize, Item)>,
    #[case] l: &str,
    #[case] location: Location,
    #[case] i: usize,
) {
    let action =
        Action::parse(Faction::Vagabond(i), &format!("{is}{l}e")).expect("couldn't parse action");

    let expected = Action::Exhaust {
        faction: Faction::Vagabond(i),
        pieces: Pieces::Specific(
            items
                .into_iter()
                .map(|(n, item)| (Piece::Item(item, None), n))
                .collect(),
        ),
        in_: Some(location),
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
#[case("f", Item::Torch)]
#[case("m", Item::Boot)]
fn refresh_item(#[case] i: &str, #[case] item: Item) {
    let action =
        Action::parse(Faction::Vagabond(1), &format!("%{i}r")).expect("couldn't parse action");

    let expected = Action::Refresh {
        faction: Faction::Vagabond(1),
        pieces: Pieces::Specific(vec![(Piece::Item(item, None), 1)]),
        in_: None,
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
#[case("(2%h,%f)", vec![(2, Item::Hammer), (1, Item::Torch)])]
#[case("2%m", vec![(2, Item::Boot)])]
fn refresh_many_items(#[case] is: &str, #[case] items: Vec<(usize, Item)>) {
    let action =
        Action::parse(Faction::Vagabond(1), &format!("{is}r")).expect("couldn't parse action");

    let expected = Action::Refresh {
        faction: Faction::Vagabond(1),
        pieces: Pieces::Specific(
            items
                .into_iter()
                .map(|(n, item)| (Piece::Item(item, None), n))
                .collect(),
        ),
        in_: None,
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
#[case("(2%h,%f)", vec![(2, Item::Hammer), (1, Item::Torch)], "$d", Location::VagabondDamaged(1))]
#[case("(2%h,%f)", vec![(2, Item::Hammer), (1, Item::Torch)], "$s", Location::VagabondSatchel(1))]
#[case("2%m", vec![(2, Item::Boot)], "$d", Location::VagabondDamaged(1))]
#[case("2%m", vec![(2, Item::Boot)], "$s", Location::VagabondSatchel(1))]
fn refresh_many_items_in_location(
    #[case] is: &str,
    #[case] items: Vec<(usize, Item)>,
    #[case] l: &str,
    #[case] location: Location,
) {
    let action =
        Action::parse(Faction::Vagabond(1), &format!("{is}{l}r")).expect("couldn't parse action");

    let expected = Action::Refresh {
        faction: Faction::Vagabond(1),
        pieces: Pieces::Specific(
            items
                .into_iter()
                .map(|(n, item)| (Piece::Item(item, None), n))
                .collect(),
        ),
        in_: Some(location),
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn refresh_all_items_in_satchel() {
    let action =
        Action::parse(Faction::Vagabond(1), &format!("_$sr")).expect("couldn't parse action");

    let expected = Action::Refresh {
        faction: Faction::Vagabond(1),
        pieces: Pieces::All,
        in_: Some(Location::VagabondSatchel(1)),
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
#[case("(2%h,%f)", vec![(2, Piece::Item(Item::Hammer, None)), (1, Piece::Item(Item::Torch, None))])]
#[case("2%m", vec![(2, Piece::Item(Item::Boot, None))])]
#[case("%em", vec![(1, Piece::Item(Item::Boot, Some(ItemState::Exhausted)))])]
#[case("%rm", vec![(1, Piece::Item(Item::Boot, Some(ItemState::Refreshed)))])]
fn damage_many_items(#[case] is: &str, #[case] items: Vec<(usize, Piece)>) {
    let action =
        Action::parse(Faction::Vagabond(1), &format!("{is}$s->$d")).expect("couldn't parse action");

    let expected = Action::Move {
        pieces: Pieces::Specific(items.into_iter().map(|(n, item)| (item, n)).collect()),
        from: Location::VagabondSatchel(1),
        to: Location::VagabondDamaged(1),
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
#[case("(2%h,%f)", vec![(2, Piece::Item(Item::Hammer, None)), (1, Piece::Item(Item::Torch, None))])]
#[case("2%m", vec![(2, Piece::Item(Item::Boot, None))])]
#[case("%em", vec![(1, Piece::Item(Item::Boot, Some(ItemState::Exhausted)))])]
#[case("%rm", vec![(1, Piece::Item(Item::Boot, Some(ItemState::Refreshed)))])]
fn damage_many_items_on_defense(
    #[case] is: &str,
    #[case] items: Vec<(usize, Piece)>,
    #[values(None, Some(1), Some(2))] i: Option<usize>,
) {
    let action = Action::parse(
        Faction::Marquise,
        &format!(
            "{is}V{0}$s->V{0}$d",
            i.map(|i| i.to_string()).unwrap_or("".to_string())
        ),
    )
    .expect("couldn't parse action");

    let expected = Action::Move {
        pieces: Pieces::Specific(items.into_iter().map(|(n, item)| (item, n)).collect()),
        from: Location::VagabondSatchel(i.unwrap_or(1)),
        to: Location::VagabondDamaged(i.unwrap_or(1)),
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
#[case("(2%h,%f)", vec![(2, Item::Hammer), (1, Item::Torch)])]
#[case("2%m", vec![(2, Item::Boot)])]
fn repair_many_items(#[case] is: &str, #[case] items: Vec<(usize, Item)>) {
    let action =
        Action::parse(Faction::Vagabond(1), &format!("{is}$d->$s")).expect("couldn't parse action");

    let expected = Action::Move {
        pieces: Pieces::Specific(
            items
                .into_iter()
                .map(|(n, item)| (Piece::Item(item, None), n))
                .collect(),
        ),
        from: Location::VagabondDamaged(1),
        to: Location::VagabondSatchel(1),
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
#[case("b", Item::Bag, "t", Location::VagabondTrack(1))]
#[case("m", Item::Boot, "s", Location::VagabondSatchel(1))]
#[case("c", Item::Coins, "t", Location::VagabondTrack(1))]
#[case("x", Item::Crossbow, "s", Location::VagabondSatchel(1))]
#[case("h", Item::Hammer, "s", Location::VagabondSatchel(1))]
#[case("s", Item::Sword, "s", Location::VagabondSatchel(1))]
#[case("t", Item::Tea, "t", Location::VagabondTrack(1))]
fn gain_ruin_item(
    #[case] i: &str,
    #[case] expected_item: Item,
    #[case] l: &str,
    #[case] location: Location,
) {
    let action = Action::parse(Faction::Vagabond(1), &format!("%{i}10->${l}"))
        .expect("couldn't parse action");

    let expected = Action::Move {
        pieces: Pieces::Specific(vec![(Piece::Item(expected_item, None), 1)]),
        from: Location::Clearing(10),
        to: location,
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn remove_ruins() {
    let action =
        Action::parse(Faction::Vagabond(1), &format!("mr10->*")).expect("couldn't parse action");

    let expected = Action::RemoveFromGame {
        pieces: vec![(Piece::Marker(Marker::Ruins), 1)],
        from: Some(Location::Clearing(10)),
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn aid_card(#[values(None, Some(1), Some(2))] i: Option<usize>) {
    let action = Action::parse(
        Faction::Vagabond(1),
        &format!(
            "#V{}->M",
            i.map(|i| i.to_string()).unwrap_or("".to_string())
        ),
    )
    .expect("couldn't parse action");

    let expected = Action::Move {
        pieces: Pieces::Specific(vec![(Piece::Card(Card::DeckCard(None, None)), 1)]),
        from: Location::Hand(Faction::Vagabond(i.unwrap_or(1))),
        to: Location::Hand(Faction::Marquise),
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
#[case("b", Item::Bag, "t", Location::VagabondTrack(1))]
#[case("m", Item::Boot, "s", Location::VagabondSatchel(1))]
#[case("c", Item::Coins, "t", Location::VagabondTrack(1))]
#[case("x", Item::Crossbow, "s", Location::VagabondSatchel(1))]
#[case("h", Item::Hammer, "s", Location::VagabondSatchel(1))]
#[case("s", Item::Sword, "s", Location::VagabondSatchel(1))]
#[case("t", Item::Tea, "t", Location::VagabondTrack(1))]
fn take_item_from_player(
    #[case] i: &str,
    #[case] expected_item: Item,
    #[case] l: &str,
    #[case] location: Location,
) {
    let action = Action::parse(Faction::Vagabond(1), &format!("%{i}M$i->${l}"))
        .expect("couldn't parse action");

    let expected = Action::Move {
        pieces: Pieces::Specific(vec![(Piece::Item(expected_item, None), 1)]),
        from: Location::Crafted(Faction::Marquise),
        to: location,
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
#[case("b", Item::Bag, "t", Location::VagabondTrack(1))]
#[case("m", Item::Boot, "s", Location::VagabondSatchel(1))]
#[case("c", Item::Coins, "t", Location::VagabondTrack(1))]
#[case("x", Item::Crossbow, "s", Location::VagabondSatchel(1))]
#[case("h", Item::Hammer, "s", Location::VagabondSatchel(1))]
#[case("s", Item::Sword, "s", Location::VagabondSatchel(1))]
#[case("t", Item::Tea, "t", Location::VagabondTrack(1))]
fn take_item_from_player_with_spectific_state(
    #[case] i: &str,
    #[case] expected_item: Item,
    #[case] l: &str,
    #[case] location: Location,
    #[values(("e", ItemState::Exhausted), ("r", ItemState::Refreshed))] state: (&str, ItemState),
) {
    let action = Action::parse(
        Faction::Vagabond(1),
        &format!("%{}{}M$i->${}", state.0, i, l),
    )
    .expect("couldn't parse action");

    let expected = Action::Move {
        pieces: Pieces::Specific(vec![(Piece::Item(expected_item, Some(state.1)), 1)]),
        from: Location::Crafted(Faction::Marquise),
        to: location,
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
#[case("#rerrand", Suit::Rabbit, Quest::Errand)]
#[case("#ferrand", Suit::Fox, Quest::Errand)]
#[case("#mescort", Suit::Mouse, Quest::Escort)]
fn complete_quest(#[case] c: &str, #[case] suit: Suit, #[case] quest: Quest) {
    let action =
        Action::parse(Faction::Vagabond(1), &format!("{c}Q->$q")).expect("couldn't parse action");

    let expected = Action::Move {
        pieces: Pieces::Specific(vec![(Piece::Card(Card::Quest(suit, quest)), 1)]),
        from: Location::AvailableQuests,
        to: Location::VagabondCompletedQuests(1),
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
#[case("#rerrand", Suit::Rabbit, Quest::Errand)]
#[case("#ferrand", Suit::Fox, Quest::Errand)]
#[case("#mescort", Suit::Mouse, Quest::Escort)]
fn draw_new_quest(#[case] c: &str, #[case] suit: Suit, #[case] quest: Quest) {
    let action =
        Action::parse(Faction::Vagabond(1), &format!("{c}->Q")).expect("couldn't parse action");

    let expected = Action::Place {
        pieces: vec![(Piece::Card(Card::Quest(suit, quest)), 1)],
        at: vec![Location::AvailableQuests],
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
#[case("f", Item::Torch)]
#[case("m", Item::Boot)]
fn exhaust_item_on_defense(#[case] i: &str, #[case] item: Item) {
    let action =
        Action::parse(Faction::Marquise, &format!("%{i}V$se")).expect("couldn't parse action");

    let expected = Action::Exhaust {
        faction: Faction::Marquise,
        pieces: Pieces::Specific(vec![(Piece::Item(item, None), 1)]),
        in_: Some(Location::VagabondSatchel(1)),
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
#[case("b", Item::Bag)]
#[case("m", Item::Boot)]
#[case("c", Item::Coins)]
#[case("x", Item::Crossbow)]
#[case("h", Item::Hammer)]
#[case("s", Item::Sword)]
#[case("t", Item::Tea)]
fn remove_item(#[case] i: &str, #[case] expected_item: Item) {
    let action =
        Action::parse(Faction::Vagabond(1), &format!("%{i}$s->*")).expect("couldn't parse action");

    let expected = Action::RemoveFromGame {
        pieces: vec![(Piece::Item(expected_item, None), 1)],
        from: Some(Location::VagabondSatchel(1)),
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn set_relationship(
    #[values(("M", Faction::Marquise), ("E", Faction::Eyrie))] with: (&str, Faction),
    #[values(("0", Relationship::Indifferent), ("1", Relationship::Allied1), ("a", Relationship::Allied), ("h", Relationship::Hostile))]
    to: (&str, Relationship),
) {
    let action = Action::parse(Faction::Vagabond(1), &format!("m{}={}", with.0, to.0))
        .expect("couldn't parse action");

    let expected = Action::SetRelationship {
        faction: Faction::Vagabond(1),
        with: with.1,
        to: to.1,
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn set_relationship_on_defense(
    #[values(("M", Faction::Marquise), ("E", Faction::Eyrie))] with: (&str, Faction),
    #[values(("0", Relationship::Indifferent), ("1", Relationship::Allied1), ("a", Relationship::Allied), ("h", Relationship::Hostile))]
    to: (&str, Relationship),
) {
    let action =
        Action::parse(with.1, &format!("Vm{}={}", with.0, to.0)).expect("couldn't parse action");

    let expected = Action::SetRelationship {
        faction: Faction::Vagabond(1),
        with: with.1,
        to: to.1,
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn coalition() {
    let action = Action::parse(Faction::Vagabond(1), "#rdomE").expect("couldn't parse action");

    let expected = Action::ActivateDominance {
        faction: Faction::Vagabond(1),
        coalition: Some(Faction::Eyrie),
        suit: CardSuit::Rabbit,
        commentary: None,
    };
    assert_eq!(expected, action);
}

#[rstest]
fn move_item_and_exhaust() {
    let action = Action::parse(Faction::Vagabond(1), "%t$t-e>$s").expect("couldn't parse action");

    let expected = Action::MoveExhaust {
        pieces: Pieces::Specific(vec![(Piece::Item(Item::Tea, None), 1)]),
        from: Location::VagabondTrack(1),
        to: Location::VagabondSatchel(1),
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn move_item_and_refresh() {
    let action = Action::parse(Faction::Vagabond(1), "%t$s-r>$t").expect("couldn't parse action");

    let expected = Action::MoveRefresh {
        pieces: Pieces::Specific(vec![(Piece::Item(Item::Tea, None), 1)]),
        from: Location::VagabondSatchel(1),
        to: Location::VagabondTrack(1),
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn take_item_and_refresh() {
    let action = Action::parse(Faction::Vagabond(1), "%sM$i-r>$s").expect("couldn't parse action");

    let expected = Action::MoveRefresh {
        pieces: Pieces::Specific(vec![(Piece::Item(Item::Sword, None), 1)]),
        from: Location::Crafted(Faction::Marquise),
        to: Location::VagabondSatchel(1),
        commentary: None,
    };

    assert_eq!(expected, action);
}
